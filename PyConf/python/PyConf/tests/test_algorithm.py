###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
import re

import Configurables
from PyConf import ConfigurationError
from PyConf.dataflow import DataHandle
from PyConf.Algorithms import (
    Gaudi__Examples__IntDataConsumer as IntDataConsumer,
    Gaudi__Examples__IntDataProducer as IntDataProducer,
    Gaudi__Examples__IntToFloatData as IntToFloatData,
    Gaudi__Examples__THDataConsumer as ToolConsumer,
    Gaudi__Examples__VectorDataProducer as VectorDataProducer,
    Gaudi__Examples__IntVectorsToIntVector as IntVectorsToIntVector,
    Gaudi__Examples__SDataProducer as SDataProducer,
    Gaudi__Examples__SRangesToIntVector as SRangesToIntVector,
    MyGaudiAlgorithm,
    Gaudi__Examples__CountSelectedTracks as CountSelectedTracks,
    Gaudi__Examples__SelectTracks as SelectTracks,
)
from PyConf.Tools import (
    Gaudi__Examples__FloatTool as FloatTool,
    MyTool,
)


class MockDataHandle(DataHandle):
    def __init__(self, location, id=None, type='unknown_t'):
        self._location = location
        self._type = type
        self._id = id or location
        # a dummy "producer", could also be replaced by a mock
        self._producer = IntDataProducer()

    @property
    def type(self):
        return self._type


def test_init():
    # Fail on nonexistent properties
    with pytest.raises(ConfigurationError) as e:
        IntDataProducer(NotAProp=1)
    assert re.search(r'NotAProp.*not propert.* of.*IntDataProducer', str(e))

    # Output property is detected
    producer = IntDataProducer()
    assert len(producer.outputs) == 1
    assert len(producer.inputs) == 0
    data = producer.outputs['OutputLocation']
    assert isinstance(data, DataHandle)
    assert data.producer == producer

    # initializing an algorithm twice should give the same instance
    producer2 = IntDataProducer()
    assert producer is producer2, "algorithms instantiation doesn't seem to be cached correctly"

    with pytest.raises(ConfigurationError):
        # same algorithms shouldn't have different names
        IntDataProducer(name="wurst")

    # Cannot set outputs explicitly
    with pytest.raises(ConfigurationError) as e:
        producer = IntDataProducer(OutputLocation='test')
    assert re.search(r'Cannot set output property OutputLocation explicitly',
                     str(e))

    # Must always provide all inputs
    with pytest.raises(ConfigurationError) as e:
        consumer = IntDataConsumer()
    assert re.match(r'.*provide all inputs.*InputLocation.*', str(e))

    # Type of inputs
    with pytest.raises(TypeError) as e:
        consumer = IntDataConsumer(InputLocation='test')
    assert re.search(r'Inputs.*as DataHandles.*InputLocation.*', str(e))

    consumer = IntDataConsumer(InputLocation=data)
    assert consumer.inputs['InputLocation'] == data

    consumer = IntDataConsumer(InputLocation=producer)
    assert consumer.inputs['InputLocation'] == data

    with pytest.raises(ConfigurationError):
        # cannot change property after instantiation
        consumer.OutputLevel = 2


def test_init_input_location_list():
    """Properties that are lists of DataHandle objects should be supported."""
    multitransformer_bare = IntVectorsToIntVector()
    # FIXME: It is a bug that Algorithm cannot detect an input property for an
    # algorithm that accepts N inputs. This occurs because there's no API in
    # Gaudi for asking if a property is an input, only the heurisitcs of seeing
    # if a default value is a DataHandle. Because the default value here is an
    # empty list, the heuristics fail
    # assert len(multitransformer_bare.inputs) == 1
    assert len(multitransformer_bare.outputs) == 1

    vdp1 = VectorDataProducer()
    vdp2 = VectorDataProducer()
    mt = IntVectorsToIntVector(InputLocations=[vdp1, vdp2])
    mt2 = IntVectorsToIntVector(
        InputLocations=[vdp1.OutputLocation, vdp2.OutputLocation])
    # We should be able to handle a mix of algorithms and data handles
    mt3 = IntVectorsToIntVector(InputLocations=[vdp1, vdp2.OutputLocation])

    # If we initialize the MultiTransformer with inputs, then we can detect the
    # DataHandles and everything looks sensible (wrt. the FIXME above)
    assert len(mt.inputs) == 1
    assert len(mt2.inputs) == 1
    assert len(mt.inputs['InputLocations']) == 2
    assert len(mt2.inputs['InputLocations']) == len(
        mt3.inputs['InputLocations']) == 2
    assert all(isinstance(x, DataHandle) for x in mt.inputs['InputLocations'])
    assert all(isinstance(x, DataHandle) for x in mt2.inputs['InputLocations'])


def test_tool_compat():
    producer = IntDataProducer()
    float_producer = IntToFloatData(InputLocation=producer)
    alg = ToolConsumer(
        FloatTool=FloatTool(Input=float_producer), InputLocation=producer)
    assert len(alg.tools) == 1

    with pytest.raises(TypeError):
        ToolConsumer(
            FloatTool=Configurables.Gaudi__Examples__FloatTool(),
            InputLocation=producer)  # don't be a fool, wrap your tool

    # A Tool instance should be usable in multiple (different) Algorithms
    tool = FloatTool(Input=float_producer)
    alg1 = ToolConsumer(FloatTool=tool, InputLocation=producer)
    alg2 = ToolConsumer(
        FloatTool=tool, InputLocation=producer,
        OutputLevel=0)  # make the second alg slightly different
    assert alg1.tools == alg2.tools


def test_list_of_tools_compat():
    float_producer = IntToFloatData(InputLocation=IntDataProducer())
    # MyGaudiAlgorithm expects DataObject handles but we don't have a
    # dummy producer for these, so "erase" the type to avoid the check.
    data = IntDataProducer().OutputLocation.untyped()
    alg = MyGaudiAlgorithm(
        raw=data,
        tracks=data,
        hits=data,
        MyPublicToolHandleArrayProperty=[
            FloatTool(public=True, Input=float_producer),
            FloatTool(public=True, Input=float_producer)
        ])
    assert len(alg.tools["MyPublicToolHandleArrayProperty"]) == 2


def test_public_tool_init():
    """Public tool properties are configurable only with public tools."""
    public_tool = MyTool(public=True)
    # Tools are private by default
    private_tool = MyTool()

    # MyGaudiAlgorithm expects DataObject handles but we don't have a
    # dummy producer for these, so "erase" the type to avoid the check.
    data = IntDataProducer().OutputLocation.untyped()
    producer_kwargs = dict(raw=data, tracks=data, hits=data)

    MyGaudiAlgorithm(PubToolHandle=public_tool, **producer_kwargs)
    with pytest.raises(ConfigurationError):
        MyGaudiAlgorithm(PubToolHandle=private_tool, **producer_kwargs)


def test_datahandle_type_check():
    # Passing the same type is fine
    int_data = IntDataProducer().OutputLocation
    IntDataConsumer(InputLocation=int_data)

    # Passing the wrong type fails
    float_data = IntToFloatData(InputLocation=IntDataProducer()).OutputLocation
    with pytest.raises(ConfigurationError) as excinfo:
        IntDataConsumer(InputLocation=float_data)
    assert (re.search(r'IntDataConsumer\.InputLocation.*int.*float',
                      str(excinfo.value), re.DOTALL) is not None)

    # Passing an "untyped" handle is fine
    untyped_data = float_data.untyped()
    assert untyped_data.type == 'unknown_t'
    assert untyped_data.location == float_data.location
    IntDataConsumer(InputLocation=untyped_data)

    # KeyedContainer can be given to a Range
    keyed_container = SDataProducer()
    SRangesToIntVector(InputRanges=[keyed_container])
    # but not if the contained type does not match
    with pytest.raises(ConfigurationError):
        CountSelectedTracks(InputData=keyed_container)

    # SharedObjectsContainer (aka Selection) can be given to a Range
    shared_objects_container = SelectTracks(
        InputData=MockDataHandle('/Event/Range')).OutputData
    SelectTracks(InputData=shared_objects_container)


def algorithm_equal_hashes():
    """Two Algorithms should have equal hashes if their properties and inputs are equal."""
    pass


def algorithm_unequal_hashes():
    """Two Algorithms should not have equal hashes if their properties and inputs are not equal."""
    pass


def algorithm_hash_forced_outputs():
    """An Algorithm with a forced output should have a different hash to one without."""
    pass
