###############################################################################
# (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import Configurables
from PyConf import configurable
from PyConf.components import (
    Algorithm,
    Tool,
    _is_configurable_algorithm,
    _is_configurable_tool,
)


def _wrapped_component_type(wrapper, component_type):
    """Wrap a Configurable class in a PyConf component.

    Return the Configurable class `component_type` wrapped as a
    `PyConf.components.Algorithm` or `PyConf.components.Tool`. See the
    corresponding `__new__` method for details on the keyword arguments.

    """

    @configurable
    def wrapped(**kwargs):
        return wrapper(component_type, **kwargs)

    wrapped.getDefaultProperties = component_type.getDefaultProperties

    return wrapped


def _create_importer(wrapper, type_predicate):
    cache = {}

    class PyConfImportModule(object):
        """Magic module that wraps retrieved objects in PyConf components

        Requested objects are imported from Gaudi.Configurables. Every
        algorithm or tool that you can import from Configurables, you
        can also import from this module.

            >>> from PyConf.Algorithms import MyGaudiAlgorithm
            >>> from PyConf.Tools import Gaudi__Examples__FloatTool

        Importing tools as algorithms or vice versa will fail.

        """

        def __init__(self, file, name):
            # Create a better illusion that we are a module
            self.__file__ = file
            self.__name__ = name

        @property
        def __all__(self):
            # TODO ideally we will return only Tools or Algorithms but
            #      the snippet below seems to be slow.
            # return [
            #     n for n in Configurables.__all__
            #     if type_predicate(getattr(Configurables, n))
            # ]
            return Configurables.__all__

        def __dir__(self):
            return self.__all__

        def __getattr__(self, name):
            try:
                wrapped = cache[name]
            except KeyError:
                try:
                    component_type = getattr(Configurables, name)
                except AttributeError:
                    # Expect names starting with a letter to come from
                    # Gaudi.Configurables and others to come from this module.
                    if name[0].isalpha():
                        raise
                    else:
                        raise AttributeError(
                            "module '{}' has no attribute '{}'".format(
                                self.__name__, name))
                if not type_predicate(component_type):
                    raise TypeError(
                        'cannot create {} with {}, which is of type {}'.format(
                            wrapper, component_type,
                            component_type.getGaudiType()))
                wrapped = _wrapped_component_type(wrapper, component_type)
                cache[name] = wrapped
            return wrapped

    return PyConfImportModule


AlgorithmImporter = _create_importer(Algorithm, _is_configurable_algorithm)
ToolImporter = _create_importer(Tool, _is_configurable_tool)
