/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Local
#include "RichDetectors/RichX.h"

// RichDet
#include "RichDet/DeRich2.h"

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class Rich2 Rich2.h
   *
   *  Rich2 helper class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class Rich2 final : public Rich::Detector::RichX {

  public:
    // types

    /// The underlying detector description object (DetDesc or DD4HEP)
    using DetElem = DeRich2;

  public:
    /// Constructor from DetDesc Rich2
    Rich2( const DetElem& deRich ) : RichX( deRich ) {}

  public:
    // conditions handling

    /// Default conditions name
    static constexpr const char* DefaultConditionKey = "Rich2Detector";

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static decltype( auto )                                                       //
    addConditionDerivation( PARENT*                     parent,                   ///< Pointer to parent algorithm
                            LHCb::DetDesc::ConditionKey key = DefaultConditionKey ///< Derived object name
    ) {
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "Rich2::addConditionDerivation : Key=" << key << endmsg;
      }
      return LHCb::DetDesc::addConditionDerivation<Rich2( const DetElem& )> //
          ( parent->conditionDerivationMgr(),                               // manager
            {DeRichLocations::Rich2},                                       // input condition locations
            std::move( key ) );                                             // output derived condition location
    }
  };

} // namespace Rich::Detector
