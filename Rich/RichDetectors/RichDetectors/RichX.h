/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <memory>
#include <vector>

// Det Desc
#include "DetDesc/ConditionKey.h"
#include "DetDesc/IConditionDerivationMgr.h"

// LHCbKernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// RichDet
#include "RichDet/DeRich.h"

// LHCbMath
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/SIMDTypes.h"

// Utils
#include "RichUtils/RichSIMDRayTracing.h"

// Local
#include "RichDetectors/RichMirror.h"
#include "RichDetectors/RichPD.h"
#include "RichDetectors/RichPDPanel.h"

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class Rich Rich1.h
   *
   *  Rich Detector base class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class RichX {

  public:
    // types

    /// The underlying detector description object (DetDesc or DD4HEP)
    using BaseDetElem = DeRich;

    /// Type for container of allocated mirror objects
    using Mirrors = std::vector<std::shared_ptr<const Mirror>>;

    /// The PD type to use in public API
#ifdef USE_DD4HEP
    // For now use RichDet here as well. To Do migrate to DDD4HEP classes...
    using PD = DeRichPD;
#else
    using PD = DeRichPD;
#endif

  public:
    // constructors

    /// Constructor from detector object
    RichX( const BaseDetElem& det )
        : m_type( det.rich() )
        , m_rich( &det )
        , m_panels( {PDPanel( *det.pdPanel( Rich::firstSide ) ), //
                     PDPanel( *det.pdPanel( Rich::secondSide ) )} ) {}

  private:
    // methods

    template <typename MIRRORS>
    inline Mirrors convertMirrors( MIRRORS&& in_mirrs ) const {
      Mirrors out_mirrs;
      out_mirrs.reserve( in_mirrs.size() );
      for ( auto m : in_mirrs ) { out_mirrs.emplace_back( std::make_unique<const Mirror>( *m ) ); }
      return out_mirrs;
    }

  public:
    // Accesssors
    // Note for now just forward to DetDesc implementations but eventually
    // aim is to port functionality here, in preparation for (simplier) DD4HEP objects.

    /// Check a parameter exists
    inline decltype( auto ) exists( const std::string& name ) const { return m_rich->exists( name ); }

    /// Access a parameter
    template <typename TYPE>
    inline decltype( auto ) param( const std::string& name ) const {
      return m_rich->param<TYPE>( name );
    }

    /// Access a parameter vector
    template <typename TYPE>
    inline decltype( auto ) paramVect( const std::string& name ) const {
      return m_rich->paramVect<TYPE>( name );
    }

    /// The RICH type
    inline Rich::DetectorType rich() const noexcept { return m_type; }

    /// primary mirrors
    inline decltype( auto ) primaryMirrors() const { return convertMirrors( m_rich->primaryMirrors() ); }

    /// secondary mirrors
    inline decltype( auto ) secondaryMirrors() const { return convertMirrors( m_rich->secondaryMirrors() ); }

    /// Spherical mirror radius
    inline decltype( auto ) sphMirrorRadius() const noexcept { return m_rich->sphMirrorRadius(); }

    /// Spherical mirror radius (SIMD)
    inline decltype( auto ) sphMirrorRadiusSIMD() const noexcept { return m_rich->sphMirrorRadiusSIMD(); }

    /// Access CoC for given side(s)
    template <typename SIDE>
    inline decltype( auto ) nominalCentreOfCurvature( const SIDE& side ) const noexcept {
      return m_rich->nominalCentreOfCurvature( side );
    }

    /// Access CoC (SIMD) for given side
    inline decltype( auto ) nominalCentreOfCurvatureSIMD( const Rich::Side side ) const noexcept {
      return m_rich->nominalCentreOfCurvatureSIMD( side );
    }

    /// Access nominal plane for given side(s)
    template <typename SIDE>
    inline decltype( auto ) nominalPlane( const SIDE& side ) const noexcept {
      return m_rich->nominalPlane( side );
    }

    /// Access nominal plane (SIMD)
    inline decltype( auto ) nominalPlaneSIMD( const Rich::Side side ) const { return m_rich->nominalPlaneSIMD( side ); }

    /// Returns the nominal normal vector of the flat mirror plane for this Rich
    inline decltype( auto ) nominalNormal( const Rich::Side side ) const noexcept {
      return m_rich->nominalNormal( side );
    }

    /// Returns the SIMD nominal normal vector of the flat mirror plane for this Rich
    inline decltype( auto ) nominalNormalSIMD( const Rich::Side side ) const noexcept {
      return m_rich->nominalNormalSIMD( side );
    }

    /// Access which detector side given point is on
    template <typename POINT>
    inline decltype( auto ) side( const POINT& point ) const noexcept {
      return m_rich->side( point );
    }

    /// Access RICH Beampipe object. To be seen if this is reallty used and needed with DD4HEP ?
    inline decltype( auto ) beampipe() const noexcept { return m_rich->beampipe(); }

    /// Access PD Panels
    inline decltype( auto ) pdPanel( const Rich::Side panel ) const noexcept {
      return m_rich->pdPanel( panel );
      // return &m_panels[panel];
    }

    /// Method to find the row/column of a spherical mirror segment.
    inline decltype( auto ) sphMirrorSegPos( const int mirrNum ) const { return m_rich->sphMirrorSegPos( mirrNum ); }

    /// Method to find the row/column of a flat mirror segment. It can be used to
    inline decltype( auto ) secMirrorSegPos( const int mirrNum ) const { return m_rich->secMirrorSegPos( mirrNum ); }

    /// Returns a pointer to the tabulated property that holds the nominal quantum efficiency of a PD
    inline decltype( auto ) nominalPDQuantumEff() const noexcept { return m_rich->nominalPDQuantumEff(); }

    /// Returns a pointer to the tabulated property that holds the nominal reflectivity of primary mirrors
    inline decltype( auto ) nominalSphMirrorRefl() const noexcept { return m_rich->nominalSphMirrorRefl(); }

    /// Returns a pointer to the tabulated property that holds the nominal reflectivity of secondary mirrors
    inline decltype( auto ) nominalSecMirrorRefl() const noexcept { return m_rich->nominalSecMirrorRefl(); }

    /** Returns a pointer to the tabulated property that holds the absorption
     *  length of the gas window for this Rich */
    inline decltype( auto ) gasWinAbsLength() const noexcept { return m_rich->gasWinAbsLength(); }

  public:
    // ray tracing

    /// Ray trace a given direction with the given PD panel (scalar)
    LHCb::RichTraceMode::RayTraceResult rayTrace( const Rich::Side          side,        //
                                                  const Gaudi::XYZPoint&    pGlobal,     //
                                                  const Gaudi::XYZVector&   vGlobal,     //
                                                  Gaudi::XYZPoint&          hitPosition, //
                                                  LHCb::RichSmartID&        smartID,     //
                                                  const PD*&                dePD,        //
                                                  const LHCb::RichTraceMode mode ) const {
      // are we configured to test individual PD acceptance or just interset the plane ?
      return ( mode.detPlaneBound() == LHCb::RichTraceMode::DetectorPlaneBoundary::RespectPDTubes
                   ? m_panels[side].PDWindowPoint( pGlobal, vGlobal, hitPosition, smartID, dePD, mode )
                   : m_panels[side].detPlanePoint( pGlobal, vGlobal, hitPosition, smartID, dePD, mode ) );
    }

    /// type for SIMD ray tracing result
    using SIMDRayTResult = Rich::RayTracingUtils::SIMDResult;
    /// scalar FP type for SIMD objects
    using FP = Rich::SIMD::DefaultScalarFP;
    /// SIMD float type
    using SIMDFP = Rich::SIMD::FP<FP>;
    /// Array of PD pointers
    using SIMDPDs = Rich::SIMD::STDArray<const PD*>;
    /// Array of SmartIDs
    using SIMDSmartIDs = SIMDRayTResult::SmartIDs;

    /// Ray trace a given direction with the given PD panel (SIMD)
    SIMDRayTResult::Results rayTrace( const Rich::Side              side,        //
                                      const Rich::SIMD::Point<FP>&  pGlobal,     //
                                      const Rich::SIMD::Vector<FP>& vGlobal,     //
                                      Rich::SIMD::Point<FP>&        hitPosition, //
                                      SIMDSmartIDs&                 smartID,     //
                                      SIMDPDs&                      PDs,         //
                                      const LHCb::RichTraceMode     mode ) const {
      // are we configured to test individual PD acceptance or just interset the plane ?
      return ( mode.detPlaneBound() == LHCb::RichTraceMode::DetectorPlaneBoundary::RespectPDTubes
                   ? m_panels[side].PDWindowPointSIMD( pGlobal, vGlobal, hitPosition, smartID, PDs, mode )
                   : m_panels[side].detPlanePointSIMD( pGlobal, vGlobal, hitPosition, smartID, PDs, mode ) );
    }

    /// Ray trace a given direction with the correct PD panel (SIMD)
    SIMDRayTResult::Results rayTrace( const Rich::SIMD::Sides&      sides,       //
                                      const Rich::SIMD::Point<FP>&  pGlobal,     //
                                      const Rich::SIMD::Vector<FP>& vGlobal,     //
                                      Rich::SIMD::Point<FP>&        hitPosition, //
                                      SIMDSmartIDs&                 smartID,     //
                                      SIMDPDs&                      PDs,         //
                                      const LHCb::RichTraceMode     mode ) const;

  private:
    // data

    /// RICH type
    Rich::DetectorType m_type = Rich::InvalidDetector;

    /// DetDesc Rich object
    const BaseDetElem* m_rich{nullptr};

  protected:
    // data

    /// The owned PD Panel objects, for each detector side
    PanelArray<PDPanel> m_panels{};
  };

} // namespace Rich::Detector
