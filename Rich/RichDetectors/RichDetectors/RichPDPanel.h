/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <memory>

// LHCbKernel
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// LHCbMath
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/SIMDTypes.h"

// Local
#include "RichDetectors/RichPD.h"

// RichDet
#include "RichDet/DeRichPDPanel.h"

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class Rich Rich1.h
   *
   *  Rich Detector base class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class PDPanel {

  public:
    // types

    /// The underlying detector description object (DetDesc or DD4HEP)
    using DetElem = DeRichPDPanel;

  public:
    // constructors

    /// Default Constructor
    PDPanel() = default;

    /// Constructor from det elem
    PDPanel( const DetElem& panel ) : m_panel( &panel ) {}

  public:
    // Accesssors
    // Note for now just forward to DetDesc implementations but eventually
    // aim is to port functionality here, in preparation for (simplier) DD4HEP objects.

    /// Intersect PD panel (scalar)
    template <typename... ARGS>
    inline decltype( auto ) detPlanePoint( ARGS&&... args ) const {
      return m_panel->detPlanePoint( std::forward<ARGS>( args )... );
    }

    /// Intersect PD window (scalar)
    template <typename... ARGS>
    inline decltype( auto ) PDWindowPoint( ARGS&&... args ) const {
      return m_panel->PDWindowPoint( std::forward<ARGS>( args )... );
    }

    /// Intersect PD panel (SIMD)
    template <typename... ARGS>
    inline decltype( auto ) detPlanePointSIMD( ARGS&&... args ) const {
      return m_panel->detPlanePointSIMD( std::forward<ARGS>( args )... );
    }

    /// Intersect PD window (SIMD)
    template <typename... ARGS>
    inline decltype( auto ) PDWindowPointSIMD( ARGS&&... args ) const {
      return m_panel->PDWindowPointSIMD( std::forward<ARGS>( args )... );
    }

  private:
    // data

    /// DetDesc panel object
    const DetElem* m_panel{nullptr};
  };

} // namespace Rich::Detector
