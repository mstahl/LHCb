/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichFutureDAQ/EncodeTel40Data.h"
#include "RichFutureDAQ/RichPackedFrameSizes.h"

// Gaudi
#include "GaudiKernel/Kernel.h"

using namespace Rich::Future::DAQ;

namespace {

  /// Set a given bit on
  template <typename POS, typename DATA>
  void setBitOn( const POS pos, DATA& data ) noexcept {
    data |= 1 << pos;
  }

  /// Is the given frame bit already on ?
  template <typename POS, typename DATA>
  bool isBitOn( const POS pos, const DATA data ) noexcept {
    return ( 0 != ( data & ( 1 << pos ) ) );
  }

} // namespace

EncodeTel40::EncodeTel40( const Tel40CableMapping&   tel40Maps, //
                          CommonMessagingBase const* parent )
    : m_parent( parent ) {
  // init. the data struture to fill
  // get the list of active Tel40s and the links for each
  const auto& links = tel40Maps.linksPerSourceID();
  // loop over tel40's
  for ( const auto& tel40 : links ) {
    // make entry for this Tel40
    auto& t = m_tel40Data[tel40.first];
    // loop over links and make default entry for each
    for ( const auto& c : tel40.second ) { t[c]; }
  }
}

void EncodeTel40::DataHalves::add( const Rich::DAQ::FrameBitIndex bit ) {

  // Which half of the data payload are we in ?
  const DataType half = ( bit.data() < PayloadBitOffsets[1] ? 0u : 1u );

  // Functor to set a payload bit in a half
  auto setPayloadBit = []( const auto b, const auto half, HalfPayload& halfData ) {
    // bit shifted by offset in data half
    const auto lbit = b - PayloadBitOffsets[half];
    assert( lbit >= 0 );

    // word index from local bit value
    const auto word = lbit / BitsPerWord;
    assert( (std::size_t)word < halfData.size() );

    // index in word (0-7)
    const auto index = lbit % BitsPerWord;
    assert( index < BitsPerWord );

    // set bit on
    assert( !isBitOn( index, halfData[word] ) );
    setBitOn( index, halfData[word] );
  };

  // Do we need to convert this half to NZS ?
  // i.e. are we still ZS but at the max. size for this mode.
  if ( UNLIKELY( m_zs[half] && m_data[half].size() == MaxZSByteSize[half] ) ) {

    // New data structure for NZS form
    HalfPayload nzs_data( NZSByteSize[half], 0u );

    // Set the MSB in the highest word to 1 to flag this as in NZS mode
    setBitOn( MaxBitIndex, nzs_data.back() );

    // Loop over ZS data and fill into NZS
    for ( const auto b : m_data[half] ) { setPayloadBit( b, half, nzs_data ); }

    // Move NZS data to main storage
    m_data[half] = std::move( nzs_data );

    // flag this half as now in NZS mode
    m_zs[half] = false;

  } // convert data half to NZS form

  // Finally, set the new global bit
  // What mode are we in ?
  if ( LIKELY( m_zs[half] ) ) {
    // ZS mode so just append bit to list as byte value
    assert( m_data[half].size() < MaxZSByteSize[half] );
    m_data[half].emplace_back( bit.data() );
  } else {
    // We are in NZS mode, so just flag this bit in the data
    setPayloadBit( bit.data(), half, m_data[half] );
  }
}

/// Fill RawBanks into a RawEvent
void EncodeTel40::fill( LHCb::RawEvent& rawEv, const std::uint8_t version ) {

  // Loop over the Tel40s filled
  for ( const auto& tel40 : m_tel40Data ) {

    if ( m_parent ) { m_parent->debug() << "Creating RawBank for Source ID = " << tel40.first << endmsg; }

    /// container for packed frame sizes
    std::vector<PackedFrameSizes> packedFrameSizes;
    packedFrameSizes.reserve( tel40.second.size() / 2 );

    // Tally the overall size of the data payload
    std::uint32_t totDataSize{0};

    // loop over connections for this Tel40 and collect frame sizes
    std::uint8_t word = 1;
    for ( const auto& con : tel40.second ) {
      // Number of bytes in the frame payload
      const auto bSize = con.second.nBytes();
      totDataSize += bSize;
      if ( 1 == word ) {
        packedFrameSizes.emplace_back( 0, bSize );
        word = 0; // Next time through fill first half of this word
      } else {
        packedFrameSizes.back().setSize0( bSize );
        word = 1; // Next time through start a new word.
      }
    }

    // Create vector for the RawBank payload
    std::vector<DataHalves::DataType> payload;
    payload.reserve( packedFrameSizes.size() + totDataSize );

    // Pipe the data size header words to the RawBank
    for ( const auto h : packedFrameSizes ) {
      if ( m_parent ) { m_parent->debug() << "  -> Size Header " << h << endmsg; }
      payload.emplace_back( h.data() );
    }

    // now save the rest of the payload for each connector
    for ( const auto& con : tel40.second ) {
      if ( m_parent ) {
        // zero pad its to two digits...
        boost::format twoDigits( "%u" );
        twoDigits.modify_item( 1, boost::io::group( std::setw( 2 ), std::setfill( '0' ) ) );
        m_parent->debug() << "  -> Con:" << twoDigits % (int)con.first.data() << " " << con.second << endmsg;
      }
      // save to payload
      con.second.appendTo( payload );
    }

    // finally save to RawEvent
    rawEv.addBank( tel40.first.data(), LHCb::RawBank::Rich, version, std::move( payload ) );

  } // tel40's
}
