/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichFutureDAQ/RichTel40CableMapping.h"

// RICH
#include "RichUtils/RichException.h"
#include "RichUtils/ToArray.h"
#include "RichUtils/ZipRange.h"

// STL
#include <limits>

using namespace Rich::Future::DAQ;

void Tel40CableMapping::fillCableMaps( const DeRichSystem& richSys ) {

  using namespace Rich::DAQ;

  const auto cable_conds = std::array{"R1U_Tel40CablingMap", "R1D_Tel40CablingMap", //
                                      "R2A_Tel40CablingMap", "R2C_Tel40CablingMap"};
  const auto rich_types  = std::array{Rich::Rich1, Rich::Rich1, Rich::Rich2, Rich::Rich2};
  const auto panel_types = std::array{Rich::top, Rich::bottom, Rich::aside, Rich::cside};

  // default initialised status to OK
  m_isInitialised = true;

  for ( const auto&& [cable_cond, rich, panel] : Ranges::ConstZip( cable_conds, rich_types, panel_types ) ) {

    // load the condition, if available
    if ( richSys.hasCondition( cable_cond ) ) {

      const auto cond = richSys.condition( cable_cond );

      // Number of links for this RICH panel
      const std::size_t nLinks = cond->param<int>( "NumberOfLinks" );

      // Load links data
      const auto pmtTypes   = cond->paramVect<std::string>( "PMTTypes" );
      const auto modNames   = cond->paramVect<std::string>( "ModuleNames" );
      const auto modNums    = cond->paramVect<int>( "ModuleNumbers" );
      const auto pdmdbs     = cond->paramVect<int>( "PDMDBNumbers" );
      const auto pdmdbLinks = cond->paramVect<int>( "PDMDBLinks" );
      const auto sourceIDs  = cond->paramVect<int>( "Tel40SourceIDs" );
      const auto connectors = cond->paramVect<int>( "Tel40SConnectors" );
      const auto mpos       = cond->paramVect<int>( "Tel40MPOs" );
      const auto statuses   = cond->paramVect<int>( "Tel40LinkIsActive" );
      // sanity size check
      if ( nLinks != pmtTypes.size() ||   //
           nLinks != modNames.size() ||   //
           nLinks != modNums.size() ||    //
           nLinks != pdmdbs.size() ||     //
           nLinks != pdmdbLinks.size() || //
           nLinks != sourceIDs.size() ||  //
           nLinks != connectors.size() || //
           nLinks != mpos.size() ||       //
           nLinks != statuses.size() ) {
        throw Rich::Exception( "Inconsistent data sizes for " + std::string{cable_cond} );
      }

      // loop over data and fill lookup structures
      for ( const auto&& [type, name, modN, pdmdb, pdmdbLink, sID, conn, mpo, status] : //
            Ranges::ConstZip( pmtTypes, modNames, modNums, pdmdbs, pdmdbLinks, sourceIDs, connectors, mpos,
                              statuses ) ) {

        // data
        assert( (std::size_t)conn <= ConnectionsPerTel40MPO );
        assert( sID >= 0 && sID < std::numeric_limits<SourceID::Type>::max() );
        const SourceID sourceID( sID );
        // connections in DB are numbered [1-12] so subtract one to convert to [0-11]
        const Tel40Connector link( ( ConnectionsPerTel40MPO * ( mpo - 1 ) ) + ( conn - 1 ) );

        // PMT type
        const bool isLargePMT = type == "H";

        // The cached RichSmartID for this entry
        LHCb::RichSmartID smartID( rich, panel, LHCb::RichSmartID::MaPMTID );
        smartID.setLargePMT( isLargePMT );

        // link data struct
        const Tel40LinkData linkD( name,                   //
                                   smartID,                //
                                   sourceID,               //
                                   link,                   //
                                   isLargePMT,             //
                                   bool( status ),         //
                                   PDModuleNumber( modN ), //
                                   PDMDBID( pdmdb ),       //
                                   PDMDBFrame( pdmdbLink ) );

        // fill the Tel40 connection data structure
        if ( (std::size_t)sourceID.data() >= m_tel40ConnData.size() ) {
          throw Rich::Exception( "Tel40 Source ID '" + std::to_string( sourceID.data() ) + "' exceeds expected range" );
        }
        auto& conData = m_tel40ConnData[sourceID.data()];
        assert( (std::size_t)link.data() < conData.size() );
        conData[link.data()] = linkD;

        // fill Tel40 module data structure
        assert( (std::size_t)modN < m_tel40ModuleData.size() ); // in range
        auto& mData = m_tel40ModuleData[modN];
        assert( (std::size_t)pdmdb < mData.size() ); // in range
        auto& pData = mData[pdmdb];
        assert( (std::size_t)pdmdbLink < pData.size() ); // in range
        assert( !pData[pdmdbLink].isValid() );           // not yet initialised
        pData[pdmdbLink] = linkD;

        // fill the active links per source ID map
        auto& links = m_linksPerSourceID[sourceID];
        assert( std::find( links.begin(), links.end(), link ) == links.end() );
        links.insert( link );
      }

    } else {
      // indicate initialisation failed.
      m_isInitialised = false;
    }
  } // conditions loop
}
