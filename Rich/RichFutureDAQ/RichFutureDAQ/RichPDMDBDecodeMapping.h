/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <cassert>
#include <cstdint>
#include <map>
#include <ostream>
#include <set>
#include <vector>

// Kernel
#include "Kernel/RichSmartID.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Gaudi
#include "GaudiKernel/CommonMessaging.h"
#include "GaudiKernel/SerializeSTL.h"

// Det Desc
#include "DetDesc/ConditionKey.h"
#include "DetDesc/IConditionDerivationMgr.h"

// RICH DAQ
#include "RichFutureDAQ/RichTel40CableMapping.h"

namespace Rich::Future::DAQ {

  // overloads for vectors etc.
  using GaudiUtils::operator<<;

  /// Helper class for RICH PDMDB readout mapping
  class PDMDBDecodeMapping final {

  public:
    /// Constructor from RICH detector elements
    PDMDBDecodeMapping( const DeRichSystem& richSys ) : m_isInitialised( true ) {
      // load the mapping conditions needed for encoding
      fillRType( richSys );
      fillHType( richSys );
    }

  public:
    // data types

    /// The data for each anode
    class BitData final {
    public:
      /// The EC number (0-3)
      Rich::DAQ::ElementaryCell ec;
      /// The PMT number in EC
      Rich::DAQ::PMTInEC pmtInEC;
      /// The Anode index (0-63)
      Rich::DAQ::AnodeIndex anode;

    public:
      /// Default constructor
      BitData() = default;
      /// Constructor from values
      BitData( const Rich::DAQ::ElementaryCell _ec,   ///< ec
               const Rich::DAQ::PMTInEC        _pmt,  ///< pmt
               const Rich::DAQ::AnodeIndex     _anode ///< anode
               )
          : ec( _ec ), pmtInEC( _pmt ), anode( _anode ) {}

    public:
      /// Check if data is valid
      inline constexpr bool isValid() const noexcept {
        return ( ec.isValid() && pmtInEC.isValid() && anode.isValid() );
      }

    public:
      /// ostream operator
      friend std::ostream& operator<<( std::ostream& os, const BitData& bd ) {
        return os << "{ EC=" << bd.ec << " PMTInEC=" << bd.pmtInEC << " Anode=" << bd.anode << " }";
      }
    };

  private:
    // defines

    /// Max Number of frames per PDMDB
    static constexpr const std::size_t FramesPerPDMDB = 6;

    /// Number of PDMDBs per module
    static constexpr const std::size_t PDMDBPerModule = 2;

    ///  Max Number of frames per PDM
    static constexpr const std::size_t FramesPerPDM = PDMDBPerModule * FramesPerPDMDB;

    /// Number of bits per data frame
    static constexpr const std::size_t BitsPerFrame = 86;

    /// Array of Bit Data structs per frame
    using FrameData = std::array<BitData, BitsPerFrame>;

    /// Data for each PDMDB
    using PDMDBData = std::array<FrameData, FramesPerPDMDB>;

    /// Data for each PDM
    using PDMData = std::array<PDMDBData, PDMDBPerModule>;

    ///  R-Type Module data for each RICH
    using RTypeRichData = DetectorArray<PDMData>;

  private:
    // methods

    /// fill R Type PMT anode map data
    void fillRType( const DeRichSystem& richSys );

    /// fill H Type PMT anode map data
    void fillHType( const DeRichSystem& richSys );

  public:
    // accessors

    /// Access the initialisation state
    inline bool isInitialised() const noexcept { return m_isInitialised; }

    /// Get the PDMDB data for given RICH, PDMDB and frame
    inline const auto& getFrameData( const Rich::DetectorType    rich,  //
                                     const Rich::DAQ::PDMDBID    pdmdb, //
                                     const Rich::DAQ::PDMDBFrame link,  //
                                     const bool                  isHType ) const noexcept {
      if ( LIKELY( !isHType ) ) {
        // R type PMT
        assert( (std::size_t)rich < m_pdmDataR.size() );
        assert( (std::size_t)pdmdb.data() < m_pdmDataR[rich].size() );
        assert( (std::size_t)link.data() < m_pdmDataR[rich][pdmdb.data()].size() );
        return m_pdmDataR[rich][pdmdb.data()][link.data()];
      } else {
        assert( (std::size_t)pdmdb.data() < m_pdmDataH.size() );
        assert( (std::size_t)link.data() < m_pdmDataH[pdmdb.data()].size() );
        return m_pdmDataH[pdmdb.data()][link.data()];
      }
    }

    /// Get PDMDB data for given Tel40 data
    inline const auto& getFrameData( const Tel40CableMapping::Tel40LinkData& cData ) const noexcept {
      return getFrameData( cData.smartID.rich(), cData.pdmdbNum, cData.linkNum, cData.isHType );
    }

  public:
    // conditions handling

    /// Creates a condition derivation
    template <typename PARENT>
    static decltype( auto ) addConditionDerivation( PARENT* parent ) {
      // Assume parent algorithm has one and only one input of the correct type...
      return addConditionDerivation( parent, parent->template inputLocation<PDMDBDecodeMapping>() );
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static decltype( auto )                                     //
    addConditionDerivation( PARENT*                     parent, ///< Pointer to parent algorithm
                            LHCb::DetDesc::ConditionKey key     ///< Derived object name
    ) {
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "PDMDBDecodeMapping::addConditionDerivation : Key=" << key << endmsg;
      }
      return LHCb::DetDesc::                                                 //
          addConditionDerivation<PDMDBDecodeMapping( const DeRichSystem& )>( //
              parent->conditionDerivationMgr(),                              //
              std::array{DeRichLocations::RichSystem},                       //
              std::move( key ) );
    }

    /// Default conditions name
    static constexpr const char* DefaultConditionKey = "PDMDBDecodeMapping-Handler";

  private:
    // data

    /// R type data
    RTypeRichData m_pdmDataR;

    /// H type data
    PDMData m_pdmDataH;

    /// Flag to indicate initialisation status
    bool m_isInitialised{false};
  };

} // namespace Rich::Future::DAQ
