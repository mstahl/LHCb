/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// @author Niklas Nolte
// much of that code is stolen from Sebastien Ponce's HLTEventLoopMgr
#pragma once

// The following MUST be included before GaudiKernel/Parsers.h,
// which means very early on in the compilation unit.
#include "CFNodePropertiesParse.h"
#include "Kernel/Chrono.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/IAllocationTracker.h"
#include "ThreadPool.h"
// FW includes
#include "Gaudi/Chrono/ChronoIO.h" // includes "Gaudi/Accumulators.h"
#include "Gaudi/Interfaces/IQueueingEventProcessor.h"
#include "GaudiAlg/FunctionalDetails.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/DataSvc.h"
#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IAlgExecStateSvc.h"
#include "GaudiKernel/IAlgorithm.h"
#include "GaudiKernel/IDataBroker.h"
#include "GaudiKernel/IEvtSelector.h"
#include "GaudiKernel/IHiveWhiteBoard.h"
#include "GaudiKernel/Memory.h"
#include "GaudiKernel/ThreadLocalContext.h"

#include <algorithm>
#include <chrono>
#include <condition_variable>
#include <fstream>
#include <iomanip>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

// tbb
#include "tbb/task_arena.h"
#include "tbb/task_scheduler_observer.h"

// locals
#include "ControlFlowNode.h"

class HLTControlFlowMgr final : public extends<Service, Gaudi::Interfaces::IQueueingEventProcessor> {

public:
  /// Standard Constructor
  using extends::extends;

  StatusCode initialize() override;
  StatusCode reinitialize() override { return StatusCode::FAILURE; }
  StatusCode finalize() override;

  StatusCode nextEvent( int maxevt ) override;
  StatusCode executeEvent( EventContext&& evtContext ) override;
  StatusCode executeRun( int maxevt ) override { return nextEvent( maxevt ); }
  StatusCode stopRun() override;

  // Implementation of IQueueingEventProcessor
  EventContext createEventContext() override;

  void push( EventContext&& evtContext ) override;

  bool empty() const override;

  std::optional<ResultType> pop() override;

private:
  /// Declare the root address of the event
  std::optional<IOpaqueAddress*> declareEventRootAddress();

  /// Method to check if an event failed and take appropriate actions
  StatusCode eventFailed( EventContext const& eventContext ) const;

  /// Algorithm promotion
  void promoteToExecuted( EventContext&& eventContext ) const;

  void buildLines();
  // configuring the execution order
  void configureScheduling();
  // build per-thread state-vector
  void buildNodeStates();

  // helper to release context
  inline StatusCode releaseEvtSelContext() {
    StatusCode sc = StatusCode::SUCCESS;
    auto       c  = m_evtSelContext.release();
    if ( c ) {
      if ( m_evtSelector ) {
        sc = m_evtSelector->releaseContext( c );
      } else {
        delete c;
      }
    }
    return sc;
  }

  // helper to create context
  inline StatusCode createEvtSelContext() {
    auto sc = releaseEvtSelContext();
    if ( sc ) {
      decltype( m_evtSelContext )::pointer c = nullptr;
      sc = ( m_evtSelector ? m_evtSelector->createContext( c ) : StatusCode::FAILURE );
      m_evtSelContext.reset( c );
    }
    return sc;
  }

  // functions to create m_printableDependencyTree
  void registerStructuredTree();
  void registerTreePrintWidth();

private:
  int m_nextevt = 0;

  mutable std::atomic<uint16_t> m_failed_evts_detected = 0;
  mutable bool                  m_shutdown_now         = false;

  std::unique_ptr<ThreadPool> m_debug_pool = nullptr;

private:
  Gaudi::Property<std::string> m_histPersName{this, "HistogramPersistency", "", ""};
  Gaudi::Property<std::string> m_evtsel{this, "EvtSel", "", ""};
  Gaudi::Property<int> m_threadPoolSize{this, "ThreadPoolSize", -1, "Size of the threadpool initialised by TBB"};
  Gaudi::Property<int> m_printFreq{this, "PrintFreq", 1, "Print Frequency for the full algorithm tree"};
  Gaudi::Property<std::vector<NodeDefinition>> m_compositeCFProperties{
      this, "CompositeCFNodes", {}, "Specification of composite CF nodes"};
  Gaudi::Property<std::vector<std::string>> m_BarrierAlgNames{
      this, "BarrierAlgNames", {}, "Names of Barrier (Gather) Algorithms"};
  Gaudi::Property<std::vector<std::vector<std::string>>> m_userDefinedEdges{
      this, "AdditionalCFEdges", {}, "Additional Control Flow Edges defined by the User \
                                      (format: [ [before, after], [before2, after2] ])"};
  Gaudi::Property<std::set<std::string>> m_definitelyRunThese{
      this, "AdditionalAlgs", {}, "Add algs that do not participate in the control flow but should\
                                   definitely run, like e.g. a callgrindprofile"};

  Gaudi::Property<int> m_startTimeAtEvt{this, "StartTimeAtEvt", -1, "start timing at this event. Counting from 0. \
                                        Default choice is deduced from #slots and #evts \
                                        to be reasonably far away from the beginning of processing"};
  Gaudi::Property<int> m_stopTimeAtEvt{this, "StopTimeAtEvt", -1, "stop timing at this event. Counting from 0. \
                                          Default choice is deduced from #slots and #evts \
                                          to be reasonably far away from the end of processing"};

  Gaudi::Property<int> m_stopAfterNFailures{this, "StopAfterNFailures", 3,
                                            "Stop processing if this number of consecutive event failures happened"};

  Gaudi::Property<std::size_t> m_minNameColWidth{this, "MinNameColumnWidth", 46u,
                                                 "Minimum width of component name comlumn in timing table"};
  Gaudi::Property<std::size_t> m_maxNameColWidth{this, "MaxNameColumnWidth", 100u,
                                                 "Maximum width of component name comlumn in timing table"};
  Gaudi::Property<std::size_t> m_estMemoryPoolSize{
      this, "MemoryPoolSize", 10 * 1024 * 1024,
      "Estimated size of each event-local memory pool, in bytes. Set to zero to disable event-local memory pools."};

  // this property is mainly needed to support execution of old algorithms that need to be called via SysExecute like
  // the ones that inherit from DVCommonBase
  Gaudi::Property<bool> m_EnableLegacyMode{
      this, "EnableLegacyMode", false,
      "Call SysExecute of an algorithm. If false algorithms will be called via execute which is faster."};

  /// Reference to the Event Data Service's IDataManagerSvc interface
  IDataManagerSvc* m_evtDataMgrSvc = nullptr;
  /// Reference to the Event Selector
  IEvtSelector* m_evtSelector = nullptr;
  /// Reference to the Histogram Data Service
  IDataManagerSvc* m_histoDataMgrSvc = nullptr;
  /// Reference to the Histogram Persistency Service
  IConversionSvc* m_histoPersSvc = nullptr;
  /// Reference to the Whiteboard
  IHiveWhiteBoard* m_whiteboard = nullptr;
  /// Reference to the AlgExecStateSvc
  IAlgExecStateSvc* m_algExecStateSvc = nullptr;
  // the used databroker
  IDataBroker* m_databroker = nullptr;

  ServiceHandle<IAllocationTracker> m_alloc_tracker{this, "AllocationTrackerService", "TimingAllocationTracker"};

  // the tbb "threadpool"
  std::unique_ptr<tbb::task_arena> m_taskArena;

  /// atomic count of the number of finished events
  mutable std::atomic<uint32_t> m_finishedEvt{0};
  /// condition variable to wake up main thread when we need to create a new event
  mutable std::condition_variable m_createEventCond;
  /// mutex assoiciated with m_createEventCond condition variable
  std::mutex m_createEventMutex;

  /// event selector context
  std::unique_ptr<IEvtSelector::Context> m_evtSelContext;

  // state vectors for each event, once filled, then copied per event
  std::vector<NodeState>                                                                 m_NodeStates;
  std::vector<AlgState>                                                                  m_AlgStates;
  std::vector<Gaudi::Accumulators::AveragingCounter<LHCb::chrono::fast_clock::duration>> m_TimingCounters;
  std::vector<Gaudi::Accumulators::BinomialCounter<uint32_t>>                            m_NodeStateCounters;

  // statistics for the per-event memory pool
  // in principle these members could be removed if
  // LHCb::Allocators::Utils::provides_stats_v<LHCb::Allocators::MonotonicBufferResource>
  // is not true (it's constexpr), but that doesn't seem worth the effort
  mutable Gaudi::Accumulators::BinomialCounter<uint32_t, Gaudi::Accumulators::atomicity::full>
                                                                                           m_memoryPoolMultipleAllocations;
  mutable Gaudi::Accumulators::StatCounter<uint64_t, Gaudi::Accumulators::atomicity::full> m_memoryPoolSize,
      m_memoryPoolBlocks, m_memoryPoolCapacity, m_memoryPoolAllocations;

public:
  using SchedulerStates = std::pair<std::vector<NodeState, LHCb::Allocators::EventLocal<NodeState>>,
                                    std::vector<AlgState, LHCb::Allocators::EventLocal<AlgState>>>;

private:
  // all controlflownodes
  std::map<std::string, VNode> m_allVNodes;
  // all nodes to execute in ordered manner
  std::vector<gsl::not_null<BasicNode*>> m_orderedNodesVec;
  // highest node
  VNode* m_motherOfAllNodes = nullptr;

  std::vector<AlgWrapper> m_definitelyRunTheseAlgs;

  // for printing
  // printable dependency tree (will be built during initialize
  std::vector<std::string> m_printableDependencyTree;
  std::vector<std::string> m_AlgNames;
  // map order of print to order of m_NodeStates and m_allVNodes
  std::vector<int> m_mapPrintToNodeStateOrder;
  // maximum width of the dependency tree
  int m_maxTreeWidth{};

  template <typename Resource>
  void fillMemoryPoolStats( Resource* memResource ) const;

  // runtime adding of states to print tree and states
public:
  template <typename Printable>
  std::stringstream buildPrintableStateTree( LHCb::span<Printable const> states ) const;

  std::stringstream buildAlgsWithStates( LHCb::span<AlgState const> states ) const;

  // to be able to check which states belong to which node (from the outside)
  auto getNodeNamesWithIndices() {
    std::map<std::string, int> names_indices;
    for ( auto const& [_, vnode] : m_allVNodes ) {
      names_indices.emplace( std::visit(
          []( auto const& node ) {
            return std::pair{node.m_name, node.m_NodeID};
          },
          vnode ) );
    }
    return names_indices;
  }

  mutable std::mutex             m_doneMutex;
  mutable std::queue<ResultType> m_done;
};
