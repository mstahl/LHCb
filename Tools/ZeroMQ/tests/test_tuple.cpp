/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <iostream>
#include <tuple>
#include <vector>

#include <ZeroMQ/SerializeSize.h>

int main() {
  auto t = std::make_tuple( 1, 1, 5, "test" );
  std::cout << Serialize::serialize_size( t ) << std::endl;
  auto v = std::vector<decltype( t )>{1, t};
  std::cout << Serialize::serialize_size( v ) << std::endl;
}
