/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <iostream>
#include <vector>

#include <ZeroMQ/functions.h>
#include <dict/ZeroMQDict.h>
#include <zmq/zmq.hpp>

using namespace std;

int main() {

  auto context = zmq::context_t{};

  auto s1 = zmq::socket_t{context, zmq::PAIR};
  s1.bind( "ipc:///tmp/test1" );
  auto s2 = zmq::socket_t{context, zmq::PAIR};
  s2.bind( "ipc:///tmp/test2" );

  auto poller = ZeroMQPoller{};
  auto id_s1  = poller.register_socket( s1, zmq::POLLIN );
  auto id_s2  = poller.register_socket( s2, zmq::POLLIN );

  std::vector<std::pair<size_t, int>> ids;
  while ( true ) {
    auto           ids = poller.poll( -1 );
    zmq::message_t msg;
    for ( const auto& entry : ids ) {
      if ( entry.first == id_s1 && entry.second == zmq::POLLIN ) {
        s1.recv( &msg );
        cout << "received message on s1" << endl;
      }
      if ( entry.first == id_s2 && entry.second == zmq::POLLIN ) {
        s2.recv( &msg );
        cout << "received message on s2" << endl;
      }
    }
  }
}
