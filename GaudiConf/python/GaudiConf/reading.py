###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helpers for configuring an application to read Moore HLT2 output."""
from __future__ import absolute_import
import os

from Configurables import (UnpackMCParticle, UnpackMCVertex,
                           UnpackParticlesAndVertices, HltPackedDataDecoder)

from RawEventFormat import Raw_location_db

from .PersistRecoConf import PersistRecoPacking

__all__ = ["mc_unpackers", "unpackers", "decoders"]


def mc_unpackers(stream='/Event/HLT2', filtered_mc=True):
    """Return a list of unpackers for reading Monte Carlo truth objects.

    Args:
        stream (str): The TES prefix of information saved in Moore.
        filtered_mc (bool): If True, assume Moore saved only a filtered
                            subset of the input MC objects.
    """
    mc_prefix = stream if filtered_mc else "/Event"
    unpack_mcp = UnpackMCParticle(
        InputName=os.path.join(mc_prefix, "pSim/MCParticles"),
        OutputName=os.path.join(mc_prefix, "MC/Particles"),
    )
    unpack_mcv = UnpackMCVertex(
        InputName=os.path.join(mc_prefix, "pSim/MCVertices"),
        OutputName=os.path.join(mc_prefix, "MC/Vertices"),
    )
    return [unpack_mcp, unpack_mcv]


def unpackers(stream='/Event/HLT2', data_type='Upgrade'):
    """Return a list of unpackers for reading reconstructed objects.

    Args:
        stream (str): The TES prefix of information saved in Moore.
        data_type (str): The data type to configure PersistRecoPacking
    """
    prpacking = PersistRecoPacking(stream=stream, data_type=data_type)
    unpack_persistreco = prpacking.unpackers(configurables=True)
    unpack_psandvs = UnpackParticlesAndVertices(InputStream=stream)
    return unpack_persistreco + [unpack_psandvs]


def decoder(stream='/Event/HLT2', raw_event_format=4.3, data_type='Upgrade'):
    """Return a DstData raw bank decoder configured for Turbo data.
    """
    prpacking = PersistRecoPacking(stream=stream, data_type=data_type)
    container_map = prpacking.packedToOutputLocationMap()
    bank_location = Raw_location_db[raw_event_format]["DstData"]
    decoder = HltPackedDataDecoder(
        RawEventLocations=[bank_location],
        ContainerMap=container_map,
    )
    return decoder
