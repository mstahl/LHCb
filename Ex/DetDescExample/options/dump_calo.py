###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###############################################################################
# Test option to load the DD4hep geometry
###############################################################################
# Syntax is:
###############################################################################
import os, sys
from Gaudi.Configuration import *
from Configurables import LHCbApp, ApplicationMgr, DumpCalo
from GaudiConf import IOHelper
from PRConfig.TestFileDB import test_file_db

app = ApplicationMgr()
app.EvtMax = 1
app.EvtSel = "NONE"
app.TopAlg += [DumpCalo()]
LHCbApp().DataType = "Upgrade"
LHCbApp().Simulation = True
