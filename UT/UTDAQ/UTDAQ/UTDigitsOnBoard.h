/*****************************************************************************\
 * (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
/** @class UTDigitsOnBoard UTDigitsOnBoard.h
 *
 *  Helper class for keeping track of digits...
 *
 *  @author Xuhao Yaun (based on code by A Beiter, M Needham)
 *  @date   2020-06-24
 */

#pragma once
#include "Event/UTDigit.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include <algorithm>
#include <array>
#include <utility>
#include <vector>

class UTDigitsOnBoard final {
  // maximum # of lanes is 6 in all UT boards
  constexpr static auto maxNumLanes{6};
  //# of DAQBoard is 216
  constexpr static auto NumUTDAQBoard{216};

public:
  using boardPair   = std::pair<LHCb::UTDigit*, UTDAQID>;
  using DigitVector = std::vector<boardPair>;

  // constructer
  explicit UTDigitsOnBoard( unsigned int nMax ) : m_maxDigitsPerPPx( nMax ) {
    m_digitCont.reserve( UTDigitsOnBoard::NumUTDAQBoard );
    clear();
  }

  void addDigit( LHCb::UTDigit& aDigit );

  const DigitVector& digits() const { return m_digitCont; }

  bool inOverflow() const {
    return std::any_of( m_ppxCount.begin(), m_ppxCount.end(),
                        [&]( unsigned int ppx ) { return ppx >= m_maxDigitsPerPPx; } );
  }

  bool inOverflow( unsigned int ppx ) const { return m_ppxCount[ppx] >= m_maxDigitsPerPPx; };

  std::array<unsigned int, maxNumLanes> nHitsinLine() const { return m_ppxCount; };

  unsigned int maxHitsinLine() const { return *std::max_element( m_ppxCount.begin(), m_ppxCount.end() ); }

  void clear() {
    m_digitCont.clear();
    m_ppxCount.fill( 0 );
  }

private:
  unsigned int                          m_maxDigitsPerPPx{0};
  DigitVector                           m_digitCont;
  std::array<unsigned int, maxNumLanes> m_ppxCount;
};

inline void UTDigitsOnBoard::addDigit( LHCb::UTDigit& aDigit ) {
  UTDAQID            daqChanID( aDigit.getdaqID() );
  const unsigned int ppx = daqChanID.lane();

  if ( !inOverflow( ppx ) ) {
    m_digitCont.insert(
        std::partition_point( m_digitCont.begin(), m_digitCont.end(),
                              [&]( const boardPair& obj ) { return obj.second.id() < daqChanID.id(); } ),
        {&aDigit, daqChanID} );
    ++m_ppxCount[ppx];
  } else {
    // data went into the void
  }
}
