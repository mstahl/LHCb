/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/RawBank.h"
#include "Kernel/STLExtensions.h"
#include "LHCbMath/SIMDWrapper.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTInfo.h"
#include <array>
#include <boost/container/small_vector.hpp>
#include <optional>

namespace LHCb {

  namespace UTDAQ {

    /**
     * counts number of UT clusters in the given raw banks
     * if count exceeds max, it gives up and returns no value
     */
    std::optional<unsigned int> nbUTClusters( LHCb::RawBank::View banks, unsigned int maxNbClusters );

    struct LayerInfo final {
      float z;
      int   nColsPerSide;
      int   nRowsPerSide;
      float invHalfSectorYSize;
      float invHalfSectorXSize;
      float dxDy;
    };

    constexpr static int max_sectors1 = align_size( 896 );
    constexpr static int max_sectors2 = align_size( 1008 );

    struct LUT final {

      std::array<int, max_sectors1> Station1;
      std::array<int, max_sectors2> Station2;

      std::size_t size{0};
      SOA_ACCESSOR( station1, Station1.data() )
      SOA_ACCESSOR( station2, Station2.data() )
    };

    // -- the swapped versions are needed for old versions of the UT geometry, that have a bug
    constexpr static std::array<int, max_sectors1> LUTStation1Swapped = {
        // region 1 (C) 28x12 = 336
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 1, 1, 2, 2, 3, 3,
        4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84,
        // region 2 (B) 28x8 = 224
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 9, 9, 10, 10, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 1, 1, 2, 2, 3,
        3, 4, 4, 5, 5, 7, 7, 8, 8, 11, 11, 12, 12, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22,
        22, 23, 23, 25, 25, 26, 28, 31, 33, 34, 34, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 19, 19, 20, 20, 21, 21, 22,
        22, 23, 23, 24, 24, 27, 29, 30, 32, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44,
        44, 45, 45, 46, 46, 49, 51, 52, 54, 57, 57, 58, 58, 59, 59, 60, 60, 61, 61, 62, 62, 41, 41, 42, 42, 43, 43, 44,
        44, 45, 45, 47, 47, 48, 50, 53, 55, 56, 56, 58, 58, 59, 59, 60, 60, 61, 61, 62, 62, 63, 63, 64, 64, 65, 65, 66,
        66, 67, 67, 69, 69, 70, 70, 73, 73, 74, 74, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 63, 63, 64, 64, 65, 65, 66,
        66, 67, 67, 68, 68, 71, 71, 72, 72, 75, 75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80,
        // region 3 (A)  28x12 = 336
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 1, 1, 2, 2, 3, 3,
        4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84};

    constexpr static std::array<int, max_sectors2> LUTStation2Swapped = {
        // region 1 (C) 28x14 = 392
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 1, 1, 2, 2, 3, 3,
        4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 85, 85, 86, 86, 87, 87, 88, 88, 89,
        89, 90, 90, 91, 91, 92, 92, 93, 93, 94, 94, 95, 95, 96, 96, 97, 97, 98, 98, 85, 85, 86, 86, 87, 87, 88, 88, 89,
        89, 90, 90, 91, 91, 92, 92, 93, 93, 94, 94, 95, 95, 96, 96, 97, 97, 98, 98,
        // region 2 (B) 28x8 = 224
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 9, 9, 10, 10, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 1, 1, 2, 2, 3,
        3, 4, 4, 5, 5, 7, 7, 8, 8, 11, 11, 12, 12, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22,
        22, 23, 23, 25, 25, 26, 28, 31, 33, 34, 34, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 19, 19, 20, 20, 21, 21, 22,
        22, 23, 23, 24, 24, 27, 29, 30, 32, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44,
        44, 45, 45, 46, 46, 49, 51, 52, 54, 57, 57, 58, 58, 59, 59, 60, 60, 61, 61, 62, 62, 41, 41, 42, 42, 43, 43, 44,
        44, 45, 45, 47, 47, 48, 50, 53, 55, 56, 56, 58, 58, 59, 59, 60, 60, 61, 61, 62, 62, 63, 63, 64, 64, 65, 65, 66,
        66, 67, 67, 69, 69, 70, 70, 73, 73, 74, 74, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 63, 63, 64, 64, 65, 65, 66,
        66, 67, 67, 68, 68, 71, 71, 72, 72, 75, 75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80,
        // region 3 (A)  28x14 = 392
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 1, 1, 2, 2, 3, 3,
        4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 85, 85, 86, 86, 87, 87, 88, 88, 89,
        89, 90, 90, 91, 91, 92, 92, 93, 93, 94, 94, 95, 95, 96, 96, 97, 97, 98, 98, 85, 85, 86, 86, 87, 87, 88, 88, 89,
        89, 90, 90, 91, 91, 92, 92, 93, 93, 94, 94, 95, 95, 96, 96, 97, 97, 98, 98};

    constexpr static std::array<int, max_sectors1> LUTStation1 = {
        // region 1 (C) 28x12 = 336
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 1, 1, 2, 2, 3, 3,
        4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84,
        // region 2 (B) 28x8 = 224
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 9, 9, 10, 10, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 1, 1, 2, 2, 3,
        3, 4, 4, 5, 5, 7, 7, 8, 8, 11, 11, 12, 12, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22,
        22, 23, 23, 24, 24, 27, 28, 31, 32, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 19, 19, 20, 20, 21, 21, 22,
        22, 23, 23, 25, 25, 26, 29, 30, 33, 34, 34, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44,
        44, 45, 45, 47, 47, 48, 51, 52, 55, 56, 56, 58, 58, 59, 59, 60, 60, 61, 61, 62, 62, 41, 41, 42, 42, 43, 43, 44,
        44, 45, 45, 46, 46, 49, 50, 53, 54, 57, 57, 58, 58, 59, 59, 60, 60, 61, 61, 62, 62, 63, 63, 64, 64, 65, 65, 66,
        66, 67, 67, 69, 69, 70, 70, 73, 73, 74, 74, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 63, 63, 64, 64, 65, 65, 66,
        66, 67, 67, 68, 68, 71, 71, 72, 72, 75, 75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80,
        // region 3 (A)  28x12 = 336
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 1, 1, 2, 2, 3, 3,
        4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84};

    constexpr static std::array<int, max_sectors2> LUTStation2 = {
        // region 1 (C) 28x14 = 392
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 1, 1, 2, 2, 3, 3,
        4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 85, 85, 86, 86, 87, 87, 88, 88, 89,
        89, 90, 90, 91, 91, 92, 92, 93, 93, 94, 94, 95, 95, 96, 96, 97, 97, 98, 98, 85, 85, 86, 86, 87, 87, 88, 88, 89,
        89, 90, 90, 91, 91, 92, 92, 93, 93, 94, 94, 95, 95, 96, 96, 97, 97, 98, 98,
        // region 2 (B) 28x8 = 224
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 9, 9, 10, 10, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 1, 1, 2, 2, 3,
        3, 4, 4, 5, 5, 7, 7, 8, 8, 11, 11, 12, 12, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22,
        22, 23, 23, 24, 24, 27, 28, 31, 32, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 19, 19, 20, 20, 21, 21, 22,
        22, 23, 23, 25, 25, 26, 29, 30, 33, 34, 34, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44,
        44, 45, 45, 47, 47, 48, 51, 52, 55, 56, 56, 58, 58, 59, 59, 60, 60, 61, 61, 62, 62, 41, 41, 42, 42, 43, 43, 44,
        44, 45, 45, 46, 46, 49, 50, 53, 54, 57, 57, 58, 58, 59, 59, 60, 60, 61, 61, 62, 62, 63, 63, 64, 64, 65, 65, 66,
        66, 67, 67, 69, 69, 70, 70, 73, 73, 74, 74, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 63, 63, 64, 64, 65, 65, 66,
        66, 67, 67, 68, 68, 71, 71, 72, 72, 75, 75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80,
        // region 3 (A)  28x14 = 392
        1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 1, 1, 2, 2, 3, 3,
        4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 15, 15, 16, 16, 17, 17, 18, 18, 19,
        19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 29, 29, 30, 30, 31, 31, 32, 32, 33,
        33, 34, 34, 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 43, 43, 44, 44, 45, 45, 46, 46, 47,
        47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 52, 53, 53, 54, 54, 55, 55, 56, 56, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 57, 57, 58, 58, 59, 59, 60, 60, 61,
        61, 62, 62, 63, 63, 64, 64, 65, 65, 66, 66, 67, 67, 68, 68, 69, 69, 70, 70, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 71, 71, 72, 72, 73, 73, 74, 74, 75,
        75, 76, 76, 77, 77, 78, 78, 79, 79, 80, 80, 81, 81, 82, 82, 83, 83, 84, 84, 85, 85, 86, 86, 87, 87, 88, 88, 89,
        89, 90, 90, 91, 91, 92, 92, 93, 93, 94, 94, 95, 95, 96, 96, 97, 97, 98, 98, 85, 85, 86, 86, 87, 87, 88, 88, 89,
        89, 90, 90, 91, 91, 92, 92, 93, 93, 94, 94, 95, 95, 96, 96, 97, 97, 98, 98};

    using SectorsInRegionZ  = std::array<float, static_cast<int>( UTInfo::DetectorNumbers::Sectors )>;
    using SectorsInLayerZ   = std::array<SectorsInRegionZ, static_cast<int>( UTInfo::DetectorNumbers::Regions )>;
    using SectorsInStationZ = std::array<SectorsInLayerZ, static_cast<int>( UTInfo::DetectorNumbers::Layers )>;

    // -- For the moment, this is assigned here and overwritten in "computeGeometry" in case a geometry
    // -- version with a "wrong" sector ordering is used
    extern std::array<int, 64> mapQuarterSectorToSectorCentralRegion;

    constexpr static const auto mapSectorToSector = std::array{
        1,  2,  3,  4,  5,  0, 0, 0, 0, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 0, 0, 0, 36, 37, 38, 39, 40,
        41, 42, 43, 44, 45, 0, 0, 0, 0, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 0, 0, 0, 0, 76, 77, 78, 79, 80};

    /**
     * fills container of (region, sector) pairs with all sectors concerned by
     * a hit at given layer and coordinates and with given x tolerance
     */
    inline void findSectors( unsigned int layer, float x, float y, float xTol, float yTol, const LayerInfo& info,
                             boost::container::small_vector_base<std::pair<int, int>>& sectors );

    constexpr static int sectorFullChanIdx( unsigned int layer, unsigned int region, unsigned int sector ) {
      const int regions = static_cast<int>( UTInfo::DetectorNumbers::Regions );
      const int sectors = static_cast<int>( UTInfo::DetectorNumbers::Sectors );
      assert( layer <= static_cast<int>( UTInfo::DetectorNumbers::Stations ) *
                           static_cast<int>( UTInfo::DetectorNumbers::Layers ) );
      assert( region != 0 && region <= regions );
      assert( sector != 0 && sector <= sectors );
      return ( layer * regions + region - 1 ) * sectors + sector - 1;
    }

    /**
     * fills container of sector fullChanIdx with all sectors concerned by
     * a hit at given layer and coordinates and with given x tolerance
     */
    inline void findSectorsFullChanIdx( unsigned int layer, float x, float y, float xTol, float yTol,
                                        const LayerInfo& info, boost::container::small_vector_base<int>& sectors ) {
      auto localX = x - info.dxDy * y;
      // deal with sector overlaps and geometry imprecision
      xTol += 1; // mm
      auto localXmin = localX - xTol;
      auto localXmax = localX + xTol;
      int  subcolmin = std::nearbyintf( localXmin * info.invHalfSectorXSize - 0.5 ) + 2 * info.nColsPerSide;
      int  subcolmax = std::nearbyintf( localXmax * info.invHalfSectorXSize - 0.5 ) + 2 * info.nColsPerSide;
      if ( subcolmax < 0 || subcolmin >= (int)( 4 * info.nColsPerSide ) ) {
        // out of acceptance, return empty result
        return;
      }
      // on the acceptance limit
      if ( subcolmax >= (int)( 4 * info.nColsPerSide ) ) subcolmax = (int)( 4 * info.nColsPerSide ) - 1;
      if ( subcolmin < 0 ) subcolmin = 0;
      // deal with sector shifts in tilted layers and overlaps in regular ones
      yTol += ( layer == 1 || layer == 2 ) ? 8 : 1; //  mm
      auto localYmin = y - yTol;
      auto localYmax = y + yTol;
      int  subrowmin = std::nearbyintf( localYmin * info.invHalfSectorYSize - 0.5 ) + 2 * info.nRowsPerSide;
      int  subrowmax = std::nearbyintf( localYmax * info.invHalfSectorYSize - 0.5 ) + 2 * info.nRowsPerSide;
      if ( subrowmax < 0 || subrowmin >= (int)( 4 * info.nRowsPerSide ) ) {
        // out of acceptance, return empty result
        return;
      }
      // on the acceptance limit
      if ( subrowmax >= (int)( 4 * info.nRowsPerSide ) ) subrowmax = (int)( 4 * info.nRowsPerSide ) - 1;
      if ( subrowmin < 0 ) subrowmin = 0;
      for ( int subcol = subcolmin; subcol <= subcolmax; subcol++ ) {
        int region =
            subcol < (int)( 2 * info.nColsPerSide - 4 ) ? 1 : subcol >= (int)( 2 * info.nColsPerSide + 4 ) ? 3 : 2;
        if ( region == 1 ) {
          for ( int subrow = subrowmin; subrow <= subrowmax; subrow++ ) {
            sectors.emplace_back(
                sectorFullChanIdx( layer, 1, ( subcol / 2 ) * info.nRowsPerSide * 2 + subrow / 2 + 1 ) );
          }
        } else if ( region == 2 ) {
          int subcolInReg = subcol - 2 * info.nColsPerSide + 4;
          for ( int subrow = subrowmin; subrow <= subrowmax; subrow++ ) {
            if ( subrow < (int)( 2 * info.nRowsPerSide - 4 ) || subrow >= (int)( 2 * info.nRowsPerSide + 4 ) ) {
              // no in central Region
              sectors.emplace_back(
                  sectorFullChanIdx( layer, 2, mapSectorToSector[( subcolInReg / 2 ) * 14 + ( subrow / 2 )] ) );
            } else {
              // central region
              sectors.emplace_back( sectorFullChanIdx(
                  layer, 2,
                  mapQuarterSectorToSectorCentralRegion[subcolInReg * 8 + subrow - 2 * info.nRowsPerSide + 4] ) );
            }
          }
        } else {
          for ( int subrow = subrowmin; subrow <= subrowmax; subrow++ ) {
            sectors.emplace_back( sectorFullChanIdx(
                layer, 3, ( subcol / 2 - info.nColsPerSide - 2 ) * info.nRowsPerSide * 2 + subrow / 2 + 1 ) );
          }
        }
      }
    }

    struct GeomCache final {
      std::array<LayerInfo, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>      layers;
      std::array<SectorsInStationZ, static_cast<int>( UTInfo::DetectorNumbers::Stations )> sectorsZ;
      LUT                                                                                  sectorLUT;
      GeomCache( const DeUTDetector& );
    };

    //=============================================================================
    // -- find the sectors - SIMD version
    //=============================================================================
    template <typename F, typename I, typename = std::enable_if_t<std::is_same_v<I, typename LHCb::type_map<F>::int_t>>>
    auto findSectors( unsigned int layerIndex, F x, F y, F xTol, F yTol, const LayerInfo& info, I& subcolmin2,
                      I& subcolmax2, I& subrowmin2, I& subrowmax2 ) {

      static_assert( std::is_same_v<F, typename LHCb::type_map<I>::float_t> );
      static_assert( std::is_same_v<I, typename LHCb::type_map<F>::int_t> );
      using M = typename LHCb::type_map<F>::mask_t;

      F localX = x - info.dxDy * y;
      // deal with sector overlaps and geometry imprecision
      xTol        = xTol + F{1.0f}; // mm
      F localXmin = localX - xTol;
      F localXmax = localX + xTol;

      I subcolmin{( localXmin * info.invHalfSectorXSize - 0.5f ) + 2.0f * ( info.nColsPerSide ) + 0.5f}; // faking
                                                                                                         // rounding
      I subcolmax{( localXmax * info.invHalfSectorXSize - 0.5f ) + 2.0f * ( info.nColsPerSide ) + 0.5f}; // faking
                                                                                                         // rounding

      M outOfAcceptanceCol = ( subcolmax < 0 ) || ( subcolmin > I{4 * info.nColsPerSide - 1} );
      // if( all(outOfAcceptanceCol) ) return;

      // deal with sector shifts in tilted layers and overlaps in regular ones
      yTol        = yTol + ( ( layerIndex == 1 || layerIndex == 2 ) ? F{8.0f} : F{1.0f} ); //  mm
      F localYmin = y - yTol;
      F localYmax = y + yTol;
      I subrowmin{( localYmin * info.invHalfSectorYSize - 0.5f ) + 2.0f * ( info.nRowsPerSide ) + 0.5f}; // faking
                                                                                                         // rounding
      I subrowmax{( localYmax * info.invHalfSectorYSize - 0.5f ) + 2.0f * ( info.nRowsPerSide ) + 0.5f}; // faking
                                                                                                         // rounding

      M outOfAcceptanceRow = ( subrowmax < 0 ) || ( subrowmin > I{4 * info.nRowsPerSide - 1} );
      // if( all(outOfAcceptanceRow) ) return;

      // on the acceptance limit
      subcolmin2 = max( subcolmin, I{0} );
      subcolmax2 = min( subcolmax, I{4 * info.nColsPerSide - 1} );

      subrowmin2 = max( subrowmin, I{0} );
      subrowmax2 = min( subrowmax, I{( 4 * info.nRowsPerSide ) - 1} );

      return !outOfAcceptanceCol && !outOfAcceptanceRow;
    }

  } // namespace UTDAQ

} // namespace LHCb
