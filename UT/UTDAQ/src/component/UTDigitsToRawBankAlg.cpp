/*****************************************************************************\
 * (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#include "Event/BankWriter.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/UTDigit.h"
#include "Event/UTSummary.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTCommonBase.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "Kernel/UTRawBankMap.h"
#include "UTDAQ/UTADCWord.h"
#include "UTDAQ/UTBoardToBankMap.h"
#include "UTDAQ/UTDAQFunctor.h"
#include "UTDAQ/UTDigitsOnBoard.h"
#include "UTDAQ/UTHeaderWord.h"
#include <algorithm>
#include <map>
#include <string>
#include <vector>

/** @class DigitsToRawBank DigitsToRawBank.h
 *
 *  Algorithm to fill the Raw buffer with UT information from UTDigits
 *
 *  @author Xuhao Yuan (based on code by A Beiter and M Needham)
 *  @date   2020-06-24
 */

namespace LHCb::UT {

  class DigitsToRawBank : public ::UT::CommonBase<GaudiAlgorithm> {

  public:
    /// Standard constructor
    using ::UT::CommonBase<GaudiAlgorithm>::CommonBase;

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;   ///< Algorithm finalization

  private:
    /// fill the banks
    StatusCode groupByBoard( const LHCb::UTDigits& digitCont );

    unsigned int bankSize( const UTDigitsOnBoard& digitBoard ) const;

    // create a new bank
    void writeBank( const UTDigitsOnBoard& digitCont, LHCb::BankWriter& bWriter );

    Gaudi::Property<int>                 m_maxDigitsPerPPx{this, "maxDigits", 512};
    DataObjectReadHandle<LHCb::RawEvent> m_raw{this, "rawLocation", LHCb::RawEventLocation::Default};
    DataObjectReadHandle<LHCb::UTDigits> m_digits{this, "tightdigitLocation", LHCb::UTDigitLocation::UTTightDigits};

    UTBoardToBankMap m_bankMapping;

    std::map<UTDAQID::BoardID, UTDigitsOnBoard*> m_digitMap;
    std::vector<UTDigitsOnBoard>                 m_digitVectors;

    enum class banksize { even_bit = 16, odd_bit = 0 };
    enum class bits { adc = 0, strip = 5 };
    enum class mask { adc = 0x1F, strip = 0xFFE0 };
  };

  //-----------------------------------------------------------------------------
  // Implementation file for class : DigitsToRawBank
  //
  // 2020-06-24 : Xuhao Yuan (based on codes from M. Needham)
  //-----------------------------------------------------------------------------

  DECLARE_COMPONENT_WITH_ID( DigitsToRawBank, "UTDigitsToRawBankAlg" )

  // Finalisation.
  StatusCode DigitsToRawBank::finalize() {
    m_digitVectors.clear();
    m_bankMapping.clear();
    return StatusCode::SUCCESS;
  }

  // Initialisation.
  StatusCode DigitsToRawBank::initialize() {

    return ::UT::CommonBase<GaudiAlgorithm>::initialize().andThen( [&] {
      // init the map
      unsigned int nBoard = readoutTool()->nBoard();
      m_digitVectors.reserve( nBoard );
      for ( unsigned int iVal = 0; iVal < nBoard; ++iVal ) {
        auto aBoard = readoutTool()->findByDAQOrder( iVal );
        m_bankMapping.addEntry( ( aBoard->boardID() ).board(), iVal );
        m_digitMap[( aBoard->boardID() ).board()] = &m_digitVectors.emplace_back( m_maxDigitsPerPPx );
      } // iVal
    } );
  }

  StatusCode DigitsToRawBank::execute() {

    // Retrieve the RawBank
    RawEvent* tEvent = m_raw.get();

    // intialize temp bank structure each event
    std::for_each( m_digitVectors.begin(), m_digitVectors.end(), []( UTDigitsOnBoard& i ) { i.clear(); } );
    unsigned int n_overflow = 0;

    // group the data by banks..
    StatusCode sc = groupByBoard( *m_digits.get() );
    if ( sc.isFailure() ) { return Error( "Problems linking offline to DAQ channel", sc ); }

    // convert to a bank and add to buffer
    const unsigned int nBoard = readoutTool()->nBoard();
    for ( unsigned int iBoard = 0u; iBoard < nBoard; ++iBoard ) {
      // get the data ....
      const UTDAQID aBoardID = m_bankMapping.findBoard( iBoard );

      if ( m_digitVectors[iBoard].inOverflow() ) ++n_overflow;

      // make the a bankwriter....
      auto bWriter = BankWriter{bankSize( m_digitVectors[iBoard] )};
      writeBank( m_digitVectors[iBoard], bWriter );
      RawBank* tBank =
          tEvent->createBank( UTDAQ::rawInt( aBoardID.board() ), LHCb::RawBank::UT,
                              static_cast<int>( UTDAQ::version::v5 ), bWriter.byteSize(), bWriter.dataBank().data() );
      tEvent->adoptBank( tBank, true );

    } // iBoard

    // flag overflow
    if ( n_overflow > 0 ) Warning( "RawBank overflow some banks truncated" ).ignore();
    return StatusCode::SUCCESS;
  }

  StatusCode DigitsToRawBank::groupByBoard( const UTDigits& digitCont ) {
    // divide up the digits by readout board
    for ( auto digit : digitCont ) {
      UTDAQID utdaqID = readoutTool()->channelIDToDAQID( digit->channelID() );
      auto    iterMap = m_digitMap.find( utdaqID.board() );
      if ( iterMap == m_digitMap.end() ) return Warning( "Failed to find board in map ", StatusCode::FAILURE );
      UTDigitsOnBoard* tVec = iterMap->second;
      tVec->addDigit( *digit );
    } // digitCont
    return StatusCode::SUCCESS;
  }

  unsigned int DigitsToRawBank::bankSize( const UTDigitsOnBoard& digitBoard ) const {
    unsigned int nline = ( digitBoard.maxHitsinLine() + 1 ) / 2;
    return (unsigned int)( 256u / ( sizeof( unsigned int ) * 8u ) ) * nline;
  }

  void DigitsToRawBank::writeBank( const UTDigitsOnBoard& digitBoard, LHCb::BankWriter& bWriter ) {
    auto         n_digithits = digitBoard.nHitsinLine();
    unsigned int nline       = ( digitBoard.maxHitsinLine() + 1 ) / 2;

    for ( unsigned int iline = 0; iline < nline; iline++ ) {
      if ( iline == 0 ) {
        bWriter << ( UTHeaderWord( n_digithits, 1 ) ).value();
        bWriter << ( UTHeaderWord( n_digithits, 2 ) ).value();
      } else {
        bWriter << ( UTHeaderWord() ).value();
        bWriter << ( UTHeaderWord() ).value();
      }
      for ( unsigned int ilane = 0; ilane < 6; ilane++ ) {
        unsigned int n_digit_in_lane = 0;
        if ( ( n_digithits[5 - ilane] + 1 ) / 2 < iline + 1 ) {
          bWriter << static_cast<unsigned int>( 0 );
        } else {
          unsigned int even_value = 0, odd_value = 0;
          for ( const auto& digit : digitBoard.digits() ) {
            UTDigit* aDigit  = digit.first;
            UTDAQID  utdaqID = readoutTool()->channelIDToDAQID( aDigit->channelID() );
            if ( utdaqID.lane() == 5 - ilane ) {
              ++n_digit_in_lane;
              if ( n_digit_in_lane == 2 * ( iline + 1 ) - 1 ) {
                odd_value = ( utdaqID.channel() << static_cast<int>( bits::strip ) ) +
                            ( static_cast<int>( aDigit->depositedCharge() ) << static_cast<int>( bits::adc ) );
              }
              if ( n_digit_in_lane == 2 * ( iline + 1 ) ) {
                even_value = ( utdaqID.channel() << static_cast<int>( bits::strip ) ) +
                             ( static_cast<int>( aDigit->depositedCharge() ) << static_cast<int>( bits::adc ) );
              }
            }
          }
          unsigned int adcvalue = ( even_value << static_cast<unsigned int>( banksize::even_bit ) ) +
                                  ( odd_value << static_cast<unsigned int>( banksize::odd_bit ) );
          bWriter << adcvalue;
        }
      }
    }
  }

} // namespace LHCb::UT
