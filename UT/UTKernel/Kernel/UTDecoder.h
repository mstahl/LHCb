/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/RawBank.h"
#include "Kernel/STLExtensions.h"
#include "SiDAQ/SiRawBankDecoder.h"
#include "SiDAQ/SiRawBufferWord.h"
#include "UTClusterWord.h"
#include "UTDAQ/UTADCWord.h"
#include "UTDAQ/UTHeaderWord.h"
#include <boost/container/small_vector.hpp>
#include <cassert>

namespace UTDAQ {
  /// max Num of Lane in one Board/RawBank
  constexpr unsigned int max_nlane{6};

  typedef std::array<unsigned int, UTDAQ::max_nlane> digiVec;
} // namespace UTDAQ

template <UTDAQ::version>
class UTDecoder;

template <>
struct UTDecoder<UTDAQ::version::v4> : SiRawBankDecoder<UTClusterWord> {
  explicit UTDecoder( const LHCb::RawBank& bank ) : SiRawBankDecoder<UTClusterWord>{bank.data()} {
    assert( UTDAQ::version{bank.version()} == UTDAQ::version::v4 );
  }
};

template <>
class UTDecoder<UTDAQ::version::v5> final {
public:
  explicit UTDecoder( const LHCb::RawBank& bank )
      : m_bank( bank.range<uint16_t>().subspan( 4 ) )
      , m_headerL( bank.range<uint32_t>()[0] )
      , m_headerR( bank.range<uint32_t>()[1] )
      , m_nDigits{m_headerR.nClustersLane0(), m_headerR.nClustersLane1(), m_headerR.nClustersLane2(),
                  m_headerR.nClustersLane3(), m_headerL.nClustersLane4(), m_headerL.nClustersLane5()} {
    assert( UTDAQ::version{bank.version()} == UTDAQ::version::v5 );
  }

  class pos_range final {

  private:
    const LHCb::span<const uint16_t> m_bank;
    UTDAQ::digiVec                   m_Digit;
    struct Sentinel                  final {};

    class Iterator final {
      const LHCb::span<const uint16_t> m_bank;
      UTDAQ::digiVec                   m_iterDigit;
      unsigned int                     m_lane;
      unsigned int                     m_pos;
      unsigned int                     m_maxpos;
      enum class banksize { left_bit = 10, center_bit = 16, right_bit = 15 };

    public:
      Iterator( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec m_Digit ) : m_bank{bank}, m_iterDigit{m_Digit} {
        auto PosLane = std::find_if( m_iterDigit.begin(), m_iterDigit.end(),
                                     []( unsigned int& element ) { return element != 0; } );
        if ( PosLane != m_iterDigit.end() ) {
          m_lane   = ( std::distance( m_iterDigit.begin(), PosLane ) ); // first non-zero lane
          m_pos    = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
          m_maxpos = m_pos +
                     static_cast<unsigned int>( banksize::center_bit ) * ( ( m_iterDigit[m_lane] + 1 ) / 2 - 1 ) +
                     ( 1 - ( m_iterDigit[m_lane] % 2 ) );
        } else {
          m_lane   = UTDAQ::max_nlane;
          m_pos    = 0;
          m_maxpos = 0;
        }
      }

      // dereferencing
      UTADCWord operator*() const { return UTADCWord{m_bank[m_pos], m_lane}; }

      Iterator& operator++() {
        if ( m_pos % 2 ) {
          m_pos += static_cast<unsigned int>( banksize::right_bit );
        } else {
          ++m_pos;
        }
        if ( m_pos > m_maxpos ) {
          ++m_lane;
          while ( m_lane < UTDAQ::max_nlane && m_iterDigit[m_lane] == 0 ) { ++m_lane; }
          if ( m_lane < UTDAQ::max_nlane ) {
            m_pos    = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
            m_maxpos = m_pos +
                       static_cast<unsigned int>( banksize::center_bit ) * ( ( m_iterDigit[m_lane] + 1 ) / 2 - 1 ) +
                       ( 1 - ( m_iterDigit[m_lane] % 2 ) );
          }
        }
        return *this;
      }

      bool operator!=( Sentinel ) const { return m_lane < UTDAQ::max_nlane; }
    };

  public:
    pos_range( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec& ClusterVec )
        : m_bank{std::move( bank )}, m_Digit{ClusterVec} {}
    auto begin() const { return Iterator{m_bank, m_Digit}; }
    auto end() const { return Sentinel{}; }
  };

  class posadc_range final {

  private:
    const LHCb::span<const uint16_t> m_bank;
    UTDAQ::digiVec                   m_Digit;
    struct Sentinel                  final {};

    class Iterator final {
      const LHCb::span<const uint16_t> m_bank;
      UTDAQ::digiVec                   m_iterDigit;
      unsigned int                     m_lane;
      unsigned int                     m_pos;
      unsigned int                     m_maxpos;
      unsigned int                     m_pos_nextBegin;
      enum class banksize { left_bit = 10, center_bit = 16, right_bit = 15 };
      enum class adcbits { adc = 0, strip = 5 };
      enum class adcmask { adc = 0x1F, strip = 0XFFE0 };
      void nextPos() {
        if ( m_pos % 2 ) {
          m_pos += static_cast<unsigned int>( banksize::right_bit );
        } else {
          ++m_pos;
        }
      }
      void keepClustering() {
        unsigned int maxpos = m_pos;
        unsigned int maxAdc = ( m_bank[m_pos] & static_cast<int>( adcmask::adc ) ) >> static_cast<int>( adcbits::adc );
        unsigned int strip =
            ( m_bank[m_pos] & static_cast<int>( adcmask::strip ) ) >> static_cast<int>( adcbits::strip );
        while ( true ) {
          unsigned int stripPrev = strip;
          nextPos();
          if ( m_pos > m_maxpos ) break;
          unsigned int adcvalue =
              ( m_bank[m_pos] & static_cast<int>( adcmask::adc ) ) >> static_cast<int>( adcbits::adc );
          strip = ( m_bank[m_pos] & static_cast<int>( adcmask::strip ) ) >> static_cast<int>( adcbits::strip );
          if ( stripPrev + 1 != strip ) break;
          if ( adcvalue > maxAdc ) {
            maxAdc = adcvalue;
            maxpos = m_pos;
          }
        }
        m_pos_nextBegin = m_pos;
        m_pos           = maxpos;
      }

    public:
      Iterator( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec m_Digit ) : m_bank{bank}, m_iterDigit{m_Digit} {
        auto PosLane = std::find_if( m_iterDigit.begin(), m_iterDigit.end(),
                                     []( unsigned int& element ) { return element != 0; } );
        if ( PosLane != m_iterDigit.end() ) {
          m_lane   = ( std::distance( m_iterDigit.begin(), PosLane ) ); // first non-zero lane
          m_pos    = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
          m_maxpos = m_pos +
                     static_cast<unsigned int>( banksize::center_bit ) * ( ( m_iterDigit[m_lane] + 1 ) / 2 - 1 ) +
                     ( 1 - ( m_iterDigit[m_lane] % 2 ) );
          keepClustering();
        } else {
          m_lane          = UTDAQ::max_nlane;
          m_pos           = 0;
          m_maxpos        = 0;
          m_pos_nextBegin = 0;
        }
      }

      // dereferencing
      UTADCWord operator*() const { return UTADCWord{m_bank[m_pos], m_lane}; }

      Iterator& operator++() {
        m_pos = m_pos_nextBegin;

        keepClustering();

        if ( m_pos > m_maxpos ) {
          ++m_lane;
          while ( m_lane < UTDAQ::max_nlane && m_iterDigit[m_lane] == 0 ) { ++m_lane; }
          if ( m_lane < UTDAQ::max_nlane ) {
            m_pos_nextBegin = static_cast<unsigned int>( banksize::left_bit ) - 2 * m_lane;
            m_maxpos        = m_pos_nextBegin +
                       static_cast<unsigned int>( banksize::center_bit ) * ( ( m_iterDigit[m_lane] + 1 ) / 2 - 1 ) +
                       ( 1 - ( m_iterDigit[m_lane] % 2 ) );
            m_pos = m_pos_nextBegin;
            keepClustering();
          }
        }
        return *this;
      }

      bool operator!=( Sentinel ) const { return m_lane < UTDAQ::max_nlane; }
    };

  public:
    posadc_range( LHCb::span<const uint16_t> bank, const UTDAQ::digiVec& ClusterVec )
        : m_bank{std::move( bank )}, m_Digit{ClusterVec} {}
    auto begin() const { return Iterator{m_bank, m_Digit}; }
    auto end() const { return Sentinel{}; }
  };

  const UTHeaderWord& headerL() const { return m_headerL; }
  const UTHeaderWord& headerR() const { return m_headerR; }

  inline unsigned int nClusters() const {
    return std::max( std::max( std::max( m_nDigits[0], m_nDigits[1] ), std::max( m_nDigits[2], m_nDigits[3] ) ),
                     std::max( m_nDigits[4], m_nDigits[5] ) );
  }
  inline unsigned int nClusters( const unsigned int& laneID ) const { return m_nDigits[laneID]; }

  auto posRange() const { return pos_range{m_bank.first( ( ( nClusters() + 1 ) / 2 ) * 16 - 4 ), m_nDigits}; }

  auto posAdcRange() const { return posadc_range{m_bank.first( ( ( nClusters() + 1 ) / 2 ) * 16 - 4 ), m_nDigits}; }

private:
  LHCb::span<const uint16_t> m_bank;
  UTHeaderWord               m_headerL;
  UTHeaderWord               m_headerR;
  UTDAQ::digiVec             m_nDigits;
};
