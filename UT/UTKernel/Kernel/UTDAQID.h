/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <iostream>

/** @class UTDAQID UTDAQID.h "UTDAQ/UTDAQID.h"
 *
 *  Class to describe a UT DAQ component with an ID
 *
 *  Combines Board ID (which half of which TELL40), LaneID (which lane inside raw data block), and channel number (ASIC
 * + strip number ) to describe a UT DAQ channel that can have hits
 *
 *  @author M. Rudolph
 *  @date   2020-02-13
 */

class UTDAQID final {

public:
  enum BoardID : uint32_t;
  enum LaneID : uint32_t;
  enum ChannelID : uint16_t;

  // constructor for a board ID, and a specific lane in that board's output
  UTDAQID( BoardID boardID ) { m_id = ( boardID << static_cast<int>( bits::board ) ); }

  // constructor for a board ID, and a specific lane in that board's output
  UTDAQID( BoardID boardID, LaneID laneIdx ) {
    m_id = ( boardID << static_cast<int>( bits::board ) ) + ( laneIdx << static_cast<int>( bits::lane ) );
  }

  // full constructor with channel number in the lane
  UTDAQID( BoardID boardID, LaneID laneIdx, ChannelID channelNum ) {
    m_id = ( boardID << static_cast<int>( bits::board ) ) + ( laneIdx << static_cast<int>( bits::lane ) ) +
           ( channelNum << static_cast<int>( bits::channel ) );
  }

  explicit UTDAQID( unsigned int id ) : m_id( id ) {}

  /// Default Constructor
  UTDAQID() = default;

  /// board
  BoardID board() const;

  /// lane
  LaneID lane() const;

  /// channel
  ChannelID channel() const;

  /// comparison equality
  friend bool operator==( const UTDAQID& lhs, const UTDAQID& rhs ) { return lhs.id() == rhs.id(); }

  /// comparison <
  friend bool operator<( const UTDAQID& lhs, const UTDAQID& rhs ) { return lhs.id() < rhs.id(); }

  /// Retrieve UT DAQ ID as plain unsigned int
  unsigned int id() const;

  /// Operator overloading for stringoutput
  friend std::ostream& operator<<( std::ostream& s, const UTDAQID& obj ) { return obj.fillStream( s ); }

  // Fill the ASCII output stream
  std::ostream& fillStream( std::ostream& s ) const;

  /** print method for python Not needed in C++ */
  std::string toString() const;

  enum General { nullBoard = 0x000fffff };

private:
  enum class bits { channel = 0, lane = 9, board = 12 }; /// Enumeration to store the bit packing offsets
  enum class masks { channel = 0x000001ff, lane = 0x00000e00, board = 0x000ff000 };

  unsigned int m_id = 0; ///< UTDAQID
};

#include <sstream>
#include <string>

inline std::string UTDAQID::toString() const {
  std::ostringstream o;
  fillStream( o );
  return o.str();
}

inline unsigned int UTDAQID::id() const { return m_id; }

inline UTDAQID::BoardID UTDAQID::board() const {
  return static_cast<BoardID>( ( m_id & static_cast<int>( masks::board ) ) >> static_cast<int>( bits::board ) );
}

inline UTDAQID::LaneID UTDAQID::lane() const {
  return static_cast<LaneID>( ( m_id & static_cast<int>( masks::lane ) ) >> static_cast<int>( bits::lane ) );
}

inline UTDAQID::ChannelID UTDAQID::channel() const {
  return static_cast<ChannelID>( ( m_id & static_cast<int>( masks::channel ) ) >> static_cast<int>( bits::channel ) );
}

inline std::ostream& UTDAQID::fillStream( std::ostream& s ) const {
  s << "{ UTDAQID: " << id() << " board: " << uint16_t( board() ) << " lane: " << uint16_t( lane() )
    << " channel : " << uint16_t( channel() ) << " }";
  return s;
}
