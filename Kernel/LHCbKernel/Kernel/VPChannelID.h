/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP

#  include "Detector/VP/VPChannelID.h"
namespace LHCb {
  using VPChannelID = LHCb::Detector::VPChannelID;
}

#else

#  include <ostream>

namespace LHCb {

  // Forward declarations

  /** @class VPChannelID VPChannelID.h
   *
   * This class identifies a single pixel in the VP
   *
   * @author Victor Coco
   *
   */

  class VPChannelID final {
  public:
    /// Default Constructor
    constexpr VPChannelID() = default;

    /// Constructor with channelID
    constexpr explicit VPChannelID( unsigned int id ) : m_channelID( id ) {}

    /// Constructor with sensor, chip, column and row
    constexpr VPChannelID( unsigned int sensor, unsigned int chip, unsigned int col, unsigned int row )
        : VPChannelID{( sensor << sensorBits ) | ( chip << chipBits ) | ( col << colBits ) | row} {}

    /// Cast
    constexpr operator unsigned int() const { return m_channelID; }

    /// Get sensor column number
    [[nodiscard]] constexpr unsigned int scol() const { return ( m_channelID & ( chipMask | colMask ) ) >> colBits; }

    /// Get module number
    [[nodiscard]] constexpr unsigned int module() const { return sensor() / 4; }

    /// Get station number
    [[nodiscard]] constexpr unsigned int station() const { return module() / 2; }

    /// Get side (left/right)
    [[nodiscard]] constexpr unsigned int sidepos() const { return module() % 2; }

    /// Special serializer to ASCII stream
    std::ostream& fillStream( std::ostream& s ) const;

    /// Retrieve const  VP Channel ID
    [[nodiscard]] constexpr unsigned int channelID() const { return m_channelID; }

    /// Update  VP Channel ID
    [[deprecated]] constexpr VPChannelID& setChannelID( unsigned int value ) {
      m_channelID = value;
      return *this;
    }

    /// Retrieve pixel row
    [[nodiscard]] constexpr unsigned int row() const { return (unsigned int)( ( m_channelID & rowMask ) >> rowBits ); }

    /// Update pixel row
    void setRow( unsigned int value );

    /// Retrieve pixel column
    [[nodiscard]] constexpr unsigned int col() const { return (unsigned int)( ( m_channelID & colMask ) >> colBits ); }

    /// Update pixel column
    void setCol( unsigned int value );

    /// Retrieve chip number
    [[nodiscard]] constexpr unsigned int chip() const { return ( m_channelID & chipMask ) >> chipBits; }

    /// Update chip number
    void setChip( unsigned int value );

    /// Retrieve sensor number
    [[nodiscard]] constexpr unsigned int sensor() const { return ( m_channelID & sensorMask ) >> sensorBits; }

    /// Update sensor number
    void setSensor( unsigned int value );

    friend std::ostream& operator<<( std::ostream& str, const VPChannelID& obj ) { return obj.fillStream( str ); }

  private:
    /// Offsets of bitfield channelID
    enum channelIDBits { rowBits = 0, colBits = 8, chipBits = 16, sensorBits = 18 };

    /// Bitmasks for bitfield channelID
    enum channelIDMasks { rowMask = 0xffL, colMask = 0xff00L, chipMask = 0x30000L, sensorMask = 0xffc0000L };

    unsigned int m_channelID{0}; ///< VP Channel ID

  }; // class VPChannelID

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline void LHCb::VPChannelID::setRow( unsigned int value ) {
  auto val = (unsigned int)value;
  m_channelID &= ~rowMask;
  m_channelID |= ( ( ( (unsigned int)val ) << rowBits ) & rowMask );
}

inline void LHCb::VPChannelID::setCol( unsigned int value ) {
  auto val = (unsigned int)value;
  m_channelID &= ~colMask;
  m_channelID |= ( ( ( (unsigned int)val ) << colBits ) & colMask );
}

inline void LHCb::VPChannelID::setChip( unsigned int value ) {
  auto val = (unsigned int)value;
  m_channelID &= ~chipMask;
  m_channelID |= ( ( ( (unsigned int)val ) << chipBits ) & chipMask );
}

inline void LHCb::VPChannelID::setSensor( unsigned int value ) {
  auto val = (unsigned int)value;
  m_channelID &= ~sensorMask;
  m_channelID |= ( ( ( (unsigned int)val ) << sensorBits ) & sensorMask );
}

#endif
