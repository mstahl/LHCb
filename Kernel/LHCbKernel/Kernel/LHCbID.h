/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Kernel/CaloCellID.h"
#include "Kernel/FTChannelID.h"
#include "Kernel/HCCellID.h"
#include "Kernel/MuonTileID.h"
#include "Kernel/OTChannelID.h"
#include "Kernel/RichSmartID.h"
#include "Kernel/STChannelID.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/VPChannelID.h"
#include "Kernel/VeloChannelID.h"
#include <ostream>

namespace LHCb {

  /** @class LHCbID LHCbID.h
   *
   * LHCb wide channel identifier
   *
   * @author Marco Cattaneo
   *
   */

  class LHCbID final {
  public:
    /// types of sub-detector channel ID
    enum class channelIDtype { Velo = 1, TT, IT, OT, Rich, Calo, Muon, VP, FT = 10, UT, HC };

    /// Default Constructor
    constexpr LHCbID() = default;

    /// Constructor from unsigned int
    explicit constexpr LHCbID( unsigned int theID ) : m_lhcbID( theID ) {}

    /// Constructor from type and unsigned int id
    constexpr LHCbID( channelIDtype t, unsigned int id )
        : m_lhcbID{( ( static_cast<unsigned int>( t ) << detectorTypeBits ) & detectorTypeMask ) | ( id & IDMask )} {
      assert( ( id & IDMask ) == id );
    }

    /// allow SIMD implementations to work with the bit representation....
    template <typename int_v>
    static constexpr auto make( channelIDtype t, int_v v ) {
      return ( ( static_cast<unsigned int>( t ) << detectorTypeBits ) & detectorTypeMask ) + v;
    }

    /// Constructor from VeloChannelID
    constexpr LHCbID( VeloChannelID chanID ) : LHCbID{channelIDtype::Velo, chanID} {}

    /// Constructor from VPChannelID, VP corresponding to pixel solution for upgrade
    constexpr LHCbID( VPChannelID chanID ) : LHCbID{channelIDtype::VP, chanID} {}

    /// Constructor from STChannelID
    constexpr LHCbID( const STChannelID& chanID )
        : LHCbID{chanID.isIT() ? channelIDtype::IT : channelIDtype::TT, chanID} {}

    /// Constructor from UTChannelID
    constexpr LHCbID( UTChannelID chanID ) : LHCbID{channelIDtype::UT, chanID} {}

    /// Constructor from OTChannelID
    constexpr LHCbID( OTChannelID chanID ) : LHCbID{channelIDtype::OT, chanID} {}

    /// Constructor from RichSmartID
    constexpr LHCbID( const RichSmartID& chanID );

    /// Constructor from CaloCellID
    constexpr LHCbID( CaloCellID chanID ) : LHCbID{channelIDtype::Calo, chanID.all()} {}

    /// Constructor from MuonTileID
    constexpr LHCbID( MuonTileID chanID ) : LHCbID{channelIDtype::Muon, chanID} {}

    /// Constructor from FTChannelID
    constexpr LHCbID( FTChannelID chanID ) : LHCbID{channelIDtype::FT, chanID} {}

    /// Constructor from HCCellID, HC standing for Herschel
    constexpr LHCbID( HCCellID cellID ) : LHCbID{channelIDtype::HC, cellID} {}

    /// comparison equality
    constexpr friend bool operator==( LHCbID lhs, LHCbID rhs ) { return lhs.lhcbID() == rhs.lhcbID(); }

    /// comparison ordering
    constexpr friend bool operator<( LHCbID lhs, LHCbID rhs ) { return lhs.lhcbID() < rhs.lhcbID(); }

    /// return true if this is a Velo identifier
    constexpr bool isVelo() const { return channelIDtype::Velo == detectorType(); }

    /// return true if this is a Velo R identifier
    constexpr bool isVeloR() const { return isVelo() && veloID().isRType(); }

    /// return true if this is a Velo Phi identifier
    constexpr bool isVeloPhi() const { return isVelo() && veloID().isPhiType(); }

    /// return true if this is a Velo Pile up identifier
    constexpr bool isVeloPileUp() const { return isVelo() && veloID().isPileUp(); }

    /// return the VeloChannelID
    constexpr VeloChannelID veloID() const;

    /// return true if this is a VP identifier
    constexpr bool isVP() const { return channelIDtype::VP == detectorType(); }

    /// return the VPChannelID
    constexpr VPChannelID vpID() const;

    /// return true if this is a TT Silicon Tracker identifier
    constexpr bool isTT() const { return channelIDtype::TT == detectorType(); }

    /// return true if this is a UT Silicon Tracker identifier
    constexpr bool isUT() const { return channelIDtype::UT == detectorType(); }

    /// return the UTChannelID
    constexpr UTChannelID utID() const;

    /// return true if this is a IT Silicon Tracker identifier
    constexpr bool isIT() const { return channelIDtype::IT == detectorType(); }

    /// return true if this is a Silicon Tracker identifier (i.e. TT, IT or UT)
    constexpr bool isST() const { return isTT() || isIT(); }

    /// return the STChannelID
    constexpr STChannelID stID() const;

    /// return true if this is a Outer Tracker identifier
    constexpr bool isOT() const { return ( channelIDtype::OT == detectorType() ); }

    /// return the OTChannelID
    constexpr OTChannelID otID() const;

    /// return true if this is a Rich identifier
    constexpr bool isRich() const { return channelIDtype::Rich == detectorType(); }

    /// return the richSmartID
    constexpr RichSmartID richID() const;

    /// return true if this is a Calo identifier
    constexpr bool isCalo() const { return channelIDtype::Calo == detectorType(); }

    /// return the CaloCellID
    constexpr CaloCellID caloID() const;

    /// return true if this is a Muon identifier
    constexpr bool isMuon() const { return channelIDtype::Muon == detectorType(); }

    /// return the MuonTileID
    constexpr MuonTileID muonID() const;

    /// return true if this is a Fibre Tracker identifier
    constexpr bool isFT() const { return channelIDtype::FT == detectorType(); }

    /// return the FTChannelID
    constexpr FTChannelID ftID() const;

    /// return true if this is a HC identifier
    constexpr bool isHC() const { return channelIDtype::HC == detectorType(); }

    /// return the HCCellID
    constexpr HCCellID hcID() const;

    /// Check the LHCbID sub-detector channel ID type identifier
    constexpr bool checkDetectorType( LHCbID::channelIDtype type ) const { return type == detectorType(); }

    /// General ID: returns detector ID = internal unsigned int
    constexpr unsigned int channelID() const;

    /// Print this LHCbID in a human readable way
    std::ostream& fillStream( std::ostream& s ) const;

    /// Retrieve const  the internal representation
    constexpr unsigned int lhcbID() const { return m_lhcbID; }

    /// Update the ID bits (to recreate the channelID)
    constexpr LHCbID& setID( unsigned int value ) {
      m_lhcbID &= ~IDMask;
      m_lhcbID |= value & IDMask;
      return *this;
    }

    /// Retrieve the LHCb detector type bits
    constexpr channelIDtype detectorType() const {
      return channelIDtype( ( m_lhcbID & detectorTypeMask ) >> detectorTypeBits );
    }

    /// Update the LHCb detector type bits
    constexpr LHCbID& setDetectorType( channelIDtype value ) {
      unsigned int val = static_cast<unsigned int>( value );
      m_lhcbID &= ~detectorTypeMask;
      m_lhcbID |= ( val << detectorTypeBits ) & detectorTypeMask;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const LHCbID& obj ) { return obj.fillStream( str ); }

  private:
    /// Offsets of bitfield lhcbID
    static constexpr auto detectorTypeBits = 28;

    /// Checks if a given bit is set
    constexpr bool testBit( unsigned int pos ) const { return ( 0 != ( lhcbID() & ( 1 << pos ) ) ); }

    /// Sets the given bit to the given value
    LHCbID& setBit( unsigned int pos, unsigned int value ) {
      m_lhcbID |= value << pos;
      return *this;
    }

    /// Bitmasks for bitfield lhcbID
    enum lhcbIDMasks { IDMask = 0xfffffffL, detectorTypeMask = 0xf0000000L };

    unsigned int m_lhcbID = 0; ///< the internal representation

  }; // class LHCbID

  inline std::ostream& operator<<( std::ostream& s, LHCbID::channelIDtype e ) {
    switch ( e ) {
    case LHCbID::channelIDtype::Velo:
      return s << "Velo";
    case LHCbID::channelIDtype::TT:
      return s << "TT";
    case LHCbID::channelIDtype::IT:
      return s << "IT";
    case LHCbID::channelIDtype::OT:
      return s << "OT";
    case LHCbID::channelIDtype::Rich:
      return s << "Rich";
    case LHCbID::channelIDtype::Calo:
      return s << "Calo";
    case LHCbID::channelIDtype::Muon:
      return s << "Muon";
    case LHCbID::channelIDtype::VP:
      return s << "VP";
    case LHCbID::channelIDtype::FT:
      return s << "FT";
    case LHCbID::channelIDtype::UT:
      return s << "UT";
    case LHCbID::channelIDtype::HC:
      return s << "HC";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCbID::channelIDtype";
    }
  }

  // -----------------------------------------------------------------------------
  // end of class
  // -----------------------------------------------------------------------------

  // Including forward declarations

  constexpr LHCbID::LHCbID( const RichSmartID& chanID ) : LHCbID{channelIDtype::Rich, chanID.dataBitsOnly()} {
    // Save the MaPMT/HPD flag in bit 27
    setBit( 27, chanID.idType() );
    // Set the validity bits
    setBit( 26, chanID.pixelDataAreValid() );
  }

  constexpr VeloChannelID LHCbID::veloID() const {
    return ( isVelo() ? VeloChannelID( m_lhcbID & IDMask ) : VeloChannelID( 0xF0000000 ) );
  }

  constexpr VPChannelID LHCbID::vpID() const { return VPChannelID( isVP() ? m_lhcbID & IDMask : 0xF0000000 ); }

  constexpr UTChannelID LHCbID::utID() const {
    return ( isUT() ? UTChannelID( m_lhcbID & IDMask ) : UTChannelID( 0xF0000000 ) );
  }

  constexpr STChannelID LHCbID::stID() const { return STChannelID( isST() ? m_lhcbID & IDMask : 0xF0000000 ); }

  constexpr OTChannelID LHCbID::otID() const { return OTChannelID( isOT() ? m_lhcbID & IDMask : 0xF0000000 ); }

  constexpr RichSmartID LHCbID::richID() const {
    // Create the RichSMartID data bits
    RichSmartID::KeyType data( isRich() ? ( m_lhcbID & IDMask ) : 0 );
    // Create a temporary RichSmartID
    RichSmartID tmpid( data );
    // Retrieve the MaPMT/HPD flag
    if ( isRich() ) { tmpid.setIDType( static_cast<RichSmartID::IDType>( testBit( 27 ) ) ); }
    // Object to return, with RICH and panel fields set
    RichSmartID id( tmpid.rich(), tmpid.panel(), tmpid.pdNumInCol(), tmpid.pdCol(), tmpid.idType() );
    // Set pixels fields
    if ( testBit( 26 ) ) {
      id.setPixelRow( tmpid.pixelRow() );
      id.setPixelCol( tmpid.pixelCol() );
      if ( tmpid.idType() == RichSmartID::HPDID ) { id.setPixelSubRow( tmpid.pixelSubRow() ); }
    }
    // return
    return id;
  }

  constexpr CaloCellID LHCbID::caloID() const {
    return isCalo() ? CaloCellID( m_lhcbID & IDMask ) : CaloCellID( 0xF0000000 );
  }

  constexpr MuonTileID LHCbID::muonID() const {
    return isMuon() ? MuonTileID( m_lhcbID & IDMask ) : MuonTileID( 0xF0000000 );
  }

  constexpr FTChannelID LHCbID::ftID() const {
    return isFT() ? FTChannelID( m_lhcbID & IDMask ) : FTChannelID( 0xF0000000 );
  }

  constexpr HCCellID LHCbID::hcID() const { return isHC() ? HCCellID( m_lhcbID & IDMask ) : HCCellID( 0xF0000000 ); }

  constexpr unsigned int LHCbID::channelID() const {

    switch ( detectorType() ) {
    case channelIDtype::VP:
      return vpID().channelID();
    case channelIDtype::Velo:
      return veloID().channelID();
    case channelIDtype::UT:
      return utID().channelID();
    case channelIDtype::TT: // C++17 [[fall-through]]
    case channelIDtype::IT:
      return stID().channelID();
    case channelIDtype::OT:
      return otID().channelID();
    case channelIDtype::Rich:
      return richID().key();
    case channelIDtype::Calo: // C++17 [[ fall-through ]]
    case channelIDtype::Muon:
      return m_lhcbID & IDMask;
    case channelIDtype::FT:
      return ftID().channelID();
    case channelIDtype::HC:
      return hcID().cellID();
    default:
      return 0;
    }
  }

} // namespace LHCb
