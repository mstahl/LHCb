/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichDetectorType.h
 *
 *  Header file for RICH particle ID enumeration : RichDetectorType
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   08/07/2004
 */
//-----------------------------------------------------------------------------

#pragma once

// std include
#include <array>
#include <cstdint>
#include <iostream>
#include <string>

// Boost
#include <boost/container/small_vector.hpp>
#include <boost/version.hpp>

// Gaudi
#include "GaudiKernel/SerializeSTL.h"

// Hack to work around cling issue
#define N_DETECTOR_TYPES 2

//  General namespace for RICH specific definitions documented in RichSide.h
namespace Rich {

  /// Number of RICH detectors
  inline constexpr std::uint16_t NRiches = N_DETECTOR_TYPES;

  /** @enum Rich::DetectorType
   *
   *  RICH Detector types
   *
   *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
   *  @date   08/07/2004
   */
  enum DetectorType : std::int8_t {
    InvalidDetector = -1, ///< Unspecified Detector
    Rich1           = 0,  ///< RICH1 detector
    Rich2           = 1,  ///< RICH2 detector
    Rich            = 1   ///< Single RICH detector
  };

  /// Type for fixed size arrays for data for each RICH
  template <typename TYPE>
  using DetectorArray = std::array<TYPE, NRiches>;

  /// Type for container of detector types
  // Explicitly specify boost::container::small_vector default template arguments
  // to sidestep cling error, see lhcb/LHCb#75
#if BOOST_VERSION < 107100
  using Detectors = boost::container::small_vector<DetectorType, N_DETECTOR_TYPES, void>;
#else
  using Detectors = boost::container::small_vector<DetectorType, N_DETECTOR_TYPES, void, void>;
#endif

  /// Access all valid detector types
  inline Detectors detectors() noexcept { return {Rich::Rich1, Rich::Rich2}; }

  /** Text conversion for DetectorType enumeration
   *
   *  @param detector RICH detector enumeration
   *  @return Detector type as a string
   */
  std::string text( const Rich::DetectorType detector );

  /// Implement textual ostream << method for Rich::DetectorType enumeration
  inline std::ostream& operator<<( std::ostream& s, const Rich::DetectorType& detector ) {
    return s << Rich::text( detector );
  }

  /// Print a vector of Detector IDs
  inline std::ostream& operator<<( std::ostream& str, const Detectors& dets ) {
    return GaudiUtils::details::ostream_joiner( str << '[', dets, ", " ) << ']';
  }

} // namespace Rich
