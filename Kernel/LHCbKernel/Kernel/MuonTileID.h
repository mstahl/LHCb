/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Kernel/MuonBase.h"
#include "Kernel/MuonLayout.h"
#include <charconv>
#include <ostream>
#include <string_view>
#include <utility>

namespace LHCb {

  /** @class MuonTileID MuonTileID.h
   *
   * Universal identifier for the Muon System entities
   *
   * @author Andrei Tsaregorodtsev
   *
   */

  class MuonTileID final {
  public:
    /// Default constructor
    constexpr MuonTileID() = default;

    /// Constructor taking a long needed for the keyed container
    constexpr explicit MuonTileID( unsigned int id ) : m_muonid{id} {}

    /// Constructor with all the arguments
    constexpr MuonTileID( int station, const IMuonLayout& lay, int region, int quarter, int x, int y ) {
      setStation( station );
      setRegion( region );
      setQuarter( quarter );
      setX( x );
      setY( y );
      setLayout( MuonLayout( lay.grid( *this ) ) );
    }

    /// Constructor from similar MuonTileID
    constexpr MuonTileID( MuonTileID id, const unsigned int region, const unsigned int quarter, const unsigned int x,
                          const unsigned int y )
        : m_muonid{id.m_muonid} {
      setQuarter( quarter );
      setRegion( region );
      setX( x );
      setY( y );
    }

    /// Constructor from relative position in the containing MuonTile MuonTileID
    MuonTileID( const MuonTileID& id, const IMuonLayout& layout, const unsigned int x, const unsigned int y );

    /// Constructor from a string representation of the MuonTileID
    explicit MuonTileID( std::string_view id );

    /// convert the MuonTileID to an int for use as a key
    constexpr operator unsigned int() const { return m_muonid; }

    /// Function to extract station
    [[nodiscard]] constexpr unsigned int station() const {
      return ( m_muonid & MuonBase::MaskStation ) >> MuonBase::ShiftStation;
    }

    /// Function to extract region
    [[nodiscard]] constexpr unsigned int region() const {
      return ( m_muonid & MuonBase::MaskRegion ) >> MuonBase::ShiftRegion;
    }

    /// Function to extract quarter
    [[nodiscard]] constexpr unsigned int quarter() const {
      return ( m_muonid & MuonBase::MaskQuarter ) >> MuonBase::ShiftQuarter;
    }

    /// Function to extract layout
    [[nodiscard]] MuonLayout layout() const { return {xGrid(), yGrid()}; }

    /// Check if layout is horizontal
    [[nodiscard]] constexpr bool isHorizontal() const { return xGrid() > yGrid(); }

    /// Function to extract index in x
    [[nodiscard]] constexpr unsigned int nX() const { return ( m_muonid & MuonBase::MaskX ) >> MuonBase::ShiftX; }

    /// Function to extract index in y
    [[nodiscard]] constexpr unsigned int nY() const { return ( m_muonid & MuonBase::MaskY ) >> MuonBase::ShiftY; }

    /// Function to extract lower part of the identifier
    [[nodiscard]] constexpr unsigned int index() const {
      return ( m_muonid & MuonBase::MaskIndex ) >> MuonBase::ShiftIndex;
    }

    /// Function to extract lower part of the identifier with station information
    [[nodiscard]] constexpr unsigned int key() const { return ( m_muonid & MuonBase::MaskKey ) >> MuonBase::ShiftKey; }

    /// comparison operator using key.
    [[nodiscard]] friend constexpr bool operator<( MuonTileID lhs, MuonTileID rhs ) { return lhs.key() < rhs.key(); }

    /// equality operator using key.
    [[nodiscard]] friend constexpr bool operator==( MuonTileID lhs, MuonTileID rhs ) {
      return lhs.m_muonid == rhs.m_muonid;
    }

    /// non-equality operator using key.
    [[nodiscard]] friend constexpr bool operator!=( MuonTileID lhs, MuonTileID rhs ) { return !( lhs == rhs ); }

    /// Find the MuonTileID which is an interception of two MuonTileID's
    [[nodiscard]] MuonTileID intercept( MuonTileID id ) const;

    /// Find the MuonTileID which is an interception of two MuonTileID's
    [[nodiscard]] MuonTileID interceptSameRegion( MuonTileID id ) const;

    /// Find the MuonTileID of a Tile of a given layout containing this pad
    [[nodiscard]] MuonTileID containerID( const IMuonLayout& layout ) const;

    /// Find the MuonTileID of a Tile which is a neighbour of this pad
    [[nodiscard]] MuonTileID neighbourID( int dirX, int dirY ) const;

    /// Get local offset X with respect to the container defined by the given IMuonLayout
    [[nodiscard]] int localX( const IMuonLayout& layout ) const;

    /// Get local offset y with respect to the container defined by the given IMuonLayout
    [[nodiscard]] int localY( const IMuonLayout& layout ) const;

    /// Check that the MuonTileID is consistent in terms of its layout
    [[nodiscard]] constexpr bool isValid() const;

    /// Check that the MuonTileID is defined. It means that its code is not 0
    [[nodiscard]] constexpr bool isDefined() const { return m_muonid != 0; }

    /// presents the MuonTileID as a readable string
    [[nodiscard]] std::string toString() const;

    /// update station identifier
    constexpr MuonTileID& setStation( const unsigned int station ) {
      return set( station, MuonBase::ShiftStation, MuonBase::MaskStation );
    }

    /// update region identifier
    constexpr MuonTileID& setRegion( const unsigned int region ) {
      return set( region, MuonBase::ShiftRegion, MuonBase::MaskRegion );
    }

    /// update quarter identifier
    constexpr MuonTileID& setQuarter( const unsigned int quarter ) {
      return set( quarter, MuonBase::ShiftQuarter, MuonBase::MaskQuarter );
    }

    /// update index in x
    constexpr MuonTileID& setX( const unsigned int x ) { return set( x, MuonBase::ShiftX, MuonBase::MaskX ); }

    /// update index in y
    constexpr MuonTileID& setY( const unsigned int y ) { return set( y, MuonBase::ShiftY, MuonBase::MaskY ); }

    /// update layout identifier
    constexpr MuonTileID& setLayout( const MuonLayout& layout ) { return setLayout( layout.xGrid(), layout.yGrid() ); }

    /// modify index in x
    constexpr MuonTileID& deltaX( int dx ) { return setX( nX() + dx ); }

    /// modify index in y
    constexpr MuonTileID& deltaY( int dy ) { return setY( nY() + dy ); }

    /// Print this MuonTileID in a human readable way
    std::ostream& fillStream( std::ostream& s ) const;

    friend std::ostream& operator<<( std::ostream& str, const MuonTileID& obj ) { return obj.fillStream( str ); }

  private:
    constexpr MuonTileID& set( unsigned int Value, unsigned int Shift, unsigned int Mask ) {
      m_muonid = ( ( Value << Shift ) & Mask ) | ( m_muonid & ~Mask );
      return *this;
    }
    constexpr unsigned xGrid() const { return ( m_muonid & MuonBase::MaskLayoutX ) >> MuonBase::ShiftLayoutX; }
    constexpr unsigned yGrid() const { return ( m_muonid & MuonBase::MaskLayoutY ) >> MuonBase::ShiftLayoutY; }

    /// update layout identifier
    constexpr MuonTileID& setLayout( unsigned lx, unsigned ly ) {
      set( ( ly << MuonBase::BitsLayoutX ) | lx, MuonBase::ShiftLayoutX,
           MuonBase::MaskLayoutY | MuonBase::MaskLayoutX );
      return *this;
    }

    unsigned int m_muonid{0}; ///< muon tile id

  }; // class MuonTileID

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

inline LHCb::MuonTileID::MuonTileID( std::string_view s ) {
  // assumed format of strid:  "S{}({},{})Q{},R{},{},{}"

  constexpr auto process = []( std::string_view prefix, std::string_view sv ) {
    if ( sv.substr( 0, prefix.size() ) != prefix ) throw std::invalid_argument{"MuonTileID: bad format"};
    sv.remove_prefix( prefix.size() );
    long l;
    auto [ptr, ec] = std::from_chars( sv.begin(), sv.end(), l );
    if ( ec != std::errc{} ) throw std::invalid_argument{"MuonTileID: bad format"};
    return std::pair{sv.substr( ptr - sv.begin() ), l};
  };

  auto p = process( "S", s );
  setStation( p.second );
  p            = process( "(", p.first );
  unsigned mlx = p.second;
  p            = process( ",", p.first );
  unsigned mly = p.second;
  setLayout( MuonLayout{mlx, mly} );
  p = process( ")Q", p.first );
  setQuarter( p.second );
  p = process( ",R", p.first );
  setRegion( p.second );
  p = process( ",", p.first );
  setX( p.second );
  p = process( ",", p.first );
  setY( p.second );
}

constexpr bool LHCb::MuonTileID::isValid() const {
  if ( !isDefined() ) return false;
  auto nx = nX();
  auto ny = nY();
  auto xg = xGrid();
  auto yg = yGrid();
  return ( ( ny >= yg && ny < 2 * yg ) || ( nx >= xg && ny < yg ) ) && nx < 2 * xg;
}
