/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  /** @class FTChannelID FTChannelID.h
   *
   * Channel ID for the Fibre Tracker (LHCb Upgrade)
   *
   * @author FT software team
   *
   */

  class FTChannelID final {
  public:
    /// Default Constructor
    constexpr FTChannelID() = default;

    /// Constructor from int
    constexpr explicit FTChannelID( unsigned int id ) : m_channelID{id} {}

    /// Explicit constructor from the geometrical location
    constexpr FTChannelID( unsigned int station, unsigned int layer, unsigned int quarter, unsigned int module,
                           unsigned int mat, unsigned int sipm, unsigned int channel )
        : FTChannelID{( station << stationBits ) + ( layer << layerBits ) + ( quarter << quarterBits ) +
                      ( module << moduleBits ) + ( mat << matBits ) + ( sipm << sipmBits ) +
                      ( channel << channelBits )} {}

    /// Explicit constructor from the geometrical location
    constexpr FTChannelID( unsigned int station, unsigned int layer, unsigned int quarter, unsigned int module,
                           unsigned int channelInModule )
        : FTChannelID{( station << stationBits ) + ( layer << layerBits ) + ( quarter << quarterBits ) +
                      ( module << moduleBits ) + ( channelInModule << channelBits )} {}

    /// Operator overload, to cast channel ID to unsigned int.                         Used by linkers where the key
    /// (channel id) is an int
    constexpr operator unsigned int() const { return m_channelID; }

    /// Comparison equality
    constexpr friend bool operator==( FTChannelID& lhs, FTChannelID& rhs ) {
      return lhs.channelID() == rhs.channelID();
    }

    /// Comparison <
    constexpr friend bool operator<( FTChannelID& lhs, FTChannelID& rhs ) { return lhs.channelID() < rhs.channelID(); }

    /// Comparison >
    constexpr friend bool operator>( FTChannelID& lhs, FTChannelID& rhs ) { return rhs < lhs; }

    /// Increment the channelID
    constexpr FTChannelID& next() {
      ++m_channelID;
      return *this;
    }

    /// Increment the channelID
    constexpr FTChannelID& addToChannel( int offset ) {
      m_channelID += offset;
      return *this;
    }

    /// Return the SiPM number within the module (0-15)
    [[nodiscard]] constexpr unsigned int sipmInModule() const {
      return ( m_channelID & ( matMask + sipmMask ) ) >> sipmBits;
    }

    /// Return the die number (0 or 1)
    [[nodiscard]] constexpr unsigned int die() const { return ( m_channelID & 0x40 ) >> 6; }

    /// Return true if channelID is in x-layer
    [[nodiscard]] constexpr bool isX() const { return ( layer() == 0 || layer() == 3 ); }

    /// Return true if channelID is in bottom part of detector
    [[nodiscard]] constexpr bool isBottom() const { return ( quarter() == 0 || quarter() == 1 ); }

    /// Return true if channelID is in top part of detector
    [[nodiscard]] constexpr bool isTop() const { return ( quarter() == 2 || quarter() == 3 ); }

    /// Print this FTChannelID in a human readable way
    std::ostream& fillStream( std::ostream& s ) const {
      return s << "{ FTChannelID : "
               << " channel =" << channel() << " sipm =" << sipm() << " mat =" << mat() << " module=" << module()
               << " quarter=" << quarter() << " layer=" << layer() << " station=" << station() << " }";
    }

    /// Retrieve const  FT Channel ID
    [[nodiscard]] constexpr unsigned int channelID() const { return m_channelID; }

    /// Update  FT Channel ID
    [[deprecated]] constexpr FTChannelID& setChannelID( unsigned int value ) {
      m_channelID = value;
      return *this;
    }

    /// Retrieve Channel in the 128 channel SiPM
    [[nodiscard]] constexpr unsigned int channel() const { return ( m_channelID & channelMask ) >> channelBits; }

    /// Retrieve ID of the SiPM in the mat
    [[nodiscard]] constexpr unsigned int sipm() const { return ( m_channelID & sipmMask ) >> sipmBits; }

    /// Retrieve ID of the mat in the module
    [[nodiscard]] constexpr unsigned int mat() const { return ( m_channelID & matMask ) >> matBits; }

    /// Retrieve Module id (0 - 5 or 0 - 6)
    [[nodiscard]] constexpr unsigned int module() const { return ( m_channelID & moduleMask ) >> moduleBits; }

    /// Retrieve Quarter ID (0 - 3)
    [[nodiscard]] constexpr unsigned int quarter() const { return ( m_channelID & quarterMask ) >> quarterBits; }

    /// Retrieve Layer id
    [[nodiscard]] constexpr unsigned int layer() const { return ( m_channelID & layerMask ) >> layerBits; }

    /// Retrieve Station id
    [[nodiscard]] constexpr unsigned int station() const { return ( m_channelID & stationMask ) >> stationBits; }

    /// Retrieve unique layer
    [[nodiscard]] constexpr unsigned int uniqueLayer() const { return ( m_channelID & uniqueLayerMask ) >> layerBits; }

    /// Retrieve unique quarter
    [[nodiscard]] constexpr unsigned int uniqueQuarter() const {
      return ( m_channelID & uniqueQuarterMask ) >> quarterBits;
    }

    /// Retrieve unique module
    [[nodiscard]] constexpr unsigned int uniqueModule() const {
      return ( m_channelID & uniqueModuleMask ) >> moduleBits;
    }

    /// Retrieve unique mat
    [[nodiscard]] constexpr unsigned int uniqueMat() const { return ( m_channelID & uniqueMatMask ) >> matBits; }

    /// Retrieve unique SiPM
    [[nodiscard]] constexpr unsigned int uniqueSiPM() const { return ( m_channelID & uniqueSiPMMask ) >> sipmBits; }

    /// Retrieve moduleID for monitoring
    [[nodiscard]] constexpr unsigned int moniModuleID() const {
      return 6 * ( 4 * ( 4 * ( station() - 1 ) + layer() ) + quarter() ) + module();
    }

    /// Retrieve moduleID unique per station for monitoring
    [[nodiscard]] constexpr unsigned int moniModuleIDstation() const { return module() + 4 * quarter() + 16 * layer(); }

    /// Retrieve quarterID for monitoring
    [[nodiscard]] constexpr unsigned int moniQuarterID() const {
      return 4 * layer() + quarter() + 16 * ( station() - 1 );
    }

    /// Retrieve SiPMID for monitoring
    [[nodiscard]] constexpr unsigned int moniSiPMID() const { return sipm() + 4 * mat() + 20 * module(); }

    /// Retrieve channelID for monitoring
    [[nodiscard]] constexpr unsigned int moniChannelID() const { return mat() + 4 * sipm() + 16 * channel(); }

    friend std::ostream& operator<<( std::ostream& str, const FTChannelID& obj ) { return obj.fillStream( str ); }

  private:
    /// Offsets of bitfield channelID
    enum channelIDBits {
      channelBits = 0,
      sipmBits    = 7,
      matBits     = 9,
      moduleBits  = 11,
      quarterBits = 14,
      layerBits   = 16,
      stationBits = 18
    };

    /// Bitmasks for bitfield channelID
    enum channelIDMasks {
      channelMask       = 0x7fL,
      sipmMask          = 0x180L,
      matMask           = 0x600L,
      moduleMask        = 0x3800L,
      quarterMask       = 0xc000L,
      layerMask         = 0x30000L,
      stationMask       = 0xc0000L,
      uniqueLayerMask   = layerMask + stationMask,
      uniqueQuarterMask = quarterMask + layerMask + stationMask,
      uniqueModuleMask  = moduleMask + quarterMask + layerMask + stationMask,
      uniqueMatMask     = matMask + moduleMask + quarterMask + layerMask + stationMask,
      uniqueSiPMMask    = sipmMask + matMask + moduleMask + quarterMask + layerMask + stationMask
    };

    unsigned int m_channelID{0}; ///< FT Channel ID

  }; // class FTChannelID

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------
