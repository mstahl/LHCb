/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Kernel/HeaderMapping.h"
#include <boost/version.hpp>

// ROOT/Cling has problems with std::variant in [at least] the version shipped
// in LCG_97a. Fortunately that version of LCG is new enough to contain a
// version of Boost that includes boost::variant2::variant, which ROOT/Cling
// seems to handle better.
#if BOOST_VERSION >= 107200
#  define LHCB_VARIANT_USE_BOOST_VARIANT2
#  include <boost/variant2/variant.hpp>
#endif

#include <tuple>
#include <variant>

namespace LHCb {
  /** Utilities for defining operations that optionally operate on variant
   *  types.
   */
  template <typename T>
  struct is_variant : std::false_type {
    using tuple_type = std::tuple<T>;
  };

  template <typename... Ts>
  struct is_variant<std::variant<Ts...>> : std::true_type {
    using tuple_type = std::tuple<Ts...>;
  };

#ifdef LHCB_VARIANT_USE_BOOST_VARIANT2
  template <typename... Ts>
  struct is_variant<boost::variant2::variant<Ts...>> : std::true_type {
    using tuple_type = std::tuple<Ts...>;
  };
#endif

  template <typename T>
  inline constexpr bool is_variant_v = is_variant<T>::value;

  /** Alias that forwards to boost::variant2::variant or std::variant depending
   *  on compile-time checks. See https://gitlab.cern.ch/lhcb/Rec/-/issues/124
   */
  template <typename... Ts>
  using variant =
#ifdef LHCB_VARIANT_USE_BOOST_VARIANT2
      boost::variant2::variant<Ts...>;
#else
      std::variant<Ts...>;
#endif

  /** Check that the given list of types is not empty and contains only variant
   *  types.
   */
  template <typename... Variants>
  inline constexpr bool are_all_variants_v = ( sizeof...( Variants ) > 0 ) && ( is_variant_v<Variants> && ... );

  /** Use the given callable as a visitor for variant arguments, or simply
   *  invoke it if the arguments are not variants.
   */
  template <typename F, typename... Args>
  decltype( auto ) invoke_or_visit( F&& f, Args&&... args ) {
    if constexpr ( are_all_variants_v<std::decay_t<Args>...> ) {
      // std::visit should both be found by ADL
      return visit( std::forward<F>( f ), std::forward<Args>( args )... );
    } else {
      return std::invoke( std::forward<F>( f ), std::forward<Args>( args )... );
    }
  }

  template <typename... Ts>
  using invoke_or_visit_result_t = decltype( invoke_or_visit( std::declval<Ts>()... ) );
} // namespace LHCb

template <typename... Ts>
struct LHCb::header_map<std::variant<Ts...>> {
  constexpr static auto value = ( LHCb::header_map_v<Ts> + ... ) + "<variant>";
};

#ifdef LHCB_VARIANT_USE_BOOST_VARIANT2
template <typename... Ts>
struct LHCb::header_map<boost::variant2::variant<Ts...>> {
  constexpr static auto value = ( LHCb::header_map_v<Ts> + ... ) + "<boost/variant2/variant.hpp>";
};
#endif