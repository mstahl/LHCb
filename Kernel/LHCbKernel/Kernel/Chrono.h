/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <chrono>

#ifdef __x86_64__
#  include <x86intrin.h>
#endif

namespace LHCb {
  namespace chrono {
#ifdef __x86_64__
    struct fast_clock {
      using rep                   = uint64_t;
      using period                = std::ratio<1, 1000000000>;
      using duration              = std::chrono::duration<rep, period>;
      using time_point            = std::chrono::time_point<fast_clock>;
      static const bool is_steady = true;

      static time_point now() noexcept {
        return time_point( duration( static_cast<uint64_t>( __rdtsc() * tsc_ratio ) ) );
      }

      static const uint64_t tsc_frequency;
      static const double   tsc_ratio;
    };
#else
    using fast_clock = std::chrono::steady_clock;
#endif
  } // namespace chrono
} // namespace LHCb