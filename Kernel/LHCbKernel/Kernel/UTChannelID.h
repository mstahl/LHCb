/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  /** @class UTChannelID UTChannelID.h
   *
   * Channel ID for class for UT
   *
   * @author A Beiter (based on code by M Needham, J. Wang)
   *
   */

  class UTChannelID final {
  public:
    /// types of sub-detector channel ID
    enum detType { typeUT = 2 };

    /// Default Constructor
    constexpr UTChannelID() = default;

    /// constructor with int
    constexpr explicit UTChannelID( unsigned int id ) : m_channelID( id ) {}

    /// constructor with station, layer, detRegion, sector , strip,
    UTChannelID( const unsigned int iType, const unsigned int iStation, const unsigned int iLayer,
                 const unsigned int iDetRegion, const unsigned int iSector, const unsigned int iStrip )
        : UTChannelID{( iType << typeBits ) + ( iStation << stationBits ) + ( iLayer << layerBitsUT ) +
                      ( iDetRegion << detRegionBitsUT ) + ( iSector << sectorBitsUT ) + ( iStrip << stripBits )} {}

    /// cast
    constexpr operator unsigned int() const { return m_channelID; }

    /// Retrieve type
    [[nodiscard]] constexpr unsigned int type() const { return ( m_channelID & typeMask ) >> typeBits; }

    /// test whether UT or not
    [[nodiscard]] constexpr bool isUT() const { return type() == LHCb::UTChannelID::detType::typeUT; }

    /// Retrieve sector
    [[nodiscard]] constexpr unsigned int sector() const { return ( m_channelID & sectorMaskUT ) >> sectorBitsUT; }

    /// Retrieve detRegion
    [[nodiscard]] constexpr unsigned int detRegion() const {
      return ( m_channelID & detRegionMaskUT ) >> detRegionBitsUT;
    }

    /// Retrieve layer
    [[nodiscard]] constexpr unsigned int layer() const { return ( m_channelID & layerMaskUT ) >> layerBitsUT; }

    /// Retrieve unique layer
    [[nodiscard]] constexpr unsigned int uniqueLayer() const {
      return ( m_channelID & uniqueLayerMaskUT ) >> layerBitsUT;
    }

    /// Retrieve unique detRegion
    [[nodiscard]] constexpr unsigned int uniqueDetRegion() const {
      return ( m_channelID & uniqueDetRegionMaskUT ) >> detRegionBitsUT;
    }

    /// Print this UTChannelID in a human readable way
    std::ostream& fillStream( std::ostream& s ) const;

    /// Print method for python NOT NEEDED + SLOW IN C++ use fillStream
    [[nodiscard]] std::string toString() const;

    /// Retrieve const  UT Channel ID
    [[nodiscard]] constexpr unsigned int channelID() const { return m_channelID; }

    /// Update  UT Channel ID
    [[deprecated]] constexpr UTChannelID& setChannelID( unsigned int value ) {
      m_channelID = value;
      return *this;
    }

    /// Retrieve strip
    [[nodiscard]] constexpr unsigned int strip() const { return ( m_channelID & stripMask ) >> stripBits; }

    /// Retrieve station
    [[nodiscard]] constexpr unsigned int station() const { return ( m_channelID & stationMask ) >> stationBits; }

    /// Retrieve unique sector
    [[nodiscard]] constexpr unsigned int uniqueSector() const {
      return ( m_channelID & uniqueSectorMask ) >> sectorBits;
    }

    friend std::ostream& operator<<( std::ostream& str, const UTChannelID& obj ) { return obj.fillStream( str ); }

  private:
    /// Offsets of UT bitfield that are different from TT/IT
    enum channelIDBitsUT { sectorBitsUT = 10, detRegionBitsUT = 17, layerBitsUT = 19 };
    /// Bitmasks for UT bitfield that are different from TT/IT
    enum channelIDMasksUT {
      sectorMaskUT          = 0x1fc00L,
      detRegionMaskUT       = 0x60000L,
      layerMaskUT           = 0x180000L,
      stationMaskUT         = 0x600000L,
      uniqueLayerMaskUT     = layerMaskUT + stationMaskUT,
      uniqueDetRegionMaskUT = detRegionMaskUT + layerMaskUT + stationMaskUT
    };

    /// Offsets of bitfield channelID
    enum channelIDBits {
      stripBits     = 0,
      sectorBits    = 10,
      detRegionBits = 15,
      layerBits     = 18,
      stationBits   = 21,
      typeBits      = 23
    };

    /// Bitmasks for bitfield channelID
    enum channelIDMasks {
      stripMask           = 0x3ffL,
      sectorMask          = 0x7c00L,
      detRegionMask       = 0x38000L,
      layerMask           = 0x1c0000L,
      stationMask         = 0x600000L,
      typeMask            = 0x1800000L,
      uniqueLayerMask     = layerMask + stationMask,
      uniqueDetRegionMask = detRegionMask + layerMask + stationMask,
      uniqueSectorMask    = sectorMask + detRegionMask + layerMask + stationMask
    };

    unsigned int m_channelID{0}; ///< UT Channel ID

  }; // class UTChannelID

  inline std::ostream& operator<<( std::ostream& s, LHCb::UTChannelID::detType e ) {
    switch ( e ) {
    case LHCb::UTChannelID::typeUT:
      return s << "typeUT";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::UTChannelID::detType";
    }
  }

} // namespace LHCb
