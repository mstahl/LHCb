<?xml version="1.0" ?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="options"><text>
from Gaudi.Configuration import *
from Configurables import (DDDBConf, CondDB, EventClockSvc, FakeEventTime,
                           DQFilter, GaudiSequencer,
                           DQAcceptTool, BasicDQFilter, CondDBDQScanner)
from Configurables import DetCondTest__TestConditionAlg as TestCond
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
import os

DDDBConf()

conddb = CondDB()
conddb.addLayer(os.environ.get('TEST_DQFLAGS_ROOT'))
conddb.Tags["DDDB"] = "dddb-20171030-2"

ecs = EventClockSvc()
ecs.addTool(FakeEventTime, "EventTimeDecoder")
ecs.EventTimeDecoder.StartTime = 4e9 # seconds
ecs.EventTimeDecoder.TimeStep =  5e9

dqf = DQFilter(OutputLevel = VERBOSE)
dqf.AcceptTool = DQAcceptTool(OutputLevel = VERBOSE) # public tool
dqf.AcceptTool.addTool(BasicDQFilter, "Filter")
dqf.AcceptTool.addTool(CondDBDQScanner, "Scanner")
dqf.AcceptTool.Scanner.ConditionPath = "/Conditions/DQ/Flags" # path in the DB

testCond = TestCond("TestCond")
testCond.Conditions = [ "Conditions/DQ/Flags" ]

app = ApplicationMgr()
app.TopAlg = [ GaudiSequencer(Members = [dqf]) ]
app.EvtSel = "NONE"
app.EvtMax = 10
app.ExtSvc += [MessageSvcSink()]

</text></argument>
<argument name="reference"><text>../refs/dqfilter-run-basic.ref</text></argument>
<argument name="validator">
  <text>
from GaudiConf.QMTest.LHCbExclusions import preprocessor, LineSkipper
preprocessor = preprocessor + LineSkipper(["using checked out files in"])
validateWithReference(preproc = preprocessor)
  </text>
</argument>
</extension>
