/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/AllocationTracker.h"
#include "Kernel/Chrono.h"

#include <cstddef>
#include <cstdlib>
#include <dlfcn.h>
#include <functional>
#include <mutex>
#include <stdexcept>
#include <string>

using namespace AllocationTracker;

namespace {
  constexpr std::size_t global_buffer_size{8192};
  constexpr std::size_t alignment{alignof( std::max_align_t )};
  constexpr std::size_t align_up( std::size_t n ) { return ( n + ( alignment - 1 ) ) & ~( alignment - 1 ); }

  alignas( alignment ) std::byte global_buffer[global_buffer_size];
  std::byte* global_buffer_pos{global_buffer};

  template <Function>
  struct overriden_helper {};

  template <Function>
  void record_stats( std::size_t, uint64_t );

  // List of functions to override inspired by
  // https://microsoft.github.io/mimalloc/overrides.html

  template <>
  struct overriden_helper<MALLOC> {
    static constexpr auto name = "malloc";
    static void*          local( size_t size ) {
      auto aligned_size = align_up( size );
      if ( global_buffer_pos + aligned_size > global_buffer + global_buffer_size ) {
        throw std::runtime_error( "bad local_malloc" );
      } else {
        return std::exchange( global_buffer_pos, global_buffer_pos + aligned_size );
      }
    }
    static void stats( std::size_t n, uint64_t time ) { record_stats<MALLOC>( n, time ); }
  };

  template <>
  struct overriden_helper<FREE> {
    static constexpr auto name = "free";
    static void           local( void* ) {} // allows the signature to be deduced starting from 'FREE'
  };

  template <>
  struct overriden_helper<CALLOC> {
    static constexpr auto name = "calloc";
    static void* local( std::size_t size, std::size_t n ) { return overriden_helper<MALLOC>::local( size * n ); }
    static void  stats( std::size_t size, std::size_t n, uint64_t time ) { record_stats<CALLOC>( size * n, time ); }
  };

  template <>
  struct overriden_helper<REALLOC> {
    static constexpr auto name = "realloc";
    static void*          local( void*, std::size_t ) { throw std::runtime_error( "local realloc not implemented" ); }
    static void           stats( void*, std::size_t size, uint64_t time ) { record_stats<REALLOC>( size, time ); }
  };

  template <>
  struct overriden_helper<POSIX_MEMALIGN> {
    static constexpr auto name = "posix_memalign";
    static int            local( void**, std::size_t, std::size_t ) {
      throw std::runtime_error( "local posix_memalign not implemented" );
    }
    static void stats( void**, std::size_t, std::size_t size, uint64_t time ) {
      record_stats<POSIX_MEMALIGN>( size, time );
    }
  };

  template <>
  struct overriden_helper<MEMALIGN> {
    static constexpr auto name = "memalign";
    static void* local( std::size_t, std::size_t ) { throw std::runtime_error( "local memalign not implemented" ); }
    static void  stats( std::size_t, std::size_t size, uint64_t time ) { record_stats<MEMALIGN>( size, time ); }
  };

  template <>
  struct overriden_helper<ALIGNED_ALLOC> {
    static constexpr auto name = "aligned_alloc";
    static void*          local( std::size_t, std::size_t ) {
      throw std::runtime_error( "local aligned_alloc not implemented" );
    }
    static void stats( std::size_t, std::size_t size, uint64_t time ) { record_stats<ALIGNED_ALLOC>( size, time ); }
  };

  template <>
  struct overriden_helper<VALLOC> {
    static constexpr auto name = "valloc";
    static void*          local( std::size_t ) { throw std::runtime_error( "local valloc not implemented" ); }
    static void           stats( std::size_t size, uint64_t time ) { record_stats<VALLOC>( size, time ); }
  };

  template <>
  struct overriden_helper<PVALLOC> {
    static constexpr auto name = "pvalloc";
    static void*          local( std::size_t ) { throw std::runtime_error( "local pvalloc not implemented" ); }
    static void           stats( std::size_t size, uint64_t time ) { record_stats<PVALLOC>( size, time ); }
  };

  template <Function Caller>
  using function_ptr_t = decltype( &overriden_helper<Caller>::local );

  template <typename T>
  T get_original( const char* name ) {
    void* sym = dlsym( RTLD_NEXT, name );
    if ( !sym ) { throw std::runtime_error( std::string{"Failed to retrieve symbol: "} + name ); }
    return reinterpret_cast<T>( sym );
  }

  template <Function Caller>
  struct original_function_info {
    std::mutex             modifying_original;
    bool                   inside_first_call{false};
    function_ptr_t<Caller> original_ptr{nullptr};
  };

  template <Function Caller>
  original_function_info<Caller>& get_info() {
    static original_function_info<Caller> info;
    return info;
  }

  /** Get the original function pointer. Returns nullptr if it's not available yet.
   */
  template <Function Caller>
  function_ptr_t<Caller> try_get_original() {
    auto& info = get_info<Caller>();
    if ( !info.original_ptr ) {
      // see if we can get the lock and load it
      if ( info.modifying_original.try_lock() ) {
        // maybe we got it in the meantime?
        if ( !info.original_ptr ) {
          info.inside_first_call = true;
          info.original_ptr      = get_original<function_ptr_t<Caller>>( overriden_helper<Caller>::name );
          info.inside_first_call = false;
        }
        info.modifying_original.unlock();
      }
    }
    return info.original_ptr;
  }

  /** Get the original function pointer. Blocks waiting for the mutex if need be.
   */
  template <Function Caller>
  function_ptr_t<Caller> get_original() {
    auto& info = get_info<Caller>();
    if ( !info.original_ptr ) {
      std::lock_guard _{info.modifying_original};
      // maybe we got it in the meantime?
      if ( !info.original_ptr ) {
        info.inside_first_call = true;
        info.original_ptr      = get_original<function_ptr_t<Caller>>( overriden_helper<Caller>::name );
        info.inside_first_call = false;
      }
    }
    return info.original_ptr;
  }

  template <Function Caller>
  void record_stats( std::size_t size, uint64_t time ) {
    // Very early on in initialisation (e.g. loading the original malloc() calls calloc() which calls calloc_stats())
    // we might not have access to both of these. Just skip recording statistics in that case.
    auto original_malloc = try_get_original<MALLOC>();
    if ( !original_malloc ) { return; }
    auto original_free = try_get_original<FREE>();
    if ( !original_free ) { return; }
    auto callback = getCallback();
    if ( !callback ) { return; }
    callback( Caller, size, time, original_malloc, original_free );
  }

  template <Function Caller, typename... Args>
  std::invoke_result_t<function_ptr_t<Caller>, Args...> wrap_library_call( Args&&... args ) {
    auto& info = get_info<Caller>();
    if ( info.original_ptr ) {
      // Everything is already initialised -- this should almost always be the case
      const auto       start           = LHCb::chrono::fast_clock::now();
      decltype( auto ) original_result = std::invoke( info.original_ptr, std::forward<Args>( args )... );
      std::invoke( overriden_helper<Caller>::stats, std::forward<Args>( args )...,
                   ( LHCb::chrono::fast_clock::now() - start ).count() );
      return original_result;
    } else if ( info.inside_first_call ) {
      // this means that we're in the process of retrieving the original function, but dlsym() has needed to allocate...
      // don't count this local stuff in the stats, we probably wouldn't be able to provide the callback with malloc()
      // and free() pointers anyway
      return std::invoke( overriden_helper<Caller>::local, std::forward<Args>( args )... );
    } else {
      auto original_ptr = get_original<Caller>();
      // stats can now be recorded, as original_function should now be set...
      const auto       start           = LHCb::chrono::fast_clock::now();
      decltype( auto ) original_result = std::invoke( original_ptr, std::forward<Args>( args )... );
      std::invoke( overriden_helper<Caller>::stats, std::forward<Args>( args )...,
                   ( LHCb::chrono::fast_clock::now() - start ).count() );
      return original_result;
    }
  }

  using free_t = void ( * )( void* );
} // namespace

// C
extern "C" void* malloc( size_t size ) { return wrap_library_call<MALLOC>( size ); }

extern "C" void* calloc( size_t size, size_t n ) { return wrap_library_call<CALLOC>( size, n ); }

extern "C" void* realloc( void* p, size_t newsize ) { return wrap_library_call<REALLOC>( p, newsize ); }

extern "C" void free( void* p ) {
  static free_t original_free = get_original<FREE>();
  if ( p >= global_buffer && p < global_buffer + global_buffer_size ) {
    // this is from the local_malloc function above, the system free would get confused
  } else {
    ( *original_free )( p );
  }
}

// POSIX
extern "C" int posix_memalign( void** memptr, size_t alignment, size_t size ) {
  return wrap_library_call<POSIX_MEMALIGN>( memptr, alignment, size );
}

// Linux
extern "C" void* memalign( size_t alignment, size_t size ) { return wrap_library_call<MEMALIGN>( alignment, size ); }

extern "C" void* aligned_alloc( size_t alignment, size_t size ) {
  return wrap_library_call<ALIGNED_ALLOC>( alignment, size );
}

extern "C" void* valloc( size_t size ) { return wrap_library_call<VALLOC>( size ); }

extern "C" void* pvalloc( size_t size ) { return wrap_library_call<PVALLOC>( size ); }

// C++
// https://en.cppreference.com/w/cpp/memory/new/operator_new
// Section "Global replacements" says we only have to override these two
void* operator new( std::size_t n ) {
  // FIXME should use the new_handler etc.
  void* ptr = malloc( n );
  if ( ptr ) { return ptr; }
  throw std::bad_alloc{};
}

void* operator new( std::size_t n, std::align_val_t align ) {
  auto ptr = aligned_alloc( static_cast<std::size_t>( align ), n );
  if ( ptr ) { return ptr; }
  throw std::bad_alloc{};
}

// C++
void operator delete( void* p ) noexcept { free( p ); }

void operator delete( void* p, std::size_t ) noexcept { free( p ); }

void operator delete( void* p, std::align_val_t ) noexcept { free( p ); }