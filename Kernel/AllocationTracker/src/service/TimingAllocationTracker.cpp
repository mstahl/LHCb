/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AllocationTrackerHelpers.h"

#include "Kernel/AllocationTracker.h"
#include "Kernel/IAllocationTracker.h"
#include "Kernel/STLExtensions.h"

#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/ThreadLocalContext.h"

namespace {
  // Static storage for allocation information that can be acccessed from the
  // free-standing callback function.
  std::vector<AllocationTracker::allocation_count> s_allocations;

  std::size_t indexFromSlot( std::size_t slot ) { return ( slot == EventContext::INVALID_CONTEXT_ID ) ? 0 : slot + 1; }

  AllocationTracker::allocation_count extractAllocationInformation() {
    return std::accumulate( s_allocations.begin(), s_allocations.end(), AllocationTracker::allocation_count{} );
  }

  // If the AllocationTracker library was added to LD_PRELOAD then this function will be called on every dynamic
  // allocation.
  void allocationCallback( AllocationTracker::Function, std::size_t size, uint64_t ticks, AllocationTracker::malloc_t,
                           AllocationTracker::free_t ) {
    s_allocations[indexFromSlot( Gaudi::Hive::currentContext().slot() )].add_allocation( size, ticks );
  }
} // namespace

struct TimingAllocationTracker : public extends<Service, IAllocationTracker> {
  using extends::extends;

  /** Setup before threads are spawned and tracking is enabled.
   */
  void reserveSlots( std::size_t num_slots ) override {
    if ( AllocationTracker::getCallback() ) {
      throw GaudiException{"reserveSlots was called while a callback was active. This is unsafe!",
                           "TimingAllocationTracker", StatusCode::FAILURE};
    }
    // + 1 so we can record stats from invalid slot IDs
    s_allocations.resize( num_slots + 1 );
  }

  /** Being recording dynamic allocation statistics.
   */
  void beginTracking() override {
    // Install our callback to start tracking allocations
    AllocationTracker::setCallback( allocationCallback );
  }

  /** Stop recording dynamic allocation statistics.
   */
  void endTracking() override {
    // Remove our callback, stop tracking allocations
    AllocationTracker::setCallback( nullptr );
  }

  /** Increment the internal count of the number of events that have been
   *  processed while allocation tracking was enabled. This allows the results
   *  to be normalised more helpfully.
   */
  void incrementEventCount( std::size_t num_events, uint64_t num_ticks ) override {
    m_num_events += num_events;
    m_total_ticks += num_ticks;
  }

  StatusCode finalize() final {
    // print out the dynamic allocation tracking information that we stored
    auto const alloc_info = extractAllocationInformation();
    if ( alloc_info.num_allocations() ) {
      info() << "Dynamic allocation information: " << float( alloc_info.num_allocations() ) / m_num_events
             << " allocations/evt yielded " << float( alloc_info.num_bytes() ) / ( m_num_events * 1024 * 1024 )
             << " MiB/evt taking " << float( 100.f * alloc_info.num_ticks() / m_total_ticks ) << "% of processing time"
             << endmsg;
    }
    return extends::finalize();
  }

private:
  uint64_t    m_total_ticks{0};
  std::size_t m_num_events{0};
};

DECLARE_COMPONENT( TimingAllocationTracker )