/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// ============================================================================
// Include files
// ============================================================================
namespace Relations {
  // ==========================================================================
  /** @struct BaseTable
   *  helper structure to ease interactive manipulations
   *  with Reflex dictionaries
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-02-06
   */
  struct BaseTable {};
  // ==========================================================================
  /** @struct BaseWeightedTable
   *  helper structure to ease interactive manipulations
   *  with Reflex dictionaries
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-02-06
   */
  struct BaseWeightedTable : BaseTable {};
  // ==========================================================================
} // end of namespace Relations
// ============================================================================
// The END
// ============================================================================
