/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LHCb
#include "Event/State.h"
#include "VPKernel/PixelUtils.h"

// Local
#include "VPRetinaMatrix.h"

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPRetinaMatrix::VPRetinaMatrix( uint32_t SP_row, uint32_t SP_col, uint8_t SP_pixel, const unsigned int sensor )
    : Coordinate_Retina_row( SP_row - 1 ), Coordinate_Retina_col( SP_col - 2 ), Sensor( sensor ) {
  // record SP pixels.
  for ( uint32_t shift = 0; shift < 8; ++shift ) {
    const uint8_t pixel = SP_pixel & 1;
    if ( pixel ) {
      const uint32_t row     = 4 + shift % 4;
      const uint32_t col     = 4 + shift / 4;
      Pixel_Matrix[row][col] = pixel;
    }
    SP_pixel = SP_pixel >> 1;
    if ( 0 == SP_pixel ) break;
  }

  // record which SP is added
  SPixel_Matrix[SP_row - Coordinate_Retina_row][SP_col - Coordinate_Retina_col] = 1;
}

//=============================================================================
// Check if a SP coordinate are inside the Retina
//=============================================================================
bool VPRetinaMatrix::IsInRetina( uint32_t SP_row, uint32_t SP_col ) const {
  return Coordinate_Retina_row <= (int32_t)SP_row && (int32_t)SP_row < Coordinate_Retina_row + 3 &&
         Coordinate_Retina_col <= (int32_t)SP_col && (int32_t)SP_col < Coordinate_Retina_col + 5;
}

//=============================================================================
// Add a SP to the Retina
//=============================================================================
VPRetinaMatrix& VPRetinaMatrix::AddSP( uint32_t SP_row, uint32_t SP_col, uint8_t SP_pixel ) {
  // record which SP is added
  SPixel_Matrix[SP_row - Coordinate_Retina_row][SP_col - Coordinate_Retina_col] = 1;

  // record SP pixels.
  for ( uint32_t shift = 0; shift < 8; ++shift ) {
    const uint8_t pixel = SP_pixel & 1;
    if ( pixel ) {
      const int32_t row      = ( SP_row - Coordinate_Retina_row ) * 4 + shift % 4;
      const int32_t col      = ( SP_col - Coordinate_Retina_col ) * 2 + shift / 4;
      Pixel_Matrix[row][col] = pixel;
    }
    SP_pixel = SP_pixel >> 1;
    if ( 0 == SP_pixel ) break;
  }
  return *this;
}

//=============================================================================
// Search cluster - Accounting for mirror geometry in sensors 1 and 2
//=============================================================================

std::vector<uint32_t> VPRetinaMatrix::SearchCluster( bool reduce_edge ) const {
  std::vector<uint32_t> RetinaCluster;
  RetinaCluster.reserve( 63 );

  // if sensor number % 4 is 0 or 3 apply a direct cluster search pattern
  if ( ( Sensor % 4 == 0 ) || ( Sensor % 4 == 3 ) ) {
    for ( unsigned int iX = 1; iX < 10 - 1; ++iX )
      for ( unsigned int iY = 1; iY < 12 - 1; ++iY ) {
        if ( ( ( Pixel_Matrix[iY][iX] == 1 ) ||
               ( ( ( Pixel_Matrix[iY + 1][iX] == 1 ) ) && ( ( Pixel_Matrix[iY][iX + 1] == 1 ) ) ) ) &&
             Pixel_Matrix[iY][iX - 1] == 0 && Pixel_Matrix[iY + 1][iX - 1] == 0 && Pixel_Matrix[iY - 1][iX] == 0 &&
             Pixel_Matrix[iY - 1][iX + 1] == 0 && Pixel_Matrix[iY - 1][iX - 1] == 0 ) {

          uint32_t shift_col = 0;
          uint32_t shift_row = 0;
          uint32_t n         = 0;
          uint8_t  edge      = 0;
          uint8_t  self_cont = 0;

          // compute flags
          if ( iX == 7 || iY == 9 ) {

            edge = 1;

          } else {
            if ( Pixel_Matrix[iY + 2][iX - 1] == 0 && Pixel_Matrix[iY + 3][iX - 1] == 0 &&
                 Pixel_Matrix[iY + 3][iX] == 0 && Pixel_Matrix[iY + 3][iX + 1] == 0 &&
                 Pixel_Matrix[iY + 3][iX + 2] == 0 && Pixel_Matrix[iY + 3][iX + 3] == 0 &&
                 Pixel_Matrix[iY + 2][iX + 3] == 0 && Pixel_Matrix[iY + 1][iX + 3] == 0 &&
                 Pixel_Matrix[iY][iX + 3] == 0 && Pixel_Matrix[iY - 1][iX + 3] == 0 &&
                 Pixel_Matrix[iY - 1][iX + 2] == 0 ) {

              self_cont = 1;
            }
          }
          // find cluster center
          if ( iX < 10 - 2 && iY < 12 - 2 ) {
            if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                 ( Pixel_Matrix[iY + 1][iX] == 0 ) ) {
              const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX ) << 3 );
              const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY ) << 3 );
              RetinaCluster.push_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
            } else if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                        ( Pixel_Matrix[iY + 2][iX + 1] == 0 ) ) {
              for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX] == 1 ) {
                  shift_row += iiY;
                  n++;
                }
              const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX ) << 3 );
              const uint64_t cY =
                  ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
              RetinaCluster.push_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
            } else if ( ( Pixel_Matrix[iY + 1][iX] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                        ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
              for ( unsigned int iiX = 0; iiX < 3; ++iiX )
                if ( Pixel_Matrix[iY][iX + iiX] == 1 ) {
                  shift_col += iiX;
                  n++;
                }
              const uint64_t cX =
                  ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
              const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY ) << 3 );
              RetinaCluster.push_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
            } else {
              for ( unsigned int iiX = 0; iiX < 3; ++iiX )
                for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                  if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                    shift_col += iiX;
                    shift_row += iiY;
                    n++;
                  }
              const uint64_t cX =
                  ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
              const uint64_t cY =
                  ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
              RetinaCluster.push_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
            }
            // find clusters at edges if reduce_edge is enabled
          } else if ( reduce_edge ) {
            for ( unsigned int iiX = 0; iiX < 2; ++iiX )
              for ( unsigned int iiY = 0; iiY < 2; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                  shift_col += iiX;
                  shift_row += iiY;
                  n++;
                }
            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
            RetinaCluster.push_back( cX << 11 | cY );
          }
        }
      }
    // if sensor number % 4 is 1 or 2 apply a reverse cluster search pattern
  } else if ( ( Sensor % 4 == 1 ) || ( Sensor % 4 == 2 ) ) {
    for ( unsigned int iX = 0; iX < 10 - 3; ++iX )
      for ( unsigned int iY = 1; iY < 12 - 1; ++iY ) {
        if ( iY < 12 - 2 ) {
          if ( ( ( Pixel_Matrix[iY][iX + 2] == 1 ) ||
                 ( ( ( Pixel_Matrix[iY + 1][iX + 2] == 1 ) ) && ( ( Pixel_Matrix[iY][iX + 2 - 1] == 1 ) ) ) ) &&
               Pixel_Matrix[iY][iX + 1 + 2] == 0 && Pixel_Matrix[iY + 1][iX + 1 + 2] == 0 &&
               Pixel_Matrix[iY - 1][iX + 1 + 2] == 0 && Pixel_Matrix[iY - 1][iX + 2] == 0 &&
               Pixel_Matrix[iY - 1][iX - 1 + 2] == 0 ) {

            uint32_t shift_col = 0;
            uint32_t shift_row = 0;
            uint32_t n         = 0;
            uint8_t  edge      = 0;
            uint8_t  self_cont = 0;

            // compute flags
            if ( iX == 0 || iY == 9 ) {

              edge = 1;

            } else {
              if ( Pixel_Matrix[iY - 1][iX] == 0 && Pixel_Matrix[iY - 1][iX - 1] == 0 &&
                   Pixel_Matrix[iY][iX - 1] == 0 && Pixel_Matrix[iY + 1][iX - 1] == 0 &&
                   Pixel_Matrix[iY + 2][iX - 1] == 0 && Pixel_Matrix[iY + 3][iX - 1] == 0 &&
                   Pixel_Matrix[iY + 3][iX] == 0 && Pixel_Matrix[iY + 3][iX + 1] == 0 &&
                   Pixel_Matrix[iY + 3][iX + 2] == 0 && Pixel_Matrix[iY + 3][iX + 3] == 0 &&
                   Pixel_Matrix[iY + 2][iX + 3] == 0 ) {

                self_cont = 1;
              }
            }
            // find cluster center
            if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                 ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
              const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX + 2 ) << 3 );
              const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY ) << 3 );
              RetinaCluster.push_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
            } else if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                        ( Pixel_Matrix[iY + 2][iX + 1] == 0 ) ) {
              for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX + 2] == 1 ) {
                  shift_row += iiY;
                  n++;
                }
              const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX + 2 ) << 3 );
              const uint64_t cY =
                  ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
              RetinaCluster.push_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
            } else if ( ( Pixel_Matrix[iY + 1][iX] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                        ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
              for ( unsigned int iiX = 0; iiX < 3; ++iiX )
                if ( Pixel_Matrix[iY][iX + iiX] == 1 ) {
                  shift_col += iiX;
                  n++;
                }
              const uint64_t cX =
                  ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
              const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY ) << 3 );
              RetinaCluster.push_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
            } else {
              for ( unsigned int iiX = 0; iiX < 3; ++iiX )
                for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                  if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                    shift_col += iiX;
                    shift_row += iiY;
                    n++;
                  }
              const uint64_t cX =
                  ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
              const uint64_t cY =
                  ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
              RetinaCluster.push_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
            }
          }
          // find clusters at edges if reduce_edge is enabled
        } else if ( reduce_edge && ( ( iX == 0 ) || ( iY == 10 ) ) ) {
          if ( ( ( Pixel_Matrix[iY][iX + 1] == 1 ) ||
                 ( ( ( Pixel_Matrix[iY + 1][iX + 1] == 1 ) ) && ( ( Pixel_Matrix[iY][iX] == 1 ) ) ) ) &&
               Pixel_Matrix[iY][iX + 2] == 0 && Pixel_Matrix[iY + 1][iX + 2] == 0 &&
               Pixel_Matrix[iY - 1][iX + 2] == 0 && Pixel_Matrix[iY - 1][iX + 1] == 0 &&
               Pixel_Matrix[iY - 1][iX] == 0 ) {

            uint32_t shift_col = 0;
            uint32_t shift_row = 0;
            uint32_t n         = 0;

            for ( unsigned int iiX = 0; iiX < 2; ++iiX )
              for ( unsigned int iiY = 0; iiY < 2; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                  shift_col += iiX;
                  shift_row += iiY;
                  n++;
                }
            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
            RetinaCluster.push_back( cX << 11 | cY );
          }
        }
      }
  }
  return RetinaCluster;
}
// same operations as before but reconstructing FullClusters instead of LightClusters
std::vector<LHCb::VPRetinaFullCluster> VPRetinaMatrix::SearchFullCluster( bool reduce_edge ) const {
  std::vector<LHCb::VPRetinaFullCluster> RetinaCluster;
  RetinaCluster.reserve( 63 );

  std::vector<LHCb::VPChannelID> channelID;
  channelID.reserve( 9 );
  if ( ( Sensor % 4 == 0 ) || ( Sensor % 4 == 3 ) ) {
    for ( unsigned int iX = 1; iX < 10 - 1; ++iX )
      for ( unsigned int iY = 1; iY < 12 - 1; ++iY ) {
        if ( ( ( Pixel_Matrix[iY][iX] == 1 ) ||
               ( ( ( Pixel_Matrix[iY + 1][iX] == 1 ) ) && ( ( Pixel_Matrix[iY][iX + 1] == 1 ) ) ) ) &&
             Pixel_Matrix[iY][iX - 1] == 0 && Pixel_Matrix[iY + 1][iX - 1] == 0 && Pixel_Matrix[iY - 1][iX] == 0 &&
             Pixel_Matrix[iY - 1][iX + 1] == 0 && Pixel_Matrix[iY - 1][iX - 1] == 0 ) {

          uint32_t shift_col = 0;
          uint32_t shift_row = 0;
          uint32_t n         = 0;
          uint8_t  edge      = 0;
          uint8_t  self_cont = 0;

          if ( iX == 7 || iY == 9 ) {
            edge = 1;
          } else {
            if ( Pixel_Matrix[iY + 2][iX - 1] == 0 && Pixel_Matrix[iY + 3][iX - 1] == 0 &&
                 Pixel_Matrix[iY + 3][iX] == 0 && Pixel_Matrix[iY + 3][iX + 1] == 0 &&
                 Pixel_Matrix[iY + 3][iX + 2] == 0 && Pixel_Matrix[iY + 3][iX + 3] == 0 &&
                 Pixel_Matrix[iY + 2][iX + 3] == 0 && Pixel_Matrix[iY + 1][iX + 3] == 0 &&
                 Pixel_Matrix[iY][iX + 3] == 0 && Pixel_Matrix[iY - 1][iX + 3] == 0 &&
                 Pixel_Matrix[iY - 1][iX + 2] == 0 ) {

              self_cont = 1;
            }
          }

          if ( iX < 10 - 2 && iY < 12 - 2 ) {
            if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                 ( Pixel_Matrix[iY + 1][iX] == 0 ) ) {
              const uint32_t    id_X    = Coordinate_Retina_col * 2 + iX;
              const uint32_t    id_Y    = Coordinate_Retina_row * 4 + iY;
              const uint32_t    id_chip = id_X / Pixel::CHIP_COLUMNS;
              const uint32_t    id_ccol = id_X % Pixel::CHIP_COLUMNS;
              LHCb::VPChannelID tmp_cid( Sensor, id_chip, id_ccol, id_Y );
              channelID.push_back( tmp_cid );

              const uint64_t            cX = ( ( Coordinate_Retina_col * 2 + iX ) << 3 );
              const uint64_t            cY = ( ( Coordinate_Retina_row * 4 + iY ) << 3 );
              LHCb::VPRetinaFullCluster tmp_cluster( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
              RetinaCluster.push_back( tmp_cluster );
              channelID.clear();

            } else if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                        ( Pixel_Matrix[iY + 2][iX + 1] == 0 ) ) {
              for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX] == 1 ) {
                  const uint32_t    id_X    = Coordinate_Retina_col * 2 + iX;
                  const uint32_t    id_Y    = Coordinate_Retina_row * 4 + iY + iiY;
                  const uint32_t    id_chip = id_X / Pixel::CHIP_COLUMNS;
                  const uint32_t    id_ccol = id_X % Pixel::CHIP_COLUMNS;
                  LHCb::VPChannelID tmp_cid( Sensor, id_chip, id_ccol, id_Y );
                  channelID.push_back( tmp_cid );
                  shift_row += iiY;
                  n++;
                }
              const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX ) << 3 );
              const uint64_t cY =
                  ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
              LHCb::VPRetinaFullCluster tmp_cluster( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
              RetinaCluster.push_back( tmp_cluster );
              channelID.clear();

            } else if ( ( Pixel_Matrix[iY + 1][iX] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                        ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
              for ( unsigned int iiX = 0; iiX < 3; ++iiX )
                if ( Pixel_Matrix[iY][iX + iiX] == 1 ) {
                  const uint32_t    id_X    = Coordinate_Retina_col * 2 + iX + iiX;
                  const uint32_t    id_Y    = Coordinate_Retina_row * 4 + iY;
                  const uint32_t    id_chip = id_X / Pixel::CHIP_COLUMNS;
                  const uint32_t    id_ccol = id_X % Pixel::CHIP_COLUMNS;
                  LHCb::VPChannelID tmp_cid( Sensor, id_chip, id_ccol, id_Y );
                  channelID.push_back( tmp_cid );
                  shift_col += iiX;
                  n++;
                }

              const uint64_t cX =
                  ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
              const uint64_t            cY = ( ( Coordinate_Retina_row * 4 + iY ) << 3 );
              LHCb::VPRetinaFullCluster tmp_cluster( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
              RetinaCluster.push_back( tmp_cluster );
              channelID.clear();

            } else {
              for ( unsigned int iiX = 0; iiX < 3; ++iiX )
                for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                  if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                    const uint32_t    id_X    = Coordinate_Retina_col * 2 + iX + iiX;
                    const uint32_t    id_Y    = Coordinate_Retina_row * 4 + iY + iiY;
                    const uint32_t    id_chip = id_X / Pixel::CHIP_COLUMNS;
                    const uint32_t    id_ccol = id_X % Pixel::CHIP_COLUMNS;
                    LHCb::VPChannelID tmp_cid( Sensor, id_chip, id_ccol, id_Y );
                    channelID.push_back( tmp_cid );
                    shift_col += iiX;
                    shift_row += iiY;
                    n++;
                  }
              const uint64_t cX =
                  ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
              const uint64_t cY =
                  ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );

              LHCb::VPRetinaFullCluster tmp_cluster( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
              RetinaCluster.push_back( tmp_cluster );
              channelID.clear();
            }
          } else if ( reduce_edge ) {
            for ( unsigned int iiX = 0; iiX < 2; ++iiX )
              for ( unsigned int iiY = 0; iiY < 2; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                  const uint32_t    id_X    = Coordinate_Retina_col * 2 + iX + iiX;
                  const uint32_t    id_Y    = Coordinate_Retina_row * 4 + iY + iiY;
                  const uint32_t    id_chip = id_X / Pixel::CHIP_COLUMNS;
                  const uint32_t    id_ccol = id_X % Pixel::CHIP_COLUMNS;
                  LHCb::VPChannelID tmp_cid( Sensor, id_chip, id_ccol, id_Y );
                  channelID.push_back( tmp_cid );
                  shift_col += iiX;
                  shift_row += iiY;
                  n++;
                }
            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );

            LHCb::VPRetinaFullCluster tmp_cluster( cX << 11 | cY, channelID );
            RetinaCluster.push_back( tmp_cluster );
            channelID.clear();
          }
        }
      }
  } else if ( ( Sensor % 4 == 1 ) || ( Sensor % 4 == 2 ) ) {
    for ( unsigned int iX = 0; iX < 10 - 3; ++iX )
      for ( unsigned int iY = 1; iY < 12 - 1; ++iY ) {
        if ( iY < 12 - 2 ) {
          if ( ( ( Pixel_Matrix[iY][iX + 2] == 1 ) ||
                 ( ( ( Pixel_Matrix[iY + 1][iX + 2] == 1 ) ) && ( ( Pixel_Matrix[iY][iX + 2 - 1] == 1 ) ) ) ) &&
               Pixel_Matrix[iY][iX + 1 + 2] == 0 && Pixel_Matrix[iY + 1][iX + 1 + 2] == 0 &&
               Pixel_Matrix[iY - 1][iX + 1 + 2] == 0 && Pixel_Matrix[iY - 1][iX + 2] == 0 &&
               Pixel_Matrix[iY - 1][iX - 1 + 2] == 0 ) {

            uint32_t shift_col = 0;
            uint32_t shift_row = 0;
            uint32_t n         = 0;
            uint8_t  edge      = 0;
            uint8_t  self_cont = 0;

            if ( iX == 0 || iY == 9 ) {
              edge = 1;
            } else {
              if ( Pixel_Matrix[iY - 1][iX] == 0 && Pixel_Matrix[iY - 1][iX - 1] == 0 &&
                   Pixel_Matrix[iY][iX - 1] == 0 && Pixel_Matrix[iY + 1][iX - 1] == 0 &&
                   Pixel_Matrix[iY + 2][iX - 1] == 0 && Pixel_Matrix[iY + 3][iX - 1] == 0 &&
                   Pixel_Matrix[iY + 3][iX] == 0 && Pixel_Matrix[iY + 3][iX + 1] == 0 &&
                   Pixel_Matrix[iY + 3][iX + 2] == 0 && Pixel_Matrix[iY + 3][iX + 3] == 0 &&
                   Pixel_Matrix[iY + 2][iX + 3] == 0 ) {

                self_cont = 1;
              }
            }

            if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                 ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
              const uint32_t    id_X    = Coordinate_Retina_col * 2 + iX + 2;
              const uint32_t    id_Y    = Coordinate_Retina_row * 4 + iY;
              const uint32_t    id_chip = id_X / Pixel::CHIP_COLUMNS;
              const uint32_t    id_ccol = id_X % Pixel::CHIP_COLUMNS;
              LHCb::VPChannelID tmp_cid( Sensor, id_chip, id_ccol, id_Y );
              channelID.push_back( tmp_cid );

              const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX + 2 ) << 3 );
              const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY ) << 3 );

              LHCb::VPRetinaFullCluster tmp_cluster( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
              RetinaCluster.push_back( tmp_cluster );
              channelID.clear();

            } else if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                        ( Pixel_Matrix[iY + 2][iX + 1] == 0 ) ) {
              for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX + 2] == 1 ) {
                  const uint32_t    id_X    = Coordinate_Retina_col * 2 + iX + 2;
                  const uint32_t    id_Y    = Coordinate_Retina_row * 4 + iY + iiY;
                  const uint32_t    id_chip = id_X / Pixel::CHIP_COLUMNS;
                  const uint32_t    id_ccol = id_X % Pixel::CHIP_COLUMNS;
                  LHCb::VPChannelID tmp_cid( Sensor, id_chip, id_ccol, id_Y );
                  channelID.push_back( tmp_cid );
                  shift_row += iiY;
                  n++;
                }

              const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX + 2 ) << 3 );
              const uint64_t cY =
                  ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
              LHCb::VPRetinaFullCluster tmp_cluster( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
              RetinaCluster.push_back( tmp_cluster );
              channelID.clear();

            } else if ( ( Pixel_Matrix[iY + 1][iX] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                        ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
              for ( unsigned int iiX = 0; iiX < 3; ++iiX )
                if ( Pixel_Matrix[iY][iX + iiX] == 1 ) {
                  const uint32_t    id_X    = Coordinate_Retina_col * 2 + iX + iiX;
                  const uint32_t    id_Y    = Coordinate_Retina_row * 4 + iY;
                  const uint32_t    id_chip = id_X / Pixel::CHIP_COLUMNS;
                  const uint32_t    id_ccol = id_X % Pixel::CHIP_COLUMNS;
                  LHCb::VPChannelID tmp_cid( Sensor, id_chip, id_ccol, id_Y );
                  channelID.push_back( tmp_cid );
                  shift_col += iiX;
                  n++;
                }

              const uint64_t cX =
                  ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
              const uint64_t            cY = ( ( Coordinate_Retina_row * 4 + iY ) << 3 );
              LHCb::VPRetinaFullCluster tmp_cluster( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
              RetinaCluster.push_back( tmp_cluster );
              channelID.clear();

            } else {
              for ( unsigned int iiX = 0; iiX < 3; ++iiX )
                for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                  if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                    const uint32_t    id_X    = Coordinate_Retina_col * 2 + iX + iiX;
                    const uint32_t    id_Y    = Coordinate_Retina_row * 4 + iY + iiY;
                    const uint32_t    id_chip = id_X / Pixel::CHIP_COLUMNS;
                    const uint32_t    id_ccol = id_X % Pixel::CHIP_COLUMNS;
                    LHCb::VPChannelID tmp_cid( Sensor, id_chip, id_ccol, id_Y );
                    channelID.push_back( tmp_cid );
                    shift_col += iiX;
                    shift_row += iiY;
                    n++;
                  }

              const uint64_t cX =
                  ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
              const uint64_t cY =
                  ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );

              LHCb::VPRetinaFullCluster tmp_cluster( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
              RetinaCluster.push_back( tmp_cluster );
              channelID.clear();
            }
          }
        } else if ( reduce_edge && ( ( iX == 0 ) || ( iY == 10 ) ) ) {
          if ( ( ( Pixel_Matrix[iY][iX + 1] == 1 ) ||
                 ( ( ( Pixel_Matrix[iY + 1][iX + 1] == 1 ) ) && ( ( Pixel_Matrix[iY][iX] == 1 ) ) ) ) &&
               Pixel_Matrix[iY][iX + 2] == 0 && Pixel_Matrix[iY + 1][iX + 2] == 0 &&
               Pixel_Matrix[iY - 1][iX + 2] == 0 && Pixel_Matrix[iY - 1][iX + 1] == 0 &&
               Pixel_Matrix[iY - 1][iX] == 0 ) {

            uint32_t shift_col = 0;
            uint32_t shift_row = 0;
            uint32_t n         = 0;

            for ( unsigned int iiX = 0; iiX < 2; ++iiX )
              for ( unsigned int iiY = 0; iiY < 2; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                  const uint32_t    id_X    = Coordinate_Retina_col * 2 + iX + iiX;
                  const uint32_t    id_Y    = Coordinate_Retina_row * 4 + iY + iiY;
                  const uint32_t    id_chip = id_X / Pixel::CHIP_COLUMNS;
                  const uint32_t    id_ccol = id_X % Pixel::CHIP_COLUMNS;
                  LHCb::VPChannelID tmp_cid( Sensor, id_chip, id_ccol, id_Y );
                  channelID.push_back( tmp_cid );
                  shift_col += iiX;
                  shift_row += iiY;
                  n++;
                }

            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );

            LHCb::VPRetinaFullCluster tmp_cluster( cX << 11 | cY, channelID );
            RetinaCluster.push_back( tmp_cluster );
            channelID.clear();
          }
        }
      }
  }
  return RetinaCluster;
}
