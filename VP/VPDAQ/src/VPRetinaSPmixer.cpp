/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "Kernel/VPChannelID.h"
#include "Kernel/VPConstants.h"
#include "VPDet/DeVP.h"
#include "VPKernel/PixelUtils.h"
#include "VPKernel/VeloPixelInfo.h"
#include "VPRetinaMatrix.h"
#include <algorithm>
#include <array>
#include <iomanip>
#include <iterator>
#include <random>
#include <tuple>
#include <vector>

/** @class VPRetinaSPmixer VPRetinaSPmixer.h
 * Algorithm to mix SPs in raw banks.
 *
 * There is one raw bank per sensor, that is the sensor number (0-207)
 * is the source ID of the bank.
 *
 * @author Federico Lazzari
 * @date   2018-06-20
 */

class VPRetinaSPmixer : public Gaudi::Functional::Transformer<LHCb::RawEvent( const LHCb::RawEvent& )> {

public:
  /// Standard constructor
  VPRetinaSPmixer( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization

  /// Algorithm execution
  LHCb::RawEvent operator()( const LHCb::RawEvent& ) const override;

private:
  /// bank version. (change this every time semantics change!)
  static constexpr unsigned int m_bankVersion = 2;

  /// mix SP from bank
  std::vector<uint32_t> mixSP( LHCb::span<const uint32_t> bank ) const;

  // random number
  mutable Rndm::Numbers m_rndm;
};

using namespace LHCb;

DECLARE_COMPONENT( VPRetinaSPmixer )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPRetinaSPmixer::VPRetinaSPmixer( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"RawEventLocation", LHCb::RawEventLocation::Default}},
                   KeyValue{"RawEventLocationMixed", LHCb::RawEventLocation::VeloSPmixed} ) {}

StatusCode VPRetinaSPmixer::initialize() {
  return Transformer::initialize().andThen( [&] { return m_rndm.initialize( randSvc(), Rndm::Flat( 0., 1. ) ); } );
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::RawEvent VPRetinaSPmixer::operator()( const LHCb::RawEvent& rawEvent ) const {
  RawEvent result;

  const auto& tBanks = rawEvent.banks( LHCb::RawBank::VP );
  if ( tBanks.empty() ) return result;

  const unsigned int version = tBanks[0]->version();
  if ( version != 2 ) {
    warning() << "Unsupported raw bank version (" << version << ")" << endmsg;
    return result;
  }

  debug() << "Read " << tBanks.size() << " raw banks from TES" << endmsg;

  unsigned int nBanks = 0;

  // Loop over VP RawBanks
  for ( auto iterBank : tBanks ) {

    const unsigned int               sensor = iterBank->sourceID();
    const LHCb::span<const uint32_t> bank   = iterBank->range<uint32_t>();

    std::vector<uint32_t> mixedSP;
    mixedSP.reserve( bank[0] + 1 );

    for ( const uint32_t sp_word : bank ) { mixedSP.push_back( sp_word ); }
    uint32_t n = mixedSP[0];
    if ( n > 1 ) { // check there are at least 2 SPs
      for ( uint32_t i = n - 1; i > 0; --i ) {
        // pick random int from 0 to i inclusive
        uint32_t j = static_cast<int>( m_rndm.shoot() * ( i + 1 ) );
        std::swap( mixedSP[i + 1], mixedSP[j + 1] );
      }
    }

    assert( mixedSP.size() == 1 + mixedSP[0] );

    result.addBank( sensor, LHCb::RawBank::VP, m_bankVersion, mixedSP );

    ++nBanks;

  } // loop over all banks

  debug() << "Added " << nBanks << " raw banks of retina clusters to TES" << endmsg;

  return result;
}
