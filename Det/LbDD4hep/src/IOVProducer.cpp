/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LbDD4hep/IDD4hepSvc.h"

#include "Event/ODIN.h"

#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ServiceHandle.h"

namespace LHCb::Det::LbDD4hep {

  /**
   * Algorithm creating a IOV in DD4hep and storing it in the TES
   */
  class IOVProducer : public Gaudi::Functional::Transformer<IDD4hepSvc::DD4HepSlicePtr( const ODIN& )> {
  public:
    IOVProducer( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue( "ODIN", LHCb::ODINLocation::Default ),
                       KeyValue( "SliceLocation", IDD4hepSvc::DefaultSliceLocation ) ) {
      // make sure the the ROOT logging level is such that the creation of DD4HepSvc
      // does not log anything on stderr in TGeoManager. the verbosity will then be
      // set back to a reasonnable value by the DD4hepSvc
      TGeoManager::SetVerboseLevel( 0 );
    }

    IDD4hepSvc::DD4HepSlicePtr operator()( const ODIN& odin ) const override {
      info() << "Loading conditions for Run " << odin.runNumber() << endmsg;
      return m_dd4hepSvc->get_slice( odin.runNumber() );
    }

  private:
    ServiceHandle<LHCb::Det::LbDD4hep::IDD4hepSvc> m_dd4hepSvc{this, "DD4hepSvc", "LHCb::Det::LbDD4hep::DD4hepSvc",
                                                               "DD4Hep Service"};
  };

  DECLARE_COMPONENT( IOVProducer )

} // namespace LHCb::Det::LbDD4hep
