/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "DD4hep/AlignmentData.h"
#include "DD4hep/ConditionDerived.h"
#include "DD4hep/DetElement.h"
#include "DD4hep/Detector.h"
#include "DD4hep/detail/ConditionsInterna.h"
#include "DDCond/ConditionsSlice.h"

#include "Kernel/STLExtensions.h"

#include "GaudiKernel/IService.h"

#include "boost/callable_traits.hpp"

#include <memory>
#include <type_traits>

namespace LHCb::Det::LbDD4hep {

  namespace detail {

    /// find out whether a type is actually a dd4hep handle
    template <class T>
    static auto testHandle( int )
        -> std::integral_constant<bool,
                                  std::is_base_of_v<dd4hep::Handle<typename std::decay_t<T>::Object>, std::decay_t<T>>>;
    template <class>
    static auto testHandle( long ) -> std::false_type;
    template <class T>
    struct IsHandle : decltype( testHandle<T>( 0 ) ) {};
    template <class T>
    inline constexpr bool IsHandle_v = IsHandle<T>::value;

    template <typename Callable>
    inline constexpr auto arity_v = std::tuple_size_v<boost::callable_traits::args_t<Callable>>;

    template <typename Type>
    using ReturnType = std::conditional_t<IsHandle_v<Type>, std::decay_t<Type>, Type&>;

    template <typename Input>
    ReturnType<Input> fetch_1( dd4hep::cond::ConditionUpdateContext const& context, std::size_t n ) {
      auto cond = context.condition( context.key( n ), true );
      if constexpr ( IsHandle_v<Input> ) {
        return Input( cond );
      } else {
        return cond.get<std::decay_t<Input>>();
      }
    }

    template <typename TypeList, std::size_t... I>
    using ReturnTypes = std::tuple<ReturnType<std::tuple_element_t<I, TypeList>>...>;

    template <typename TypeList, std::size_t... I>
    auto fetch_n( dd4hep::cond::ConditionUpdateContext const& ctx, std::index_sequence<I...> ) {
      return ReturnTypes<TypeList, I...>{fetch_1<std::tuple_element_t<I, TypeList>>( ctx, I )...};
    }

    template <typename Transform, std::ptrdiff_t N = detail::arity_v<Transform>>
    auto fetch_inputs_for( dd4hep::cond::ConditionUpdateContext const& ctx ) {
      using InputTypes = boost::callable_traits::args_t<Transform>;
      return fetch_n<InputTypes>( ctx, std::make_index_sequence<N>{} );
    }
  } // namespace detail

  /**
   * small functor wrapping a lambda and suitable for creating
   * a DD4hep derived condition
   */
  class GenericConditionUpdateCall : public dd4hep::cond::ConditionUpdateCall {
  public:
    /// Type for a user provided callback function.
    /// The first argument is the ConditionKey of the target and is used to be
    /// able to reuse a transformation function that behaves differently depending
    /// on the requested output, The ConditionUpdateContext will be filled with the
    /// input conditions, and the last argument is the Condition instance to update.
    using ConditionCallbackFunction =
        std::function<void( dd4hep::cond::ConditionUpdateContext const&, dd4hep::Condition& )>;
    // constructor, taking a transform and creating the 2 callables used for operator() and resolve
    // the constructor is templated and the callables' types are not, allowing a generic interface
    template <typename Transform>
    GenericConditionUpdateCall( Transform f ) {
      using ConditionType = decltype( std::apply(
          f, detail::fetch_inputs_for<Transform>( std::declval<dd4hep::cond::ConditionUpdateContext&>() ) ) );
      m_fillCondition     = [=, f = std::move( f )]( dd4hep::detail::ConditionObject*            condition,
                                                 dd4hep::cond::ConditionUpdateContext const& ctx ) {
        condition->data.bind<ConditionType>() = std::apply( f, detail::fetch_inputs_for<Transform>( ctx ) );
      };
    }
    dd4hep::Condition operator()( const dd4hep::ConditionKey&           key,
                                  dd4hep::cond::ConditionUpdateContext& context ) override {
      auto* cond = new dd4hep::detail::ConditionObject();
      cond->hash = key;
      m_fillCondition( cond, context );
      return cond;
    }
    void resolve( dd4hep::Condition, dd4hep::cond::ConditionUpdateContext& ) override {}

  private:
    std::function<void( dd4hep::detail::ConditionObject*, dd4hep::cond::ConditionUpdateContext& )> m_fillCondition;
  };

  /** @class IDD4hepSvc IDD4hepSvc.h DetDesc/IDD4hepSvc.h
   *
   *  Definition of abstract interface for the service providing the DD4hep Geometry
   *
   */
  struct IDD4hepSvc : extend_interfaces<IService> {

    using DD4HepSlicePtr                       = std::shared_ptr<dd4hep::cond::ConditionsSlice>;
    using DD4HepDerivationFunc                 = std::shared_ptr<dd4hep::cond::ConditionUpdateCall>;
    static constexpr auto DefaultSliceLocation = "IOVLockDD4hep";

    /** Declaration of the unique interface identifier
     *  ( interface id, major version, minor version)
     */
    DeclareInterfaceID( IDD4hepSvc, 1, 0 );

    /**
     * get the Condition slice from the cache
     */
    virtual DD4HepSlicePtr get_slice( size_t iov ) = 0;

    /**
     * drop a Condition slice from the cache
     */
    virtual void drop_slice( size_t iov ) = 0;

    /**
     * Add a derived condition to DD4hep
     * @arg inputs the keys for the conditions the derived one depends on.
     *      These conditions will be given as input to the func functor
     * @arg output the key for the new derived condition
     * @arg func a functor able to create/update the derived condition
     *      out of the inputs
     */
    virtual bool add( LHCb::span<const std::string> inputs, const std::string& output, DD4HepDerivationFunc& func ) = 0;

    /**
     * Helper to update in the conditions overlay the alignment condition of a given DetElement.
     */
    virtual void update_alignment( const dd4hep::DetElement& de, const dd4hep::Delta& delta ) = 0;
  };

} // namespace LHCb::Det::LbDD4hep
