/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/GeneratePrFittedForwardTracks.h"
#include "Event/MuonPID_v2.h"
#include "Event/PrFittedForwardTracks.h"
#include "Event/PrMuonPIDs.h"
#include "Event/StateParameters.h"
#include "Event/Zip.h"
#include "GaudiKernel/SerializeSTL.h"
#include "SOAExtensions/ZipAlgorithms.h"
#include "SOAExtensions/ZipUtils.h"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestZipInfrastructure
#include <boost/test/unit_test.hpp>

#include <chrono>
#include <ctime>
#include <functional>
#include <iostream>
#include <random>
#include <tuple>
#include <utility>

using Tracks   = LHCb::Pr::Fitted::Forward::Tracks;
using MuonPIDs = LHCb::Pr::Muon::PIDs;

namespace MuonTag = LHCb::Pr::Muon::Tag;

template <typename Ismuon>
auto count_muons( MuonPIDs const& muonPIDs, Ismuon ismuon ) {
  size_t nrec_muons = 0;
  size_t count      = 0;
  for ( auto const& pid : muonPIDs.scalar() ) {
    if ( pid.IsMuon().cast() ) { nrec_muons++; }
    BOOST_CHECK( ( ismuon( count ) ) == pid.IsMuon().cast() );
    count++;
  }
  return nrec_muons;
}

std::tuple<size_t, MuonPIDs> create_muonids( Tracks const& tracks ) {
  auto     ismuon = []( int t ) { return ( t % 2 == 0 ); };
  auto     nmuons = tracks.size() / 2 + tracks.size() % 2; // Even true, odd false
  MuonPIDs muonPIDs{tracks.zipIdentifier()};
  int      counter = 0;
  for ( auto const t : tracks.scalar() ) {
    auto pid = muonPIDs.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    pid.field<MuonTag::Status>().set( ismuon( counter ) && t.pt() > 0.f );
    pid.field<MuonTag::Chi2Corr>().set( 0.f );
    counter++;
  }
  // Check if counted correctly with scalar version
  BOOST_CHECK( ( count_muons( muonPIDs, ismuon ) == static_cast<size_t>( nmuons ) ) );
  BOOST_CHECK( ( muonPIDs.size() == static_cast<size_t>( tracks.size() ) ) );
  return std::tuple<int, MuonPIDs>{nmuons, std::move( muonPIDs )};
}

BOOST_AUTO_TEST_CASE( test_transform_from_tracks ) {
  unsigned int ntracks    = 999;
  auto         tracks     = LHCb::Pr::Fitted::Forward::generate_tracks( ntracks );
  auto [nmuons, muonPIDs] = create_muonids( tracks );

  // Check if counted correctly using vectorized version, compiler flag dependent.
  size_t nrec_muons = 0;
  for ( auto const& pid : muonPIDs.simd() ) { nrec_muons += popcount( pid.IsMuon() && pid.loop_mask() ); }
  BOOST_CHECK( ( nrec_muons == nmuons ) );
}

BOOST_AUTO_TEST_CASE( test_zipping_of_tracks_and_muonids ) {
  unsigned int ntracks = 999;
  // Test mix of const and non-const containers
  const auto tracks       = LHCb::Pr::Fitted::Forward::generate_tracks( ntracks );
  auto [nmuons, muonPIDs] = create_muonids( tracks );

  // Test zipping
  auto zipped = LHCb::v2::Event::make_zip( tracks, muonPIDs );

  // Pr::Zip filter returning combined object
  auto [new_tracks, new_pids] =
      zipped.filter( []( auto const& chunk ) { return ( chunk.pt() > 0.f ) && ( chunk.IsMuon() ); } );
  BOOST_CHECK( ( new_tracks.size() == nmuons ) );
  BOOST_CHECK( ( new_pids.size() == nmuons ) );

  for ( auto const& pid : new_pids.scalar() ) { BOOST_CHECK( pid.IsMuon().cast() ); }
}

BOOST_AUTO_TEST_CASE( test_timing_of_ismuon ) {
  std::cout << "Test timing of scalar and vectorised loops" << std::endl;

  unsigned int ntracks    = 999;
  auto         tracks     = LHCb::Pr::Fitted::Forward::generate_tracks( ntracks );
  auto [nmuons, muonPIDs] = create_muonids( tracks );

  auto iterable_muonPIDs = LHCb::Pr::make_zip( muonPIDs );

  // Test timing
  unsigned int                                                iterations = 1000;
  unsigned int                                                tries      = 15;
  unsigned int                                                n_success  = 0;
  std::chrono::time_point<std::chrono::high_resolution_clock> start, end;

  for ( unsigned int j = 0; j < tries; j++ ) {

    // vectorised loop
    bool correct_vector = false;
    start               = std::chrono::system_clock::now();
    for ( unsigned int i = 0; i < iterations; i++ ) {
      size_t nrec_muons_vector = 0;
      for ( auto const& pid : muonPIDs.simd() ) { nrec_muons_vector += popcount( pid.IsMuon() && pid.loop_mask() ); }
      correct_vector = nrec_muons_vector == nmuons;
    }
    end                            = std::chrono::system_clock::now();
    int elapsed_seconds_vectorized = std::chrono::duration_cast<std::chrono::microseconds>( end - start ).count();

    // scalar loop
    bool correct_scalar = false;
    start               = std::chrono::system_clock::now();
    for ( unsigned int i = 0; i < iterations; i++ ) {
      size_t nrec_muons_scalar = 0;
      for ( auto const& pid : muonPIDs.scalar() ) {
        if ( pid.IsMuon().cast() ) { nrec_muons_scalar++; }
      }
      correct_scalar = nrec_muons_scalar == nmuons;
    }
    end                        = std::chrono::system_clock::now();
    int elapsed_seconds_scalar = std::chrono::duration_cast<std::chrono::microseconds>( end - start ).count();

    BOOST_CHECK( correct_vector );
    BOOST_CHECK( correct_scalar );

    using dType = decltype( iterable_muonPIDs )::default_simd_t;
    if constexpr ( !std::is_same_v<SIMDWrapper::scalar::types, dType> ) {
      std::cout << "Elapsed time (vector loops): " << elapsed_seconds_vectorized << " micro s. (Try " << j << ")\n";
      std::cout << "Elapsed time (scalar loops): " << elapsed_seconds_scalar << " micro s. (Try " << j << ")\n";
      // Check that vectorized is faster than scalar
      bool success = ( elapsed_seconds_vectorized < elapsed_seconds_scalar );
      if ( success ) {
        n_success++;
      } else {
        std::cout << "Failed" << std::endl;
      }
      // BOOST_CHECK( success  );
    } else {
      std::cout << "Elapsed time (scalar  (deduced) loops): " << elapsed_seconds_vectorized << " micro s. (Try " << j
                << ")\n";
      std::cout << "Elapsed time (scalar (explicit) loops): " << elapsed_seconds_scalar << " micro s. (Try " << j
                << ")\n";

      // Check that both scalar versions give about the same result.
      bool success = static_cast<double>( elapsed_seconds_vectorized ) / elapsed_seconds_scalar < 2.0 &&
                     static_cast<double>( elapsed_seconds_scalar ) / elapsed_seconds_vectorized < 2.0;
      if ( success ) {
        n_success++;
      } else {
        std::cout << "Failed" << std::endl;
      }
      // BOOST_CHECK( success  );
    }
  }
  BOOST_CHECK( ( 2 * n_success > tries ) );

  std::cout << "Success " << n_success << " / " << tries << "\n";
}
