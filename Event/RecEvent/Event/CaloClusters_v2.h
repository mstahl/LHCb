/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/CaloDataFunctor.h"
#include "Event/CaloDigitStatus.h"
#include "Event/CaloDigits_v2.h"
#include "Event/CaloPosition.h"
#include "GaudiKernel/Point3DTypes.h"
#include "Kernel/CaloCellID.h"
#include "Kernel/EventLocalUnique.h"
#include "Kernel/STLExtensions.h"
#include <cstdint>
#include <utility>
#include <vector>

namespace LHCb::Event::Calo {
  using DigitStatus = LHCb::Calo::DigitStatus::Status;
  using CellID      = LHCb::Calo::CellID;
  inline namespace v2 {

    // Namespace for locations in TDS
    namespace ClusterLocation {
      inline const std::string Default      = "Rec/Calo/Clusters";
      inline const std::string EcalRaw      = "Rec/Calo/EcalClustersRaw";
      inline const std::string EcalOverlap  = "Rec/Calo/EcalClustersOverlap";
      inline const std::string Ecal         = "Rec/Calo/EcalClusters";
      inline const std::string Hcal         = "Rec/Calo/HcalClusters";
      inline const std::string EcalSplit    = "Rec/Calo/EcalSplitClusters";
      inline const std::string DefaultHlt   = "Hlt/Calo/Clusters";
      inline const std::string EcalHlt      = "Hlt/Calo/EcalClusters";
      inline const std::string HcalHlt      = "Hlt/Calo/HcalClusters";
      inline const std::string EcalSplitHlt = "Hlt/Calo/EcalSplitClusters";
      inline const std::string EcalHlt1     = "Hlt1/Calo/EcalClusters";
    } // namespace ClusterLocation

    /** @class CaloCluster CaloCluster.h
     *
     * Calorimeter Cluster * * * The class represents the cluster in calorimeter as
     * a * collection of connected and tagged cells. * *
     *
     * @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *
     */

    class Clusters final {
    public:
      /// CaloCluster type
      enum class Type : unsigned char { Undefined = 0, Invalid, CellularAutomaton, Area3x3, Area2x2 };

    private:
      struct Cluster_ {
        Cluster_( Type type, CaloCellID seed, std::pair<int, int> entries, double energy, Gaudi::XYZPoint position,
                  uint16_t cold )
            : m_seed{seed}
            , m_entries{entries}
            , m_energy{energy}
            , m_position{std::move( position )}
            , m_type{type}
            , m_cold{cold} {
          assert( static_cast<bool>( seed ) );
        }
        CaloCellID                  m_seed;    ///< The cellID of the seed digit for the cluster
        std::pair<int16_t, int16_t> m_entries; ///< first entry, # of entries for this cluster
        double                      m_energy;
        Gaudi::XYZPoint             m_position;
        Type                        m_type; ///< The type of the cluster
        uint16_t                    m_cold;

        // allow Cluster_ to be used by EnergyTransverse..
        double     e() const { return m_energy; }
        CaloCellID cellID() const { return m_seed; }
      };
      struct SpreadAndCovariance_ { // this used to be in CaloPosition...
        /// 3x3 Covariance matrix (E,X,Y)
        Gaudi::SymMatrix3x3 m_covariance;
        /// 2x2 spread matrix (X,Y)
        Gaudi::SymMatrix2x2 m_spread;
        /// center of gravity
        Gaudi::Vector2 m_center;
      };

      class Entry {
        CellID      m_id;
        double      m_energy;
        double      m_fraction = 1.;
        DigitStatus m_status;

      public:
        // constructors
        Entry( CaloCellID id, double e, DigitStatus s ) : m_id{id}, m_energy{e}, m_status{s} {
          assert( static_cast<bool>( id ) );
        }
        Entry( CaloCellID id, double e, double f, DigitStatus s ) : m_id{id}, m_energy{e}, m_fraction{f}, m_status{s} {
          assert( static_cast<bool>( id ) );
        }

        // observers
        [[nodiscard]] CaloCellID  cellID() const { return m_id; }
        [[nodiscard]] double      fraction() const { return m_fraction; }
        [[nodiscard]] double      energy() const { return m_energy; }
        [[nodiscard]] DigitStatus status() const { return m_status; } ///< see CaloDigitStatus

        // modifiers
        Entry& addStatus( DigitStatus s ) {
          m_status.set( s );
          return *this;
        }
        Entry& removeStatus( DigitStatus s ) {
          m_status.reset( s );
          return *this;
        }
        Entry& resetStatus() {
          m_status = CaloDigitStatus::Mask::Undefined;
          return *this;
        }
        Entry& setStatus( DigitStatus St ) {
          m_status = St;
          return *this;
        }
        Entry& setFraction( double f ) {
          m_fraction = f;
          return *this;
        }
        Entry& setEnergy( double e ) {
          m_energy = e;
          return *this;
        }
      };

      std::vector<Entry>                m_entries;
      std::vector<Cluster_>             m_clusters;
      std::vector<SpreadAndCovariance_> m_cold;

      template <typename T>
      class Proxy {
        T*           m_parent = nullptr;
        unsigned int m_offset = 0;

        friend std::remove_const_t<T>;

        decltype( auto ) clus() const { return m_parent->m_clusters[m_offset]; }
        decltype( auto ) cold() const { return m_parent->m_cold[clus().m_cold]; }

        Proxy( T* parent, unsigned int offset ) : m_parent{parent}, m_offset{offset} {}

      public:
        // allow non-const proxy to be converted to const proxy
        template <typename U,
                  typename = std::enable_if_t<!std::is_same_v<U, T> && std::is_same_v<std::add_const_t<U>, T>>>
        Proxy( Proxy<U> p ) : m_parent{p.m_parent}, m_offset{p.m_offset} {}

        auto operator-> () const { return this; }
        auto operator-> () { return this; }

        // ' rebind ' the proxy to a different container -- dangerous: use with care!
        template <typename Container>
        [[nodiscard]] auto rebind( Container& c ) const {
          assert( c.size() == m_parent->size() );
          return c[m_offset];
        }

        // observers, and, in case if T is non-const, implicitly(!) setters

        [[nodiscard]] auto& energy() const { return clus().m_energy; }
        [[nodiscard]] auto& e() const { return energy(); }
        [[nodiscard]] auto& position() const { return clus().m_position; }
        [[nodiscard]] auto& covariance() const { return cold().m_covariance; }
        [[nodiscard]] auto& spread() const { return cold().m_spread; }
        [[nodiscard]] auto& center() const { return cold().m_center; }

        // the following really cannot be modified...

        [[nodiscard]] unsigned size() const { return clus().m_entries.second; }
        /// the type of cluster
        [[nodiscard]] auto type() const { return clus().m_type; }
        /// The cellID of the seed digit for the cluster
        [[nodiscard]] auto seed() const { return cellID(); } // FIXME: deprecate!
        [[nodiscard]] auto cellID() const { return clus().cellID(); }
        /// The cluster contents itself - entries : digits with their status
        [[nodiscard]] auto entries() const {
          auto e = clus().m_entries;
          return make_span( m_parent->m_entries ).subspan( e.first, e.second );
        }
      };

      template <typename T>
      class Iterator {
        T*           m_parent = nullptr;
        unsigned int m_offset = 0;
        friend std::remove_const_t<T>;

      public:
        Iterator() = default;

        Iterator( T* parent, unsigned int offset ) : m_parent{parent}, m_offset{offset} {}

        // promote a non-const T iterator to a const T iterator
        template <typename U,
                  typename = std::enable_if_t<!std::is_same_v<U, T> && std::is_same_v<std::add_const_t<U>, T>>>
        Iterator( Iterator<U> iter ) : Iterator{iter.m_parent, iter.m_offset} {}

        decltype( auto ) operator*() const { return ( *m_parent )[m_offset]; }
        decltype( auto ) operator-> () const { return ( *m_parent )[m_offset]; }

        Iterator& operator--() {
          --m_offset;
          return *this;
        }

        Iterator& operator++() {
          ++m_offset;
          return *this;
        }

        Iterator& operator+=( int i ) {
          m_offset += i;
          return *this;
        }

        friend int operator-( const Iterator& lhs, const Iterator& rhs ) {
          assert( lhs.m_parent == rhs.m_parent );
          return lhs.m_offset - rhs.m_offset;
        }

        friend bool operator==( const Iterator& lhs, const Iterator& rhs ) {
          assert( lhs.m_parent == rhs.m_parent );
          return lhs.m_offset == rhs.m_offset;
        }

        friend bool operator!=( const Iterator& lhs, const Iterator& rhs ) { return !( lhs == rhs ); }
      };

      template <typename T>
      class Range_ {
        T*           m_parent = nullptr;
        unsigned int m_begin  = 0;
        unsigned int m_end    = 0;

        friend std::remove_const_t<T>;
        Range_( T* parent, unsigned int first, unsigned int last ) : m_parent{parent}, m_begin{first}, m_end( last ) {
          assert( m_end >= m_begin );
        }

      public:
        // allow non-const range to be converted to const range
        template <typename U,
                  typename = std::enable_if_t<!std::is_same_v<U, T> && std::is_same_v<std::add_const_t<U>, T>>>
        Range_( Range_<U> p ) : Range_{p.m_parent, p.m_begin, p.m_end} {}
        Range_( T& parent ) : Range_{&parent, 0, parent.size()} {}

        template <typename U,
                  typename = std::enable_if_t<std::is_same_v<U, T> || std::is_same_v<std::add_const_t<U>, T>>>
        Range_( Proxy<U> p ) : Range_{p.m_parent, p.m_offset, p.m_offset + 1} {}
        [[nodiscard]] auto     begin() const { return Iterator{m_parent, m_begin}; }
        [[nodiscard]] auto     end() const { return Iterator{m_parent, m_end}; }
        [[nodiscard]] bool     empty() const { return m_begin == m_end; }
        [[nodiscard]] unsigned size() const {
          assert( m_end >= m_begin );
          return m_end - m_begin;
        }

        [[nodiscard]] decltype( auto ) front() const {
          assert( !empty() );
          return ( *m_parent )[m_begin];
        }
        [[nodiscard]] decltype( auto ) back() const {
          assert( !empty() );
          return ( *m_parent )[m_end - 1];
        }
        [[nodiscard]] decltype( auto ) operator[]( unsigned int idx ) const {
          assert( idx < size() );
          return ( *m_parent )[m_begin + idx];
        }

        [[nodiscard]] Range_ first( unsigned int N ) const {
          assert( N <= size() );
          return {m_parent, m_begin, m_begin + N};
        }
        [[nodiscard]] Range_ last( unsigned int N ) const {
          assert( N <= size() );
          return {m_parent, m_end - N, m_end};
        }
        [[nodiscard]] Range_ subspan( unsigned int first, unsigned int N ) const {
          assert( first + N <= size() );
          return {m_parent, m_begin + first, m_begin + first + N};
        }
        [[nodiscard]] Range_ subspan( unsigned int first ) const {
          assert( m_begin + first <= m_end );
          return {m_parent, m_begin + first, m_end};
        }
      };

      template <typename T>
      class IndexView {
        T*                                            m_parent = nullptr;
        std::array<int16_t, LHCb::Calo::Index::max()> m_index;

      public:
        explicit IndexView( T* parent ) : m_parent{parent} {
          if ( !m_parent ) return;
          m_index.fill( -1 );
          for ( auto&& [i, cluster] : range::enumerate( *m_parent, int16_t{0} ) ) {
            auto& idx = m_index[LHCb::Calo::Index{cluster.cellID()}];
            if ( UNLIKELY( idx != -1 ) ) {
              // oops -- not unique, so flag, and just give up...
              m_parent = nullptr;
              return;
            }
            idx = i;
          }
        }
        [[nodiscard]] explicit operator bool() const { return m_parent != nullptr; }
        [[nodiscard]] auto     find( CaloCellID id ) const {
          if ( !*this ) throw;
          auto i = m_index[LHCb::Calo::Index{id}];
          return i < 0 ? m_parent->end() : Iterator{m_parent, static_cast<unsigned int>( i )};
        }
        [[nodiscard]] auto begin() const { return m_parent->begin(); }
        [[nodiscard]] auto end() const { return m_parent->end(); }
      };

      [[nodiscard]] bool noDuplicateEntries( int first, int N ) const {
        assert( N >= 0 );
        std::set<LHCb::CaloCellID> s;
        std::generate_n( std::inserter( s, s.end() ), N, [&] { return m_entries[first++].cellID(); } );
        return s.size() == static_cast<unsigned int>( N );
      }

    public:
      using reference       = Proxy<Clusters>;
      using const_reference = Proxy<const Clusters>;
      using Range           = Range_<Clusters>;
      using const_Range     = Range_<const Clusters>;
      using Entries         = LHCb::span<Entry>;
      using const_Entries   = LHCb::span<const Entry>;

      Clusters() = default;

      Clusters( Clusters&& ) = default;
      Clusters& operator=( Clusters&& ) = default;
      Clusters( const Clusters& )       = default;
      Clusters& operator=( const Clusters& ) = default;

      [[nodiscard]] auto operator[]( unsigned int idx ) const {
        assert( idx < size() );
        return Proxy{this, idx};
      }
      [[nodiscard]] auto operator[]( unsigned int idx ) {
        assert( idx < size() );
        return Proxy{this, idx};
      }

      void reserveForClusters( int n ) {
        m_clusters.reserve( n );
        m_cold.reserve( n );
        m_entries.reserve( 9 * n );
      }
      void reserveForEntries( int n ) {
        m_entries.reserve( n );
        m_clusters.reserve( n / 3 );
        m_cold.reserve( n / 3 );
      }

      struct which_entries_t {
        int first;
        int N = -1;
      };
      auto emplace_back( CaloCellID seed, Type t, which_entries_t entries, double energy, Gaudi::XYZPoint position ) {
        if ( entries.N == -1 ) entries.N = m_entries.size() - entries.first;
        assert( entries.first + entries.N <= static_cast<int>( m_entries.size() ) );
        assert( noDuplicateEntries( entries.first, entries.N ) );
        assert( std::all_of( std::next( m_entries.begin(), entries.first ),
                             std::next( m_entries.begin(), entries.first + entries.N ),
                             []( const auto& e ) { return static_cast<bool>( e.cellID() ); } ) );
        m_clusters.emplace_back( t, seed, std::pair{entries.first, entries.N}, energy, std::move( position ),
                                 m_cold.size() );
        m_cold.emplace_back();
        return Proxy{this, size() - 1};
      }

      auto emplace_back( const_reference r ) {
        auto        first   = m_entries.size();
        const auto& r_e     = r.m_parent->m_entries;
        const auto& r_c     = r.clus();
        auto        r_first = std::next( r_e.begin(), r_c.m_entries.first );
        m_entries.insert( m_entries.end(), r_first, r_first + r_c.m_entries.second );
        auto& c_c     = m_clusters.emplace_back( r_c );
        c_c.m_entries = {first, m_entries.size() - first};
        c_c.m_cold    = m_cold.size();
        m_cold.push_back( r.m_parent->m_cold[r_c.m_cold] );
        assert( size() > 0 );
        return Proxy{this, size() - 1};
      }

      [[nodiscard]] auto cbegin() const { return Iterator{this, 0}; }
      [[nodiscard]] auto cend() const { return Iterator{this, size()}; }
      [[nodiscard]] auto begin() const { return cbegin(); }
      [[nodiscard]] auto end() const { return cend(); }
      [[nodiscard]] auto begin() { return Iterator{this, 0}; }
      [[nodiscard]] auto end() { return Iterator{this, size()}; }

      [[nodiscard]] auto range() const { return Range_{this, 0, size()}; }
      [[nodiscard]] auto range() { return Range_{this, 0, size()}; }

      [[nodiscard]] auto index() const { return IndexView{this}; }

      [[nodiscard]] bool     empty() const { return m_clusters.empty(); }
      [[nodiscard]] unsigned size() const { return m_clusters.size(); }

      const auto& emplace_back( CaloCellID id, double energy, DigitStatus status ) {
        assert( static_cast<bool>( id ) );
        return m_entries.emplace_back( id, energy, status );
      }
      const auto& emplace_back( CaloCellID id, double energy, double fraction, DigitStatus status ) {
        assert( static_cast<bool>( id ) );
        return m_entries.emplace_back( id, energy, fraction, status );
      }
      void push_back( Entry const& e ) {
        assert( static_cast<bool>( e.cellID() ) );
        m_entries.push_back( e );
      }
      [[nodiscard]] int  totalNumberOfEntries() const { return m_entries.size(); }
      [[nodiscard]] auto range_of_entries( int first = 0, int N = -1 ) {
        auto s = make_span( m_entries );
        return N < 0 ? s.subspan( first ) : s.subspan( first, N );
      }

      template <typename Det>
      void sort( LHCb::CaloDataFunctor::EnergyTransverse<Det> et ) {
        // having an index to m_cold (and indices into m_entries) in m_clusters avoids having to touch m_cold (or
        // m_entries)...
        std::sort( m_clusters.begin(), m_clusters.end(),
                   [et = et]( const auto& lhs, const auto& rhs ) { return et( &lhs ) > et( &rhs ); } );
      }

      Iterator<Clusters> erase( Iterator<Clusters> it ) {
        // note: we leave the entries for the erased cluster 'as-is' as otherwise we would have to potentially re-index
        // _all_ clusters... TODO: should we flag the redundant/unused entries as such?
        assert( it.m_parent == this );
        assert( it != end() );
        m_clusters.erase( std::next( m_clusters.begin(), it.m_offset ) );
        return it; // this iterator now points to the next element...
      }

      template <typename Predicate,
                typename = std::enable_if_t<std::is_invocable_r_v<bool, Predicate, Proxy<Clusters>>>>
      Iterator<Clusters> remove_if( Predicate p ) {
        auto first = begin();
        while ( first != end() ) {
          if ( std::invoke( p, *first ) ) {
            first = erase( first );
          } else {
            ++first;
          }
        }
        return first;
      }
    };

    using Cluster = Clusters::const_reference;

  } // namespace v2
} // namespace LHCb::Event::Calo

namespace std {
  template <>
  struct iterator_traits<LHCb::Event::Calo::v2::Clusters::Iterator<LHCb::Event::Calo::v2::Clusters>> {
    using difference_type   = int;
    using iterator_category = random_access_iterator_tag;
  };
  template <>
  struct iterator_traits<LHCb::Event::Calo::v2::Clusters::Iterator<const LHCb::Event::Calo::v2::Clusters>> {
    using difference_type   = int;
    using iterator_category = random_access_iterator_tag;
  };
} // namespace std
