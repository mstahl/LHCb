/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/SOACollection.h"
#include "Kernel/EventLocalAllocator.h"

/** @file  PrMuonPIDs.h
 *  @brief Definition of object for MuonPID.
 */
namespace LHCb::Pr::Muon {
  enum StatusMasks { IsMuon = 0x1, InAcceptance = 0x2, PreSelMomentum = 0x4, IsMuonLoose = 0x8, IsMuonTight = 0x10 };

  namespace details {
    template <StatusMasks Mask>
    constexpr int setFlag( int input, bool value ) {
      return input ^ ( ( -value ^ input ) & Mask );
    }
  } // namespace details

  namespace V2 = LHCb::v2::Event;

  namespace Tag {
    struct Status : V2::int_field {};
    struct Chi2Corr : V2::float_field {};

    template <typename T>
    using pids_t = V2::SOACollection<T, Status, Chi2Corr>;
  } // namespace Tag

  struct PIDs : Tag::pids_t<PIDs> {
    using base_t = typename Tag::pids_t<PIDs>;
    using base_t::base_t;

    // Define an optional custom proxy for this track
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct PIDProxy : V2::Proxy<simd, behaviour, ContainerType> {
      using V2::Proxy<simd, behaviour, ContainerType>::Proxy;
      using simd_t = SIMDWrapper::type_map_t<simd>;
      using int_v  = typename simd_t::int_v;

      [[nodiscard]] auto IsMuon() const {
        auto status = this->template get<Tag::Status>();
        return !( ( status & int_v{StatusMasks::IsMuon} ) == int_v{0} );
      }
      [[nodiscard]] auto Chi2Corr() const { return this->template get<Tag::Chi2Corr>(); }
    };
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = PIDProxy<simd, behaviour, ContainerType>;

  private:
    DECLARE_PROXY_FRIEND( Proxy );
  };

  DECLARE_PROXY( Proxy ) {
    PROXY_METHODS( Proxy, dType, PIDs, m_pids );

  private:
    template <typename Tag, typename... Ts>
    auto loader( Ts... Is ) const { // TODO: move in PROXY_METHODS
      return this->load_vector( this->m_pids->template data<Tag>( Is... ) );
    }

  public:
    auto IsMuon() const {
      auto status = loader<Tag::Status>();
      return !( ( status & StatusMasks::IsMuon ) == 0 );
    }
    auto Chi2Corr() const { return loader<Tag::Chi2Corr>(); }
  };
} // namespace LHCb::Pr::Muon

// Register the proxy as appropriate for the container.
REGISTER_PROXY( LHCb::Pr::Muon::PIDs, LHCb::Pr::Muon::Proxy );
REGISTER_HEADER( LHCb::Pr::Muon::PIDs, "Event/PrMuonPIDs.h" );