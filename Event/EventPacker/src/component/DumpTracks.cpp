/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : DumpTracks
//
// 2004-07-14 : Marco Cattaneo
//-----------------------------------------------------------------------------

/** @class DumpTracks DumpTracks.h
 *  Dump all tracks in an event.
 *  Amount printed depends on OutputLevel:
 *    INFO: prints size of container
 *   DEBUG: prints also first "NumberOfObjectsToPrint" Tracks and States
 *          (default is 5)
 * VERBOSE: prints all Tracks and States
 *
 *  @author Marco Cattaneo
 *  @date   2004-07-14
 */
class DumpTracks : public GaudiAlgorithm {

public:
  /// Standard constructor
  DumpTracks( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  unsigned int                                            m_numObjects;     ///< Number of objects to print
  std::string                                             m_tracksLocation; ///< Location of tracks container
  mutable Gaudi::Accumulators::StatCounter<unsigned long> m_tracks{this, "#Tracks"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DumpTracks )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DumpTracks::DumpTracks( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "NumberOfObjectsToPrint", m_numObjects = 5 );
  declareProperty( "TracksLocation", m_tracksLocation = LHCb::TrackLocation::Default );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DumpTracks::execute() {
  LHCb::Tracks* tracksCont = get<LHCb::Tracks>( m_tracksLocation );

  info() << "There are " << tracksCont->size() << " tracks in " << m_tracksLocation << endmsg;

  m_tracks += tracksCont->size();

  if ( msgLevel( MSG::DEBUG ) ) {
    unsigned int numPrinted = 0;

    for ( const LHCb::Track* track : *tracksCont ) {
      if ( !msgLevel( MSG::VERBOSE ) && ++numPrinted > m_numObjects ) break;
      debug() << *track << endmsg;
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
