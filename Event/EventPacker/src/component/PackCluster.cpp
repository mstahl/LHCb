/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedCluster.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include <sstream>

//-----------------------------------------------------------------------------
// Implementation file for class : PackCluster
//
// 2012-03-05 : Olivier Callot
//-----------------------------------------------------------------------------

/** @class PackCluster PackCluster.h
 *
 *  Pack the cluster on the tracks of a specified container.
 *
 *  @author Olivier Callot
 *  @date   2012-03-05
 */
class PackCluster : public GaudiAlgorithm {

public:
  /// Standard constructor
  PackCluster( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  std::string m_inputName;   ///< Input Track location
  std::string m_outputName;  ///< Output location for packed clusters
  std::string m_veloClusLoc; ///< Velo clusters location
  std::string m_ttClusLoc;   ///< TT clusters location
  std::string m_utClusLoc;   ///< UT clusters location
  std::string m_itClusLoc;   ///< IT clusters location

  bool m_alwaysOutput; ///< Flag to turn on the creation of output, even when input is missing
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PackCluster )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PackCluster::PackCluster( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "InputName", m_inputName = LHCb::TrackLocation::Default );
  declareProperty( "OutputName", m_outputName = LHCb::PackedClusterLocation::Default );
  declareProperty( "UTClusters", m_utClusLoc = LHCb::UTClusterLocation::UTClusters );
  declareProperty( "AlwaysCreateOutput", m_alwaysOutput = false );
  // setProperty( "OutputLevel", 1 );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PackCluster::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // If input does not exist, and we aren't making the output regardless, just return
  if ( !m_alwaysOutput && !exist<LHCb::Tracks>( m_inputName ) ) return StatusCode::SUCCESS;

  // Check to see if packed output already exists. If it does print a warning and return
  LHCb::PackedClusters* out = getIfExists<LHCb::PackedClusters>( m_outputName );
  if ( out ) {
    return Warning( "Packed Clusters already exist at '" + m_outputName + "' -> Abort", StatusCode::SUCCESS, 3 );
  }

  // Create and save the output container
  out = new LHCb::PackedClusters();
  put( out, m_outputName );

  // Load the input. If not existing just return
  const LHCb::Tracks* tracks = getIfExists<LHCb::Tracks>( m_inputName );
  if ( !tracks ) return StatusCode::SUCCESS;

  // Select LHCbIDs from the input tracks
  std::vector<LHCb::LHCbID> allIds;
  allIds.reserve( tracks->size() * 40 );

  // Sort and remove duplicate LHCbIDs
  std::sort( allIds.begin(), allIds.end() );
  allIds.erase( std::unique( allIds.begin(), allIds.end() ), allIds.end() );

  // Cluster pointers. Only loaded when really needed.
  const LHCb::UTClusters* utClus = nullptr;

  // pack the clusters
  for ( const auto& id : allIds ) {
    if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Packing " << id << endmsg; }

    if ( id.isUT() ) {
      if ( UNLIKELY( !utClus ) ) {
        utClus = getIfExists<LHCb::UTClusters>( m_utClusLoc );
        if ( !utClus ) { Warning( "Failed to load '" + m_utClusLoc + "'" ).ignore(); }
      }
      const auto* cl = ( utClus ? utClus->object( id.utID() ) : nullptr );
      if ( cl ) {
        out->addUTCluster( cl );
      } else {
        Warning( "Failed to locate a UT cluster. Activate debug for details" ).ignore();
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Unknown UT cluster : " << id << endmsg;
      }
    } else {
      Warning( "Unknown LHCbID. Activate debug for details" ).ignore();
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Unknown LHCbID type : " << id << endmsg;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Stored " << out->clusters().size() << " clusters" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
