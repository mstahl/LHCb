/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// local
#include "Event/PackedCluster.h"
#include "Event/UTCluster.h"
#include "GaudiAlg/GaudiAlgorithm.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UnpackCluster
//
// 2012-03-06 : Olivier Callot
//-----------------------------------------------------------------------------

/** @class UnpackCluster UnpackCluster.h
 *  UNpack the clusters for Velo and ST
 *
 *  @author Olivier Callot
 *  @date   2012-03-06
 */
class UnpackCluster : public GaudiAlgorithm {

public:
  /// Standard constructor
  UnpackCluster( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  /// Create and access UT Clusters on demand
  LHCb::UTClusters* utClus() {
    if ( !m_utClus ) { put( m_utClus = new LHCb::UTClusters(), LHCb::UTClusterLocation::UTClusters + m_extension ); }
    return m_utClus;
  }

private:
  std::string m_inputName;    ///< Input name of packed clusters
  std::string m_extension;    ///< Name extension (for testing)
  bool        m_alwaysOutput; ///< Flag to turn on the creation of output, even when input is missing

  /// Check to prevent recursive calls to this, due to the multiple possible outputs
  bool m_running = false;

  // Cache pointers to containers, for on-demand creation
  LHCb::UTClusters* m_utClus = nullptr; ///< UT Clusters
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UnpackCluster )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UnpackCluster::UnpackCluster( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "InputName", m_inputName = LHCb::PackedClusterLocation::Default );
  declareProperty( "Extension", m_extension = "" );
  declareProperty( "AlwaysCreateOutput", m_alwaysOutput = false );
  // setProperty( "OutputLevel", 1 );
}

//=============================================================================
// explicit comparison for cluster sorting (hidden in anonymous namespace)
//=============================================================================
namespace {
  template <class TYPE>
  inline bool compareKeys( const TYPE* a, const TYPE* b ) {
    return ( a->key() < b->key() );
  }
} // namespace

//=============================================================================
// Main execution
//=============================================================================
StatusCode UnpackCluster::execute() {
  if ( m_running ) return StatusCode::SUCCESS;
  m_running = true;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // If input does not exist, and we aren't making the output regardless, just return
  if ( !m_alwaysOutput && !exist<LHCb::PackedClusters>( m_inputName ) ) return StatusCode::SUCCESS;

  // If any clusters already exist, return
  if ( exist<LHCb::UTClusters>( LHCb::UTClusterLocation::UTClusters + m_extension ) ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Clusters already exist -> Quitting" << endmsg;
    return StatusCode::SUCCESS;
  }

  // Reset cluster pointers, to force new ones to be creatd when needed next time
  m_utClus = nullptr;

  // Get the packed data
  const LHCb::PackedClusters* dst = getOrCreate<LHCb::PackedClusters, LHCb::PackedClusters>( m_inputName );
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Size of packed clusters = " << dst->clusters().size() << " clusters" << endmsg;
  }

  // Fill the clusters
  for ( const auto& clu : dst->clusters() ) {

    LHCb::UTCluster::ADCVector adcs;
    for ( unsigned int kk = clu.begin; clu.end != kk; ++kk ) {
      adcs.push_back( std::pair<int, unsigned int>( dst->strips()[kk], dst->adcs()[kk] ) );
    }

  } // end loop over clusters

  // Sort any filled containers.
  if ( m_utClus ) { std::sort( m_utClus->begin(), m_utClus->end(), compareKeys<LHCb::UTCluster> ); }

  //== If we stored in a different location, compare...
  if ( UNLIKELY( !m_extension.empty() ) ) {
    LHCb::UTClusters* utRef = get<LHCb::UTClusters>( LHCb::UTClusterLocation::UTClusters );
    for ( LHCb::UTClusters::iterator itU = utClus()->begin(); utClus()->end() != itU; ++itU ) {
      LHCb::UTCluster* sCl  = *itU;
      LHCb::UTCluster* sOld = utRef->object( sCl->key() );
      if ( ( sOld->interStripFraction() != sCl->interStripFraction() ) || ( sOld->pseudoSize() != sCl->pseudoSize() ) ||
           ( sOld->highThreshold() != sCl->highThreshold() ) || ( sOld->stripValues() != sCl->stripValues() ) ) {
        info() << "Old UT Cluster "
               << format( "frac%5.2f size%3d thr%2d ", sOld->interStripFraction(), sOld->pseudoSize(),
                          sOld->highThreshold() )
               << endmsg;
        info() << " new           "
               << format( "frac%5.2f size%3d thr%2d ", sCl->interStripFraction(), sCl->pseudoSize(),
                          sCl->highThreshold() )
               << endmsg;
      }
    }
  }

  m_running = false;
  return StatusCode::SUCCESS;
}

//=============================================================================
