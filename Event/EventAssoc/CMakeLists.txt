###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
gaudi_subdir(EventAssoc)

gaudi_depends_on_subdirs(Event/GenEvent
                         Event/MCEvent
                         Event/PhysEvent
                         Event/TrackEvent
                         GaudiPython
                         Kernel/Relations)

gaudi_add_dictionary(EventAssocPhys
                     dict/selPhys.h
                     dict/selPhys.xml
                     LINK_LIBRARIES GenEvent MCEvent PhysEvent TrackEvent RelationsLib)

gaudi_add_dictionary(EventAssocMC
                     dict/selMC.h
                     dict/selMC.xml
                     LINK_LIBRARIES GenEvent MCEvent PhysEvent TrackEvent RelationsLib)

