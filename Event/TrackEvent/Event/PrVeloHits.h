/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/SOACollection.h"
#include "Kernel/EventLocalAllocator.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
#include "PrTracksTag.h"
#include "SOAExtensions/ZipUtils.h"

/**
 * Hits in VP
 *
 * @author: Arthur Hennequin
 */
namespace V2 = LHCb::v2::Event;
namespace LHCb::Pr::Velo {

  namespace VPHitsTag {
    struct pos : V2::vec3_field {};
    struct ChannelId : V2::int_field {};

    template <typename T>
    using velohit_t = V2::SOACollection<T, pos, ChannelId>;
  } // namespace VPHitsTag

  struct Hits : VPHitsTag::velohit_t<Hits> {
    using base_t = typename VPHitsTag::velohit_t<Hits>;
    using base_t::base_t;
  };

} // namespace LHCb::Pr::Velo
