/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/PrLongTracks.h"
#include "Event/PrProxyHelpers.h"
#include "Event/PrTracksTag.h"
#include "Event/Zip.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "Kernel/meta_enum.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
#include "SOAExtensions/ZipUtils.h"

/**
 * Track data after the Kalman fit
 *
 * @author: Arthur Hennequin
 * 2020-08-17 updated to SOACollection by Peilian Li as Forward
 * 2021-27-01 updated to SOACollection by Alex Gilman as Generic
 */

namespace V2         = LHCb::v2::Event;
namespace TracksInfo = LHCb::Pr::TracksInfo;

namespace LHCb::v2::Event {

  /// Define the enum defining the location of a state
  meta_enum_class( StateLocation, int, Unknown = -1, ClosestToBeam, FirstMeasurement, LastMeasurement, BegRich1,
                   EndRich1, BegRich2, EndRich2 ) // Unknown unused in indexing! Indexing starts with ClosestToBeam=0

      constexpr unsigned int
      operator+( unsigned int i, StateLocation s ) {
    ASSUME( s != StateLocation::Unknown );
    return i + static_cast<std::underlying_type_t<StateLocation>>( s );
  }
  constexpr unsigned int operator+( StateLocation s, unsigned int i ) { return i + s; }

  /// Collection of states, expressed as template arguments
  template <StateLocation... L>
  struct state_collection {
    static constexpr std::size_t size = sizeof...( L );
  };

  // Needed to automatically create a collection of states given the total
  // number of states (skip Unknown)
  template <std::size_t... I>
  constexpr auto make_state_collection_impl( std::index_sequence<I...> ) {
    constexpr auto members = members_of<StateLocation>();
    return state_collection<members[I + 1]...>{};
  }

  /// Instantiation of the state_collection template that contains all the
  // available track states (except for Unknown)
  using all_states_t =
      decltype( make_state_collection_impl( std::make_index_sequence<StateLocation_meta.members.size() - 1>() ) );

  /// Track types
  meta_enum_class( TrackType, int,
                   Unknown = 0, // Unknown track type
                   Velo,        // VELO track
                   Long,        // forward track
                   Upstream,    // upstream track
                   Downstream,  // downstream track
                   Ttrack,      // seed track
                   Muon,        // muon track
                   Calo,        // calo cosmics track
                   UT           // UT track
                   )

      namespace {
    using SL = StateLocation;
  }

  /// Define the states available for a given track type
  template <TrackType T>
  struct available_states {};

  template <>
  struct available_states<TrackType::Unknown> {
    using type = state_collection<>;
  };

  template <>
  struct available_states<TrackType::Velo> {
    using type = state_collection<SL::ClosestToBeam, SL::FirstMeasurement, SL::LastMeasurement>;
  };

  template <>
  struct available_states<TrackType::Long> {
    using type = state_collection<SL::ClosestToBeam, SL::FirstMeasurement, SL::LastMeasurement, SL::BegRich1,
                                  SL::EndRich1, SL::BegRich2, SL::EndRich2>;
  };

  template <>
  struct available_states<TrackType::Upstream> {
    using type =
        state_collection<SL::ClosestToBeam, SL::FirstMeasurement, SL::LastMeasurement, SL::BegRich1, SL::EndRich1>;
  };

  template <>
  struct available_states<TrackType::Downstream> {
    using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement, SL::BegRich2, SL::EndRich2>;
  };

  template <>
  struct available_states<TrackType::Ttrack> {
    using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement>;
  };

  template <>
  struct available_states<TrackType::Muon> {
    using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement>;
  };

  template <>
  struct available_states<TrackType::Calo> {
    using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement>;
  };

  template <>
  struct available_states<TrackType::UT> {
    using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement>;
  };

  template <TrackType T>
  using available_states_t = typename available_states<T>::type;

  /// Struct to get a state location by index at compile-time
  template <std::size_t I, class T>
  struct state_at;

  template <std::size_t I, StateLocation S0, StateLocation... S>
  struct state_at<I, state_collection<S0, S...>> {
    static constexpr auto value = state_at<I - 1, state_collection<S...>>::value;
  };

  template <StateLocation S0, StateLocation... S>
  struct state_at<0, state_collection<S0, S...>> {
    static constexpr auto value = S0;
  };

  /// Alias to the value of "state_at"
  template <std::size_t I, class StateCollection>
  inline constexpr auto state_at_v = state_at<I, StateCollection>::value;

  namespace detail {

    /// Implementation of the "has_state" function
    template <TrackType T, std::size_t... I>
    bool has_state_impl( StateLocation sl, std::index_sequence<I...> ) {
      return ( ( state_at_v<I, available_states_t<T>> == sl ) || ... );
    }

    // Needed for the "Unknown" track type
    template <TrackType T>
    bool has_state_impl( StateLocation, std::index_sequence<> ) {
      return false;
    }

    /// Check if a state location is available for a given track type
    template <TrackType T>
    bool has_state( StateLocation sl ) {
      return has_state_impl<T>( sl, std::make_index_sequence<available_states_t<T>::size>() );
    }

    /** Helper type for fitted track proxies **/
    template <typename TrackProxy>
    struct FittedState {
      FittedState( TrackProxy proxy, StateLocation StateLoc ) : m_proxy{std::move( proxy )}, m_StateLoc{StateLoc} {}
      decltype( auto ) qOverP() const { return m_proxy.qOverP( m_StateLoc ); }
      decltype( auto ) momentum() const { return m_proxy.momentum( m_StateLoc ); }
      decltype( auto ) slopes() const { return m_proxy.direction( m_StateLoc ); }
      decltype( auto ) position() const { return m_proxy.position( m_StateLoc ); }
      decltype( auto ) location() const { return m_StateLoc; }
      decltype( auto ) covariance() const { return m_proxy.covariance( m_StateLoc ); }
      decltype( auto ) x() const { return position().X(); }
      decltype( auto ) y() const { return position().Y(); }
      decltype( auto ) z() const { return position().Z(); }
      decltype( auto ) tx() const { return slopes().X(); }
      decltype( auto ) ty() const { return slopes().Y(); }

    private:
      TrackProxy    m_proxy;
      StateLocation m_StateLoc;
    };
  } // namespace detail

  namespace Tag {

    struct trackVP : V2::int_field {};
    struct trackUT : V2::int_field {};
    struct trackSeed : V2::int_field {};
    struct trackFT : V2::int_field {};
    struct nVPHits : V2::int_field {};
    struct nUTHits : V2::int_field {};
    struct nFTHits : V2::int_field {};
    struct Chi2 : V2::float_field {};
    struct Chi2nDoF : V2::int_field {};
    struct Chi2PerDoF : V2::float_field {};

    // LHCbID Storage
    struct LHCbIDs : V2::ints_field<TracksInfo::MaxVPHits + TracksInfo::MaxUTHits + TracksInfo::MaxFTHits> {};
    struct numLHCbIDs : V2::int_field {};
    struct numVPLHCbIDs : V2::int_field {};
    struct numUTLHCbIDs : V2::int_field {};
    struct numFTLHCbIDs : V2::int_field {};

    // Here Max Num States set assuming the possible required states are
    // 1) Closest to Beam
    // 2) FirstMeasurement
    // 3) LastMeasurement
    // 4-7) Four states for the RICH
    inline constexpr std::size_t MaxNumStates = StateLocation_meta.members.size() - 1; // skip Unknown
    //
    struct State : V2::states_field<MaxNumStates> {};
    struct State_QoverP : V2::floats_field<MaxNumStates> {};

    constexpr std::size_t x_index  = 0;
    constexpr std::size_t y_index  = 1;
    constexpr std::size_t z_index  = 2;
    constexpr std::size_t tx_index = 3;
    constexpr std::size_t ty_index = 4;

    struct StateCov : V2::state_covs_field<MaxNumStates> {};

    constexpr std::size_t x_x_index     = 0;
    constexpr std::size_t x_y_index     = 1;
    constexpr std::size_t x_tx_index    = 2;
    constexpr std::size_t x_ty_index    = 3;
    constexpr std::size_t x_QoP_index   = 4;
    constexpr std::size_t y_y_index     = 5;
    constexpr std::size_t y_tx_index    = 6;
    constexpr std::size_t y_ty_index    = 7;
    constexpr std::size_t y_QoP_index   = 8;
    constexpr std::size_t tx_tx_index   = 9;
    constexpr std::size_t tx_ty_index   = 10;
    constexpr std::size_t tx_QoP_index  = 11;
    constexpr std::size_t ty_ty_index   = 12;
    constexpr std::size_t ty_QoP_index  = 13;
    constexpr std::size_t QoP_QoP_index = 14;

    template <typename T>
    using SOATrack_t = V2::SOACollection<T, trackFT, Chi2, Chi2nDoF, LHCbIDs, numVPLHCbIDs, numUTLHCbIDs, numFTLHCbIDs,
                                         State, State_QoverP, StateCov>;
  } // namespace Tag

  struct Tracks : Tag::SOATrack_t<Tracks> {
    using base_t = typename Tag::SOATrack_t<Tracks>;

    using base_t::allocator_type;
    Tracks( enum TrackType trackType, Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(),
            allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}, m_trackType{trackType} {

      if ( m_trackType == TrackType::Unknown ) throw std::invalid_argument( "Track type must not be Unknown" );
    }

    // Special constructor for zipping machinery
    Tracks( Zipping::ZipFamilyNumber zipIdentifier, Tracks const& other )
        : base_t{std::move( zipIdentifier ), other}, m_trackType{other.m_trackType} {}

    [[nodiscard]] auto type() const { return m_trackType; }

    // Define an optional custom proxy for this track
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct FittedProxy : V2::Proxy<simd, behaviour, ContainerType> {
      using V2::Proxy<simd, behaviour, ContainerType>::Proxy;

      [[nodiscard]] auto type() const { return m_trackType; }

      [[nodiscard]] auto qOverP( StateLocation StateLoc ) const {
        if ( !this->has_state( StateLoc ) ) { throw std::invalid_argument( "State not defined for this track type." ); }
        return this->template get<Tag::State_QoverP>( StateLoc );
      }
      [[nodiscard]] auto p( StateLocation StateLoc ) const {
        if ( !this->has_state( StateLoc ) ) { throw std::invalid_argument( "State not defined for this track type." ); }
        return abs( 1.f / qOverP( StateLoc ) );
      }
      [[nodiscard]] auto pt( StateLocation StateLoc ) const {
        if ( !this->has_state( StateLoc ) ) { throw std::invalid_argument( "State not defined for this track type." ); }
        auto const mom    = p( StateLoc );
        auto const tx     = this->template get<Tag::State>( StateLoc ).tx();
        auto const ty     = this->template get<Tag::State>( StateLoc ).ty();
        auto const tx2ty2 = tx * tx + ty * ty;
        auto const pt2    = mom * mom * tx2ty2 / ( tx2ty2 + 1 );
        using std::sqrt;
        return sqrt( pt2 );
      }
    };
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = FittedProxy<simd, behaviour, ContainerType>;

  private:
    enum TrackType m_trackType;

    /// Check if the tracks have the information of the given state
    bool has_state( StateLocation StateLoc ) const {

      switch ( m_trackType ) {
      case TrackType::Unknown:
        return detail::has_state<TrackType::Unknown>( StateLoc );
      case TrackType::Velo:
        return detail::has_state<TrackType::Velo>( StateLoc );
      case TrackType::Long:
        return detail::has_state<TrackType::Long>( StateLoc );
      case TrackType::Upstream:
        return detail::has_state<TrackType::Upstream>( StateLoc );
      case TrackType::Downstream:
        return detail::has_state<TrackType::Downstream>( StateLoc );
      case TrackType::Ttrack:
        return detail::has_state<TrackType::Ttrack>( StateLoc );
      case TrackType::Muon:
        return detail::has_state<TrackType::Muon>( StateLoc );
      case TrackType::Calo:
        return detail::has_state<TrackType::Calo>( StateLoc );
      case TrackType::UT:
        return detail::has_state<TrackType::UT>( StateLoc );
      }

      __builtin_unreachable();
    }

    DECLARE_PROXY_FRIEND( TrackProxy );
  };

  DECLARE_PROXY( TrackProxy ) {
    PROXY_METHODS( TrackProxy, dType, Tracks, m_Tracks );
    using IType = typename dType::int_v;
    using FType = typename dType::float_v;

  private:
    template <typename Tag, typename... Ts>
    auto loader( Ts... Is ) const { // TODO: move in PROXY_METHODS
      return this->load_vector( this->m_Tracks->template data<Tag>( Is... ) );
    }

  public:
    [[nodiscard]] auto trackFT() const { return loader<Tag::trackFT>(); }
    [[nodiscard]] auto LHCbIDs() const { return loader<Tag::LHCbIDs>(); }
    [[nodiscard]] auto numVPLHCbIDs() const { return loader<Tag::numVPLHCbIDs>(); }
    [[nodiscard]] auto numUTLHCbIDs() const { return loader<Tag::numUTLHCbIDs>(); }
    [[nodiscard]] auto numFTLHCbIDs() const { return loader<Tag::numFTLHCbIDs>(); }
    [[nodiscard]] auto numLHCbIDs() const { return this->numVPIDs() + this->numUTIDs() + this->numFTIDs(); }
    [[nodiscard]] auto has_state( StateLocation StateLoc ) const { return this->m_Tracks->has_state( StateLoc ); }
    [[nodiscard]] auto qOverP( StateLocation StateLoc ) const {
      if ( !this->m_Tracks->has_state( StateLoc ) ) {
        throw std::invalid_argument( "State not defined for this track type." );
      }
      return loader<Tag::State_QoverP>( StateLoc );
    }
    [[nodiscard]] auto p( StateLocation StateLoc ) const {
      if ( !this->m_Tracks->has_state( StateLoc ) ) {
        throw std::invalid_argument( "State not defined for this track type." );
      }
      return abs( 1.0 / qOverP( StateLoc ) );
    }
    [[nodiscard]] auto chi2() const { return loader<Tag::Chi2>(); }
    [[nodiscard]] auto chi2nDoF() const { return loader<Tag::Chi2nDoF>(); }
    [[nodiscard]] auto nDoF() const { return chi2nDoF(); }
    [[nodiscard]] auto chi2PerDoF() const { return chi2(); }
    auto               charge() const { return select( FType{qOverP()} > 0.f, IType{+1}, IType{-1} ); }
    auto               covariance( StateLocation StateLoc ) const {
      if ( !this->m_Tracks->has_state( StateLoc ) ) {
        throw std::invalid_argument( "State not defined for this track type." );
      }
      // This is not zero-initialised
      LHCb::LinAlg::MatSym<FType, 5> cov;
      // Each of the following represent entries of the covariance matrix. Here, the matrix has the form
      //   | X  |  Y | tX | tY | q/p|
      //  X|____|____|____|____|____|
      //  Y|____|____|____|____|____|
      // tX|____|____|____|____|____|
      // tY|____|____|____|____|____|
      // q/p|____|____|____|____|____|
      cov( 0, 0 ) = loader<Tag::StateCov>( StateLoc, Tag::x_x_index );     // x error
      cov( 0, 1 ) = loader<Tag::StateCov>( StateLoc, Tag::x_y_index );     // x-y covariance
      cov( 0, 2 ) = loader<Tag::StateCov>( StateLoc, Tag::x_tx_index );    // x-tx covariance
      cov( 0, 3 ) = loader<Tag::StateCov>( StateLoc, Tag::x_ty_index );    // x-ty covariance
      cov( 0, 4 ) = loader<Tag::StateCov>( StateLoc, Tag::x_QoP_index );   // x-qop covariance
      cov( 1, 1 ) = loader<Tag::StateCov>( StateLoc, Tag::y_y_index );     // y error
      cov( 1, 2 ) = loader<Tag::StateCov>( StateLoc, Tag::y_tx_index );    // y-tx covariance
      cov( 1, 3 ) = loader<Tag::StateCov>( StateLoc, Tag::y_ty_index );    // y-ty covariance
      cov( 1, 4 ) = loader<Tag::StateCov>( StateLoc, Tag::y_QoP_index );   // y-qop covariance
      cov( 2, 2 ) = loader<Tag::StateCov>( StateLoc, Tag::tx_tx_index );   // tx error
      cov( 2, 3 ) = loader<Tag::StateCov>( StateLoc, Tag::tx_ty_index );   // tx-ty covariance
      cov( 2, 4 ) = loader<Tag::StateCov>( StateLoc, Tag::tx_QoP_index );  // tx-qop covariance
      cov( 3, 3 ) = loader<Tag::StateCov>( StateLoc, Tag::ty_ty_index );   // ty error
      cov( 3, 4 ) = loader<Tag::StateCov>( StateLoc, Tag::ty_QoP_index );  // ty-qop covariance
      cov( 4, 4 ) = loader<Tag::StateCov>( StateLoc, Tag::QoP_QoP_index ); // qop error

      return cov;
    }

    auto position( StateLocation StateLoc ) const {
      if ( !this->m_Tracks->has_state( StateLoc ) ) {
        throw std::invalid_argument( "State not defined for this track type." );
      }
      return Vec3<FType>( loader<Tag::State>( StateLoc, Tag::x_index ), loader<Tag::State>( StateLoc, Tag::y_index ),
                          loader<Tag::State>( StateLoc, Tag::z_index ) );
    }
    auto direction( StateLocation StateLoc ) const {
      if ( !this->m_Tracks->has_state( StateLoc ) ) {
        throw std::invalid_argument( "State not defined for this track type." );
      }
      return Vec3<FType>( loader<Tag::State>( StateLoc, Tag::tx_index ), loader<Tag::State>( StateLoc, Tag::ty_index ),
                          1.f );
    }
    /** Return a proxy that exposes the a state-like API instead of the
     *  track-like API of this struct. The use of `clone<Proxy>()` discards the
     *  pointers to possible other members of the zip, which cannot be needed
     *  in the state proxy.
     */
    auto state( StateLocation StateLoc ) const {
      if ( !this->m_Tracks->has_state( StateLoc ) ) {
        throw std::invalid_argument( "State not defined for this track type." );
      }
      return detail::FittedState{full_proxy().template clone<Tracks>(), StateLoc};
    }

    auto pt( StateLocation StateLoc ) const {
      if ( !this->has_state( StateLoc ) ) { throw std::invalid_argument( "State not defined for this track type." ); }
      auto const mom    = p( StateLoc );
      auto const tx     = loader<Tag::State>( StateLoc, Tag::tx_index );
      auto const ty     = loader<Tag::State>( StateLoc, Tag::ty_index );
      auto const tx2ty2 = tx * tx + ty * ty;
      auto const pt2    = mom * mom * tx2ty2 / ( tx2ty2 + 1 );
      using std::sqrt;
      return sqrt( pt2 );
    }

    auto pseudoRapidity( StateLocation StateLoc ) const {
      if ( !this->m_Tracks->has_state( StateLoc ) ) {
        throw std::invalid_argument( "State not defined for this track type." );
      }
      return direction( StateLoc ).eta();
    }
    auto phi( StateLocation StateLoc ) const {
      if ( !this->m_Tracks->has_state( StateLoc ) ) {
        throw std::invalid_argument( "State not defined for this track type." );
      }
      return direction( StateLoc ).phi();
    }
    auto threeMomentum( StateLocation StateLoc ) const {
      if ( !this->m_Tracks->has_state( StateLoc ) ) {
        throw std::invalid_argument( "State not defined for this track type." );
      }
      auto const dir = direction( StateLoc );
      using std::sqrt;
      auto const scale = p( StateLoc ) / sqrt( dir.mag2() );
      return LHCb::LinAlg::Vec<FType, 3>{scale * dir.x, scale * dir.y, scale * dir.z};
    }
    [[nodiscard]] auto type() const { return this->m_Tracks->m_trackType; };
  };
} // namespace LHCb::v2::Event

REGISTER_PROXY( LHCb::v2::Event::Tracks, LHCb::v2::Event::TrackProxy );
REGISTER_HEADER( LHCb::v2::Event::Tracks, "Event/Track_SOA.h" );
