/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/PrProxyHelpers.h"
#include "Event/PrVeloHits.h"
#include "Event/SOACollection.h"
#include "Event/Zip.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/HeaderMapping.h"
#include "Kernel/LHCbID.h"
#include "Kernel/VPChannelID.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
#include "PrTracksTag.h"
#include "SOAExtensions/ZipUtils.h"

/**
 * Track data for exchanges between VeloTracking and UT
 *
 * @author: Arthur Hennequin
 * 2020-08-25 updated to SOACollection structure by Peilian Li
 */

namespace V2         = LHCb::v2::Event;
namespace TracksInfo = LHCb::Pr::TracksInfo;

namespace LHCb::Pr::Velo {

  namespace Tag {
    struct StateQoP : V2::float_field {};
    struct nVPHits : V2::int_field {};
    struct vp_indices : V2::ints_field<TracksInfo::MaxVPHits> {};
    struct lhcbIDs : V2::ints_field<TracksInfo::MaxVPHits> {};
    struct StatePositions : V2::floats_field<TracksInfo::NumVeloStates, TracksInfo::NumStatePars> {};
    struct StateCovXs : V2::floats_field<TracksInfo::NumVeloStates, TracksInfo::NumCovXY> {};
    struct StateCovYs : V2::floats_field<TracksInfo::NumVeloStates, TracksInfo::NumCovXY> {};

    template <typename T>
    using velo_t = V2::SOACollection<T, StateQoP, nVPHits, vp_indices, lhcbIDs, StatePositions, StateCovXs, StateCovYs>;
  } // namespace Tag

  struct Tracks : Tag::velo_t<Tracks> {
    using base_t = typename Tag::velo_t<Tracks>;
    using base_t::base_t;

    constexpr static auto NumStatePars     = TracksInfo::NumStatePars;
    constexpr static auto NumVeloStates    = TracksInfo::NumVeloStates;
    constexpr static auto NumVeloStatePars = NumStatePars * NumVeloStates;
    constexpr static auto NumCovXY         = TracksInfo::NumCovXY;
    constexpr static auto NumVeloStateCov  = TracksInfo::NumVeloStateCov;
    constexpr static auto MaxVPHits        = TracksInfo::MaxVPHits;

    template <typename F, typename M = std::true_type>
    void store_StatePosDir( int at, int state, Vec3<F> const& pos, Vec3<F> const& dir, M&& mask = {} ) {
      store<Tag::StatePositions>( at, state, 0, pos.x, mask );
      store<Tag::StatePositions>( at, state, 1, pos.y, mask );
      store<Tag::StatePositions>( at, state, 2, pos.z, mask );
      store<Tag::StatePositions>( at, state, 3, dir.x, mask );
      store<Tag::StatePositions>( at, state, 4, dir.y, mask );
    }
    template <typename F, typename M = std::true_type>
    void store_StateCovXY( int at, int state, Vec3<F> const& covx, Vec3<F> const& covy, M&& mask = {} ) {
      store<Tag::StateCovXs>( at, state, 0, covx.x, mask );
      store<Tag::StateCovXs>( at, state, 1, covx.y, mask );
      store<Tag::StateCovXs>( at, state, 2, covx.z, mask );
      store<Tag::StateCovYs>( at, state, 0, covy.x, mask );
      store<Tag::StateCovYs>( at, state, 1, covy.y, mask );
      store<Tag::StateCovYs>( at, state, 2, covy.z, mask );
    }
    template <typename I, typename M = std::true_type>
    void store_vp_index( int at, int i, I const& vpidx, M&& mask = {} ) {
      store<Tag::vp_indices>( at, i, vpidx, mask );
    }
    template <typename I, typename M = std::true_type>
    void store_lhcbID( int at, int i, I const& lhcbid, M&& mask = {} ) {
      store<Tag::lhcbIDs>( at, i, lhcbid, mask );
    }

  private:
    DECLARE_PROXY_FRIEND( TrackProxy );
  };

  DECLARE_PROXY( TrackProxy ) {
    PROXY_METHODS( TrackProxy, dType, Tracks, m_Tracks );
    using IType = typename dType::int_v;
    using FType = typename dType::float_v;

  private:
    template <typename Tag, typename... Ts>
    auto loader( Ts... Is ) const { // TODO: move in PROXY_METHODS
      return this->load_vector( this->m_Tracks->template data<Tag>( Is... ) );
    }

  public:
    [[nodiscard]] auto nHits() const { return loader<Tag::nVPHits>(); }
    [[nodiscard]] auto vp_index( std::size_t i ) const { return loader<Tag::vp_indices>( i ); }
    [[nodiscard]] auto vp_indices() const {
      std::array<IType, TracksInfo::MaxVPHits> out = {0};
      for ( auto i = 0; i < nHits().hmax( this->loop_mask() ); i++ ) out[i] = vp_index( i );
      return out;
    }
    [[nodiscard]] auto lhcbID( std::size_t i ) const { return loader<Tag::lhcbIDs>( i ); }
    [[nodiscard]] auto StatePosElement( std::size_t i, std::size_t j ) const {
      return loader<Tag::StatePositions>( i, j );
    }
    [[nodiscard]] auto StateCovXElement( std::size_t i, std::size_t j ) const {
      return loader<Tag::StateCovXs>( i, j );
    }
    [[nodiscard]] auto StateCovYElement( std::size_t i, std::size_t j ) const {
      return loader<Tag::StateCovYs>( i, j );
    }

    // Retrieve state info
    [[nodiscard]] auto StatePosDir( std::size_t i ) const {
      using LHCb::Utils::unwind;
      LHCb::LinAlg::Vec<decltype( this->StatePosElement( 0, 0 ) ), TracksInfo::NumStatePars> out;
      unwind<0, TracksInfo::NumStatePars>( [&]( auto j ) { out( j ) = this->StatePosElement( i, j ); } );
      return out;
    }
    [[nodiscard]] auto StatePos( std::size_t i ) const {
      return Vec3<FType>( StatePosDir( i )( 0 ), StatePosDir( i )( 1 ), StatePosDir( i )( 2 ) );
    }
    [[nodiscard]] auto StateDir( std::size_t i ) const {
      return Vec3<FType>( StatePosDir( i )( 3 ), StatePosDir( i )( 4 ), 1.f );
    }
    [[nodiscard]] auto StateCovX( std::size_t i ) const {
      return Vec3<FType>( StateCovXElement( i, 0 ), StateCovXElement( i, 1 ), StateCovXElement( i, 2 ) );
    }
    [[nodiscard]] auto StateCovY( std::size_t i ) const {
      return Vec3<FType>( StateCovYElement( i, 0 ), StateCovYElement( i, 1 ), StateCovYElement( i, 2 ) );
    }
    auto pseudoRapidity() const { return StateDir( 0 ).eta(); }
    auto phi() const { return StateDir( 0 ).phi(); }
    auto closestToBeamStatePos() const { return StatePos( 0 ); }
    auto closestToBeamStateDir() const { return StateDir( 0 ); }
    auto closestToBeamState() const { return LHCb::Pr::detail::VeloState{StatePos( 0 ), StateDir( 0 )}; }

    //  Retrieve the (sorted) set of LHCbIDs
    [[nodiscard]] std::vector<LHCbID> lhcbIDs() const {
      std::vector<LHCbID> ids;
      ids.reserve( TracksInfo::MaxVPHits );
      for ( auto i = 0; i < nHits().cast(); i++ ) {
        static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
        ids.emplace_back( LHCb::LHCbID( bit_cast<unsigned int, int>( lhcbID( i ).cast() ) ) );
      }
      return ids;
    }
  };
} // namespace LHCb::Pr::Velo
REGISTER_PROXY( LHCb::Pr::Velo::Tracks, LHCb::Pr::Velo::TrackProxy );
REGISTER_HEADER( LHCb::Pr::Velo::Tracks, "Event/PrVeloTracks.h" );
