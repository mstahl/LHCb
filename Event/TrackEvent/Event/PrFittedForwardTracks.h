/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/PrLongTracks.h"
#include "Event/PrProxyHelpers.h"
#include "Event/PrTracksTag.h"
#include "Event/Zip.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
#include "SOAExtensions/ZipUtils.h"

/**
 * Track data after the Kalman fit
 *
 * @author: Arthur Hennequin
 * 2020-08-17 updated to SOACollection by Peilian Li
 */

namespace V2 = LHCb::v2::Event;

namespace LHCb::Pr::Fitted::Forward {
  namespace detail {
    /** Helper type for fitted track proxies **/
    template <typename TrackProxy>
    struct FittedState {
      FittedState( TrackProxy proxy ) : m_proxy{std::move( proxy )} {}
      decltype( auto ) qOverP() const { return m_proxy.qOverP(); }
      decltype( auto ) momentum() const { return m_proxy.momentum(); }
      decltype( auto ) slopes() const { return m_proxy.closestToBeamStateDir(); }
      decltype( auto ) position() const { return m_proxy.closestToBeamStatePos(); }
      decltype( auto ) covariance() const { return m_proxy.closestToBeamStateCovariance(); }
      decltype( auto ) x() const { return position().X(); }
      decltype( auto ) y() const { return position().Y(); }
      decltype( auto ) z() const { return position().Z(); }
      decltype( auto ) tx() const { return slopes().X(); }
      decltype( auto ) ty() const { return slopes().Y(); }

    private:
      TrackProxy m_proxy;
    };
  } // namespace detail

  namespace Tag {
    struct trackFT : V2::int_field {};
    struct StateQoP : V2::float_field {};
    struct Chi2 : V2::float_field {};
    struct Chi2nDoF : V2::int_field {};

    struct StatePosition : V2::state_field {};
    inline constexpr std::size_t NumCovXY = 3;
    struct StateCovX : V2::floats_field<NumCovXY> {};
    struct StateCovY : V2::floats_field<NumCovXY> {};

    template <typename T>
    using fitfwd_t = V2::SOACollection<T, trackFT, StateQoP, Chi2, Chi2nDoF, StatePosition, StateCovX, StateCovY>;
  } // namespace Tag

  struct Tracks : Tag::fitfwd_t<Tracks> {
    using base_t = typename Tag::fitfwd_t<Tracks>;

    using base_t::allocator_type;
    Tracks( LHCb::Pr::Long::Tracks const* forward_ancestors,
            Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}, m_forward_ancestors{forward_ancestors} {}

    // Special constructor for zipping machinery
    Tracks( Zipping::ZipFamilyNumber zipIdentifier, Tracks const& other )
        : base_t{std::move( zipIdentifier ), other}, m_forward_ancestors{other.m_forward_ancestors} {}

    [[nodiscard]] LHCb::Pr::Long::Tracks const* getForwardAncestors() const noexcept { return m_forward_ancestors; };

    // Define an optional custom proxy for this track
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct FittedProxy : V2::Proxy<simd, behaviour, ContainerType> {
      using V2::Proxy<simd, behaviour, ContainerType>::Proxy;

      [[nodiscard]] auto qOverP() const { return this->template get<Tag::StateQoP>(); }
      [[nodiscard]] auto p() const { return abs( 1.f / qOverP() ); }
      [[nodiscard]] auto pt() const {
        auto const mom    = p();
        auto const s      = this->template get<Tag::StatePosition>();
        auto const tx2ty2 = s.tx() * s.tx() + s.ty() * s.ty();
        auto const pt2    = mom * mom * tx2ty2 / ( tx2ty2 + 1 );
        using std::sqrt;
        return sqrt( pt2 );
      }
    };
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = FittedProxy<simd, behaviour, ContainerType>;

  private:
    LHCb::Pr::Long::Tracks const* m_forward_ancestors{nullptr};

    DECLARE_PROXY_FRIEND( TrackProxy );
  };

  DECLARE_PROXY( TrackProxy ) {
    PROXY_METHODS( TrackProxy, dType, Tracks, m_Tracks );
    using IType = typename dType::int_v;
    using FType = typename dType::float_v;

  private:
    template <typename Tag, typename... Ts>
    auto loader( Ts... Is ) const { // TODO: move in PROXY_METHODS
      return this->load_vector( this->m_Tracks->template data<Tag>( Is... ) );
    }

  public:
    [[nodiscard]] auto trackFT() const { return loader<Tag::trackFT>(); }
    [[nodiscard]] auto qOverP() const { return loader<Tag::StateQoP>(); }
    [[nodiscard]] auto p() const { return abs( 1.0 / qOverP() ); }
    [[nodiscard]] auto chi2() const { return loader<Tag::Chi2>(); }
    [[nodiscard]] auto chi2nDoF() const { return loader<Tag::Chi2nDoF>(); }
    [[nodiscard]] auto nDoF() const { return chi2nDoF(); }
    [[nodiscard]] auto chi2PerDoF() const { return chi2(); }

    [[nodiscard]] auto StatePosElement( std::size_t i ) const { return loader<Tag::StatePosition>( i ); }
    [[nodiscard]] auto covX() const {
      return Vec3<FType>( loader<Tag::StateCovX>( 0 ), loader<Tag::StateCovX>( 1 ), loader<Tag::StateCovX>( 2 ) );
    }
    [[nodiscard]] auto covY() const {
      return Vec3<FType>( loader<Tag::StateCovY>( 0 ), loader<Tag::StateCovY>( 1 ), loader<Tag::StateCovY>( 2 ) );
    }
    auto charge() const { return select( FType{qOverP()} > 0.f, IType{+1}, IType{-1} ); }
    auto closestToBeamStateCovariance() const {
      // This is not zero-initialised
      LHCb::LinAlg::MatSym<FType, 5> cov;
      auto const                     qop = qOverP();
      cov( 0, 0 )                        = covX().x; // x error
      cov( 0, 2 )                        = covX().y; // x-tx covariance
      cov( 2, 2 )                        = covX().z; // tx error
      cov( 1, 1 )                        = covY().x; // y error
      cov( 1, 3 )                        = covY().y; // y-ty covariance
      cov( 3, 3 )                        = covY().z; // ty error
      cov( 0, 1 )                        = 0.f;      // x-y covariance
      cov( 0, 3 )                        = 0.f;      // x-ty covariance
      cov( 0, 4 )                        = 0.f;      // x-qop covariance
      cov( 1, 2 )                        = 0.f;      // y-tx covariance
      cov( 1, 4 )                        = 0.f;      // y-qop covariance
      cov( 2, 3 )                        = 0.f;      // tx-ty covariance
      cov( 2, 4 )                        = 0.f;      // tx-qop covariance
      cov( 3, 4 )                        = 0.f;      // ty-qop covariance
      // FIXME this hack should not be hardcoded, the factor does significantly
      // impact the p4cov of a particle that is fitted with this daughter.
      // ipchi2 of the mother is basically useless in that case!
      cov( 4, 4 ) = 0.6e-5f * qop * qop; // qop erro
      return cov;
    }

    auto closestToBeamStatePos() const {
      return Vec3<FType>( StatePosElement( 0 ), StatePosElement( 1 ), StatePosElement( 2 ) );
    }
    auto closestToBeamStateDir() const { return Vec3<FType>( StatePosElement( 3 ), StatePosElement( 4 ), 1.f ); }
    /** Return a proxy that exposes the a state-like API instead of the
     *  track-like API of this struct. The use of `clone<Proxy>()` discards the
     *  pointers to possible other members of the zip, which cannot be needed
     *  in the state proxy.
     */
    auto closestToBeamState() const { return detail::FittedState{full_proxy().template clone<Tracks>()}; }

    auto pt() const {
      auto const mom = p();
      auto const dir = closestToBeamStateDir();
      auto const pt2 = mom * mom * ( dir.X() * dir.X() + dir.Y() * dir.Y() ) / dir.mag2();
      using std::sqrt;
      return sqrt( pt2 );
    }
    auto pseudoRapidity() const { return closestToBeamStateDir().eta(); }
    auto phi() const { return closestToBeamStateDir().phi(); }
    auto threeMomentum() const {
      auto const dir = closestToBeamStateDir();
      using std::sqrt;
      auto const scale = p() / sqrt( dir.mag2() );
      return LHCb::LinAlg::Vec<FType, 3>{scale * dir.x, scale * dir.y, scale * dir.z};
    }
    // @brief Return a sorted vector of unique LHCbID objects on the t'th track.
    [[nodiscard]] std::vector<LHCbID> lhcbIDs() const {
      auto const* forward_tracks = this->m_Tracks->getForwardAncestors();
      if ( !forward_tracks ) { return std::vector<LHCbID>{}; }
      static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
      static_assert( behaviour() == LHCb::Pr::ProxyBehaviour::Contiguous );
      const int  forward_track_index    = trackFT().cast();
      auto const iterable_tracks_scalar = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( *forward_tracks );
      auto       lhcbids                = iterable_tracks_scalar[forward_track_index].lhcbIDs();

      // Sort and remove duplicates
      std::sort( lhcbids.begin(), lhcbids.end() );
      lhcbids.erase( std::unique( lhcbids.begin(), lhcbids.end() ), lhcbids.end() );

      return lhcbids;
    }

    auto nHits() const {
      auto const* forward = this->m_Tracks->getForwardAncestors();
      if ( UNLIKELY( !forward ) ) {
        throw GaudiException{"No long track ancestors!", "LHCb::Pr::Fitted::Forward::Tracks::nHits",
                             StatusCode::FAILURE};
      }
      static_assert( behaviour() == LHCb::Pr::ProxyBehaviour::Contiguous );
      if constexpr ( std::is_same_v<dType, SIMDWrapper::scalar::types> ) {
        auto iterable_tracks = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( *forward );
        return iterable_tracks[trackFT().cast()].nHits();
      } else {
        auto const indices_FT      = this->trackFT();
        auto const indices         = select( this->loop_mask(), indices_FT, indices_FT.hmin( this->loop_mask() ) );
        auto const iterable_tracks = LHCb::Pr::make_zip<LHCb__Pr__Proxy__simd>( *forward );
        auto const tracks          = iterable_tracks.gather( indices );
        return tracks.nHits();
      }
    }
  };
} // namespace LHCb::Pr::Fitted::Forward

REGISTER_PROXY( LHCb::Pr::Fitted::Forward::Tracks, LHCb::Pr::Fitted::Forward::TrackProxy );
REGISTER_HEADER( LHCb::Pr::Fitted::Forward::Tracks, "Event/PrFittedForwardTracks.h" );
