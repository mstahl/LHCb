/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/SOACollection.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "LHCbMath/Vec3.h"

namespace LHCb::v2::Event {
  /**
   * Tag and Proxy baseclasses for representing a 5 parameters state vector (x, y, z, tx, ty)
   */
  template <std::size_t... N>
  struct states_field : floats_field<N..., 5> {
    template <typename Tag, SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour,
              typename ContainerType, typename... Ts>
    struct NDStateProxy {
      using simd_t     = SIMDWrapper::type_map_t<Simd>;
      using type       = typename Tag::template vec_type<simd_t>;
      using OffsetType = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using proxy_type = NDNumericProxy<Tag, Simd, Behaviour, ContainerType, Ts..., std::size_t>;

      NDStateProxy( ContainerType* container, OffsetType offset, Ts... Is )
          : m_container( container ), m_offset( offset ), m_indices( Is... ) {}

      template <auto i>
      [[nodiscard]] constexpr auto proxy() const {
        return std::apply(
            [&]( auto&&... args ) {
              return proxy_type{m_container, m_offset, std::forward<decltype( args )>( args )..., i};
            },
            m_indices );
      }

      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto x() const { return proxy<0>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto y() const { return proxy<1>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto z() const { return proxy<2>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto tx() const { return proxy<3>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto ty() const { return proxy<4>().get(); }

      inline __attribute__( ( always_inline ) ) constexpr void setPosition( type const& x, type const& y,
                                                                            type const& z ) {
        proxy<0>().set( x );
        proxy<1>().set( y );
        proxy<2>().set( z );
      }

      inline __attribute__( ( always_inline ) ) constexpr void setDirection( type const& tx, type const& ty ) {
        proxy<3>().set( tx );
        proxy<4>().set( ty );
      }

      inline __attribute__( ( always_inline ) ) constexpr void set( type const& x, type const& y, type const& z,
                                                                    type const& tx, type const& ty ) {
        setPosition( x, y, z );
        setDirection( tx, ty );
      }

      constexpr auto get() const { return *this; }

    private:
      ContainerType*          m_container;
      OffsetType              m_offset;
      const std::tuple<Ts...> m_indices;
    };

    template <typename Tag, SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour,
              typename ContainerType, typename... Ts>
    using proxy_type = NDStateProxy<Tag, Simd, Behaviour, ContainerType, Ts...>;
  };
  using state_field = states_field<>;

  /**
   * Tag and Proxy baseclasses for representing a 5 X 5 state covariance matrix:
   *          | x  |  y | tx | ty | q/p|
   *         x|____|____|____|____|____|
   *         y|____|____|____|____|____|
   *        tx|____|____|____|____|____|
   *        ty|____|____|____|____|____|
   *       q/p|____|____|____|____|____|
   * The matrices are stored as a flattened vector length 15 vector
   * (as the matrix is symmetric) in the following form:
   * (x_x, x_y, x_tx, x_ty, x_q/p, y_y, y_tx, y_ty, y_q/p, tx_tx, tx_ty, tx_q/p, ty_ty, ty_q/p, q/p_q/p)
   *
   */
  template <std::size_t... N>
  struct state_covs_field : floats_field<N..., 15> {
    template <typename Tag, SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour,
              typename ContainerType, typename... Ts>
    struct StateCovProxy {
      using simd_t     = SIMDWrapper::type_map_t<Simd>;
      using type       = typename Tag::template vec_type<simd_t>;
      using OffsetType = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using proxy_type = NDNumericProxy<Tag, Simd, Behaviour, ContainerType, Ts..., std::size_t>;

      StateCovProxy( ContainerType* container, OffsetType offset, Ts... Is )
          : m_container( container ), m_offset( offset ), m_indices( Is... ) {}

      template <auto i>
      [[nodiscard]] constexpr auto proxy() const {
        return std::apply(
            [&]( auto&&... args ) {
              return proxy_type{m_container, m_offset, std::forward<decltype( args )>( args )..., i};
            },
            m_indices );
      }

      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto x_x() const { return proxy<0>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto x_y() const { return proxy<1>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto x_tx() const { return proxy<2>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto x_ty() const { return proxy<3>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto x_QoverP() const {
        return proxy<4>().get();
      }

      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto y_y() const { return proxy<5>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto y_tx() const { return proxy<6>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto y_ty() const { return proxy<7>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto y_QoverP() const {
        return proxy<8>().get();
      }

      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto tx_tx() const { return proxy<9>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto tx_ty() const { return proxy<10>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto tx_QoverP() const {
        return proxy<11>().get();
      }

      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto ty_ty() const { return proxy<12>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto ty_QoverP() const {
        return proxy<13>().get();
      }

      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto QoverP_QoverP() const {
        return proxy<14>().get();
      }

      inline __attribute__( ( always_inline ) ) constexpr void
      setXCovariance( type const& x_x, type const& x_y, type const& x_tx, type const& x_ty, type const& x_QoverP ) {
        proxy<0>().set( x_x );
        proxy<1>().set( x_y );
        proxy<2>().set( x_tx );
        proxy<3>().set( x_ty );
        proxy<4>().set( x_QoverP );
      }

      inline __attribute__( ( always_inline ) ) constexpr void
      setYCovariance( type const& y_y, type const& y_tx, type const& y_ty, type const& y_QoverP ) {
        proxy<5>().set( y_y );
        proxy<6>().set( y_tx );
        proxy<7>().set( y_ty );
        proxy<8>().set( y_QoverP );
      }

      inline __attribute__( ( always_inline ) ) constexpr void setTXCovariance( type const& tx_tx, type const& tx_ty,
                                                                                type const& tx_QoverP ) {
        proxy<9>().set( tx_tx );
        proxy<10>().set( tx_ty );
        proxy<11>().set( tx_QoverP );
      }

      inline __attribute__( ( always_inline ) ) constexpr void setTYCovariance( type const& ty_ty,
                                                                                type const& ty_QoverP ) {
        proxy<12>().set( ty_ty );
        proxy<13>().set( ty_QoverP );
      }

      inline __attribute__( ( always_inline ) ) constexpr void setQoverPCovariance( type const& QoverP_QoverP ) {
        proxy<14>().set( QoverP_QoverP );
      }
      inline __attribute__( ( always_inline ) ) constexpr void
      set( type const& x_x, type const& x_y, type const& x_tx, type const& x_ty, type const& x_QoverP, type const& y_y,
           type const& y_tx, type const& y_ty, type const& y_QoverP, type const& tx_tx, type const& tx_ty,
           type const& tx_QoverP, type const& ty_ty, type const& ty_QoverP, type const& QoverP_QoverP ) {
        setXCovariance( x_x, x_y, x_tx, x_ty, x_QoverP );
        setYCovariance( y_y, y_tx, y_ty, y_QoverP );
        setTXCovariance( tx_tx, tx_ty, tx_QoverP );
        setTYCovariance( ty_ty, ty_QoverP );
        setQoverPCovariance( QoverP_QoverP );
      }

      constexpr auto& get() const { return *this; }

    private:
      ContainerType*          m_container;
      OffsetType              m_offset;
      const std::tuple<Ts...> m_indices;
    };

    template <typename Tag, SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour,
              typename ContainerType, typename... Ts>
    using proxy_type = StateCovProxy<Tag, Simd, Behaviour, ContainerType, Ts...>;
  };
  using state_cov_field = state_covs_field<>;

  // * Tag and Proxy baseclasses for representing a 3 parameters vector (x, y, z)
  template <std::size_t... N>
  struct vec3s_field : floats_field<N..., 3> {
    template <typename Tag, SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour,
              typename ContainerType, typename... Ts>
    struct Vec3Proxy {
      using simd_t     = SIMDWrapper::type_map_t<Simd>;
      using type       = typename Tag::template vec_type<simd_t>;
      using OffsetType = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using proxy_type = NDNumericProxy<Tag, Simd, Behaviour, ContainerType, Ts..., std::size_t>;

      Vec3Proxy( ContainerType* container, OffsetType offset, Ts... Is )
          : m_container( container ), m_offset( offset ), m_indices( Is... ) {}

      template <auto i>
      [[nodiscard]] constexpr auto proxy() const {
        return std::apply(
            [&]( auto&&... args ) {
              return proxy_type{m_container, m_offset, std::forward<decltype( args )>( args )..., i};
            },
            m_indices );
      }

      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto x() const { return proxy<0>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto y() const { return proxy<1>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto z() const { return proxy<2>().get(); }
      [[nodiscard]] inline __attribute__( ( always_inline ) ) constexpr auto vec3() const {
        return Vec3<type>( x(), y(), z() );
      }

      inline __attribute__( ( always_inline ) ) constexpr void set( type const& x, type const& y, type const& z ) {
        proxy<0>().set( x );
        proxy<1>().set( y );
        proxy<2>().set( z );
      }
      inline __attribute__( ( always_inline ) ) constexpr void set( Vec3<type> const& vec ) {
        proxy<0>().set( vec.x );
        proxy<1>().set( vec.y );
        proxy<2>().set( vec.z );
      }
      constexpr auto get() const { return *this; }

    private:
      ContainerType*          m_container;
      OffsetType              m_offset;
      const std::tuple<Ts...> m_indices;
    };

    template <typename Tag, SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour,
              typename ContainerType, typename... Ts>
    using proxy_type = Vec3Proxy<Tag, Simd, Behaviour, ContainerType, Ts...>;
  };
  using vec3_field = vec3s_field<>;
} // namespace LHCb::v2::Event

/*
 * Define maximum numbers of hits and states for Pr::Tracks which are detector specific
 */
namespace LHCb::Pr::TracksInfo {

  // The total layers of each sub-detector
  enum class DetLayers { VPLayers = 26, FTLayers = 12, UTLayers = 4 };
  // The maximum number of hits for PrLongTracks, depends on the pattern recognition
  enum class MaxHits { VPHits = 26, FTHits = 15, UTHits = 8 };

  inline constexpr std::size_t NumStatePars    = 5; // statePosVec vector(5D) (x,y,z,dx,dy)
  inline constexpr std::size_t NumSeedStates   = 3;
  inline constexpr std::size_t NumLongStates   = 2;
  inline constexpr std::size_t NumVeloStates   = 2;
  inline constexpr std::size_t NumCovXY        = 3;
  inline constexpr std::size_t NumVeloStateCov = NumCovXY * NumVeloStates;
  inline constexpr std::size_t MaxVPHits       = 26;
  inline constexpr std::size_t MaxUTHits       = 8;
  inline constexpr std::size_t MaxFTHits       = 12;
} // namespace LHCb::Pr::TracksInfo
