/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/State.h"
#include "Event/Track_SOA.h"
#include "Event/Track_v1.h"
#include "Event/Track_v2.h"
#include "GaudiAlg/FunctionalDetails.h"

#include <stdexcept>
#include <tuple>
#include <type_traits>

namespace LHCb::v2::Event::conversion {
  // Conversion to v1::Track and v2::Track State locations
  constexpr State::Location to_aos_state_loc( StateLocation loc ) {
    switch ( loc ) {
    case ( StateLocation::ClosestToBeam ):
      return State::ClosestToBeam;
    case ( StateLocation::FirstMeasurement ):
      return State::FirstMeasurement;
    case ( StateLocation::LastMeasurement ):
      return State::LastMeasurement;
    case ( StateLocation::BegRich1 ):
      return State::BegRich1;
    case ( StateLocation::EndRich1 ):
      return State::EndRich1;
    case ( StateLocation::BegRich2 ):
      return State::BegRich2;
    case ( StateLocation::EndRich2 ):
      return State::EndRich2;
    case ( StateLocation::Unknown ):
      return State::LocationUnknown;
    default:
      throw std::invalid_argument( "invalid StateLocation" );
    }
  }

  // This is what will be used most often
  template <StateLocation L>
  inline constexpr auto to_aos_state_loc_v = to_aos_state_loc( L );

  // Conversion from v1/v2 Track types to PrFittedGeneric types
  template <class Enum>
  static TrackType to_soa_track_type( Enum t ) {
    switch ( t ) {
    case Enum::Velo:
      return TrackType::Velo;
    case Enum::Long:
      return TrackType::Long;
    case Enum::Upstream:
      return TrackType::Upstream;
    case Enum::Downstream:
      return TrackType::Downstream;
    case Enum::Ttrack:
      return TrackType::Ttrack;
    case Enum::Muon:
      return TrackType::Muon;
    case Enum::Calo:
      return TrackType::Calo;
    case Enum::UT:
      return TrackType::UT;
    default:
      return TrackType::Unknown;
    }
  }

  /// Check if an object is valid
  template <class Proxy>
  constexpr std::enable_if_t<!std::is_pointer_v<Proxy>, bool> ref_is_valid( Proxy const& ) {
    return true;
  }

  template <class Proxy>
  constexpr std::enable_if_t<std::is_pointer_v<Proxy>, bool> ref_is_valid( Proxy const& v ) {
    return ( v != nullptr );
  }

  /// Update the fit result
  template <class TrackProxy, class OldTrack>
  void update_fit_result( TrackProxy& outTrack, OldTrack const& track ) {
    outTrack.template field<Tag::Chi2>().set( track.chi2() );
    outTrack.template field<Tag::Chi2nDoF>().set( track.nDoF() );
  }

  /// Update a single state
  template <StateLocation L, class TrackProxy, class OldTrack>
  bool update_state( TrackProxy& outTrack, OldTrack const& t ) {

    auto rstate = t.stateAt( to_aos_state_loc_v<L> );

    if ( !ref_is_valid( rstate ) ) return false;

    auto state = Gaudi::Functional::details::deref( rstate );

    // positions, slopes, q/p
    outTrack.template field<Tag::State>( L ).set( state.x(), state.y(), state.z(), state.tx(), state.ty() );
    outTrack.template field<Tag::State_QoverP>( L ).set( state.qOverP() );

    // covariance
    auto const& cov = state.covariance();

    outTrack.template field<Tag::StateCov>( L ).set( cov[0][0], cov[0][1], cov[0][2], cov[0][3], cov[0][4], cov[1][1],
                                                     cov[1][2], cov[1][3], cov[1][4], cov[2][3], cov[2][3], cov[2][4],
                                                     cov[3][3], cov[3][4], cov[4][4] );
    return true;
  }

  namespace detail {
    /// Actual implementation of the "update_states" function
    template <class TrackProxy, class OldTrack, StateLocation... L>
    static bool update_states_impl( TrackProxy& outTrack, OldTrack const& t, state_collection<L...> ) {
      return ( update_state<L>( outTrack, t ) && ... );
    }
  } // namespace detail

  /// Update all the states of a track
  template <class TrackProxy, class OldTrack>
  bool update_states( TrackType type, TrackProxy& outTrack, OldTrack& t ) {

    switch ( type ) {
    case TrackType::Unknown:
      return detail::update_states_impl( outTrack, t, available_states_t<TrackType::Unknown>{} );
    case TrackType::Velo:
      return detail::update_states_impl( outTrack, t, available_states_t<TrackType::Velo>{} );
    case TrackType::Long:
      return detail::update_states_impl( outTrack, t, available_states_t<TrackType::Long>{} );
    case TrackType::Upstream:
      return detail::update_states_impl( outTrack, t, available_states_t<TrackType::Upstream>{} );
    case TrackType::Downstream:
      return detail::update_states_impl( outTrack, t, available_states_t<TrackType::Downstream>{} );
    case TrackType::Ttrack:
      return detail::update_states_impl( outTrack, t, available_states_t<TrackType::Ttrack>{} );
    case TrackType::Muon:
      return detail::update_states_impl( outTrack, t, available_states_t<TrackType::Muon>{} );
    case TrackType::Calo:
      return detail::update_states_impl( outTrack, t, available_states_t<TrackType::Calo>{} );
    case TrackType::UT:
      return detail::update_states_impl( outTrack, t, available_states_t<TrackType::UT>{} );
    }

    __builtin_unreachable();
  }

  /// Update the LHCb IDs
  template <class TrackProxy, class OldTrack>
  void update_lhcb_ids( TrackProxy& outTrack, OldTrack& track ) {

    int numVPLHCbIDs = 0;
    int numUTLHCbIDs = 0;
    int numFTLHCbIDs = 0;

    for ( auto& id : track.lhcbIDs() ) {

      if ( id.isVelo() ) {
        outTrack.template field<Tag::LHCbIDs>( numVPLHCbIDs ).set( id.lhcbID() );
        ++numVPLHCbIDs;
        continue;
      }

      // are filled
      if ( id.isUT() ) {
        outTrack.template field<Tag::LHCbIDs>( numVPLHCbIDs + numUTLHCbIDs ).set( id.lhcbID() );
        ++numUTLHCbIDs;
        continue;
      }

      // Same comment as above, but between UT and FT id's
      if ( id.isFT() ) {
        outTrack.template field<Tag::LHCbIDs>( numVPLHCbIDs + numUTLHCbIDs + numFTLHCbIDs ).set( id.lhcbID() );
        ++numFTLHCbIDs;
        continue;
      }
    }
    outTrack.template field<Tag::numVPLHCbIDs>().set( numVPLHCbIDs );
    outTrack.template field<Tag::numUTLHCbIDs>().set( numUTLHCbIDs );
    outTrack.template field<Tag::numFTLHCbIDs>().set( numFTLHCbIDs );
  }

  using Status = int;

  enum StatusEnum : Status { Success, DifferentType, InvalidStates };

  static constexpr Status StatusShift = 10; // Only valid as long as we have less than 10 Status items

  /// Extend the result adding an additional status
  constexpr Status extend_result( Status r, StatusEnum s ) { return ( r * StatusShift ) + s; }

  /// Parse a result getting any possible new status and returning the new combined result
  constexpr std::tuple<Status, Status> process_result( Status r ) { return {r / StatusShift, r % StatusShift}; }

  /** Fill the values of a PrFittedGenericTrack with those from an old track
   *
   * The old track can be of v1::Track or v2::Track type. The "type" argument
   * must correspond to the type to check (that of the "outTrack" container).
   */
  template <class TrackProxy, class OldTrack>
  Status convert_track( TrackType type, TrackProxy& outTrack, OldTrack const& rtrack ) {

    if ( !ref_is_valid( rtrack ) ) throw std::runtime_error( "Null track pointer detected" );

    auto const& track = Gaudi::Functional::details::deref( rtrack );

    Status status = Success;

    if ( to_soa_track_type( track.type() ) != type ) status = extend_result( status, DifferentType );

    auto sc = update_states( type, outTrack, track );
    if ( !sc ) status = extend_result( status, InvalidStates );

    update_fit_result( outTrack, track );
    update_lhcb_ids( outTrack, track );

    return status;
  }
} // namespace LHCb::v2::Event::conversion
