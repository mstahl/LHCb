/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/GenerateSOATracks.h"
#include "GaudiAlg/Transformer.h"

using namespace LHCb::v2::Event;
using base_t = Gaudi::Functional::Transformer<Tracks( EventContext const& )>;

struct ProduceSOATracks final : public base_t {
  ProduceSOATracks( std::string const& name, ISvcLocator* svcLoc ) : base_t( name, svcLoc, KeyValue{"Output", ""} ) {}

  Tracks operator()( EventContext const& evtCtx ) const override {
    return generate_tracks( m_nTracks, ( m_eventCount++ ).value(), LHCb::getMemResource( evtCtx ) );
  }

private:
  mutable Gaudi::Accumulators::Counter<> m_eventCount{this, "Event"};
  Gaudi::Property<std::size_t>           m_nTracks{this, "NumberToGenerate", 100, "Number of objects to generate"};
};

DECLARE_COMPONENT( ProduceSOATracks )
