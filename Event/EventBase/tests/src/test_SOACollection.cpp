
/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#undef NDEBUG
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestSOACollection
#include "Event/SOACollection.h"
#include "LHCbMath/SIMDWrapper.h"

#include <boost/test/unit_test.hpp>

#include <array>
#include <vector>

namespace V2 = LHCb::v2::Event;

struct Float : V2::float_field {};
struct Int : V2::int_field {};
struct Position2D : V2::floats_field<2> {};

struct Test1D : V2::SOACollection<Test1D, Float> {
  using SOACollection::data;
  auto float_( int at ) { return *( data<Float>() + at ); }
};

struct Test2D : V2::SOACollection<Test2D, Float, Int> {
  using SOACollection::data;
  auto float_( int at ) { return *( data<Float>() + at ); }
  auto int_( int at ) { return *( data<Int>() + at ); }
};

struct Test3D : V2::SOACollection<Test3D, Position2D, Int> {
  using SOACollection::data;
  auto posx_( int at ) { return *( data<Position2D>( 0 ) + at ); }
  auto posy_( int at ) { return *( data<Position2D>( 1 ) + at ); }
  auto int_( int at ) { return *( data<Int>() + at ); }
};

BOOST_AUTO_TEST_CASE( CanDefaultConstruct ) {
  auto test_constr = []( auto const& x ) {
    BOOST_CHECK_EQUAL( x.capacity(), 0 );
    BOOST_CHECK_EQUAL( x.size(), 0 );
  };
  test_constr( Test1D{} );
  test_constr( Test2D{} );
  test_constr( Test3D{} );
}

BOOST_AUTO_TEST_CASE( CanMoveConstruct ) {
  // simplest 1d
  auto test_move = []( auto x ) {
    using type = std::decay_t<decltype( x )>;
    auto xzip  = x.zipIdentifier();
    type y{std::move( x )};
    auto yzip = y.zipIdentifier();
    BOOST_CHECK_EQUAL( xzip, yzip );

    type z{};
    z = std::move( y );
    BOOST_CHECK_EQUAL( z.zipIdentifier(), yzip );
  };
  test_move( Test1D{} );
  test_move( Test2D{} );
  test_move( Test3D{} );
}

BOOST_AUTO_TEST_CASE( ChangeCapacity ) {
  Test1D x{};
  x.reserve( 10 );
  BOOST_CHECK_GE( x.capacity(), 10 );
  BOOST_CHECK_EQUAL( x.size(), 0 );
}

BOOST_AUTO_TEST_CASE( ChangeSize ) {
  Test1D x{};
  x.resize( 10 );
  BOOST_CHECK_GE( x.capacity(), 10 );
  BOOST_CHECK_EQUAL( x.size(), 10 );
}

BOOST_AUTO_TEST_CASE( CanScalarEmplaceBack ) {
  Test1D x{};
  for ( int i = 0; i < 2; i++ ) { x.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<Float>().set( 1.f ); }
  BOOST_CHECK_EQUAL( x.size(), 2 );
  // first row
  BOOST_CHECK_EQUAL( x.float_( 0 ), 1.f );
  // second row
  BOOST_CHECK_EQUAL( x.float_( 1 ), 1.f );

  Test2D y{};
  for ( int i = 0; i < 2; i++ ) {
    auto a = y.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    a.field<Float>().set( 1.f );
    a.field<Int>().set( 2 );
  }
  BOOST_CHECK_EQUAL( y.size(), 2 );
  // first row
  BOOST_CHECK_EQUAL( y.float_( 0 ), 1.f );
  BOOST_CHECK_EQUAL( y.int_( 0 ), 2 );
  // second row
  BOOST_CHECK_EQUAL( y.float_( 1 ), 1.f );
  BOOST_CHECK_EQUAL( y.int_( 1 ), 2 );

  Test3D z{};
  for ( int i = 0; i < 2; i++ ) {
    auto a = z.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    a.field<Position2D>( 0 ).set( 1.f );
    a.field<Position2D>( 1 ).set( 2.f );
    a.field<Int>().set( 3 );
  }
  BOOST_CHECK_EQUAL( z.size(), 2 );
  // first row
  BOOST_CHECK_EQUAL( z.posx_( 0 ), 1.f );
  BOOST_CHECK_EQUAL( z.posy_( 0 ), 2.f );
  BOOST_CHECK_EQUAL( z.int_( 0 ), 3 );
  // second row
  BOOST_CHECK_EQUAL( z.posx_( 1 ), 1.f );
  BOOST_CHECK_EQUAL( z.posy_( 1 ), 2.f );
  BOOST_CHECK_EQUAL( z.int_( 1 ), 3 );
}

BOOST_AUTO_TEST_CASE( CanVectorEmplaceBack ) {
  if ( SIMDWrapper::sse::types::size != 4 ) return;

  Test1D x{};
  // create float_v
  std::array<float, 4>      in_f{1, 2, 3, 4};
  SIMDWrapper::sse::float_v f_v{in_f.data()};
  // emplacing
  x.emplace_back<SIMDWrapper::InstructionSet::SSE>().field<Float>().set( f_v );
  x.compress_back<SIMDWrapper::InstructionSet::SSE>( f_v > 2 ).field<Float>().set( f_v );
  BOOST_CHECK_EQUAL( x.size(), 6 );

  std::vector<float> out_f_1D{x.data<Float>(), x.data<Float>() + 6};
  // test unmasked
  for ( auto i = 0; i < 4; ++i ) { BOOST_CHECK_EQUAL( in_f[i], out_f_1D[i] ); }
  // test masked
  BOOST_CHECK_EQUAL( in_f[2], out_f_1D[4] );
  BOOST_CHECK_EQUAL( in_f[3], out_f_1D[5] );

  Test2D                  y{};
  std::array<int, 4>      in_i{5, 6, 7, 8};
  SIMDWrapper::sse::int_v i_v{in_i.data()};
  {
    auto a = y.emplace_back<SIMDWrapper::InstructionSet::SSE>();
    a.field<Float>().set( f_v );
    a.field<Int>().set( i_v );
  }
  {
    auto a = y.compress_back<SIMDWrapper::InstructionSet::SSE>( f_v > 2 );
    a.field<Float>().set( f_v );
    a.field<Int>().set( i_v );
  }
  BOOST_CHECK_EQUAL( y.size(), 6 );

  std::vector<float> out_f_2D{y.data<Float>(), y.data<Float>() + 6};
  std::vector<int>   out_i_2D{y.data<Int>(), y.data<Int>() + 6};
  for ( auto i = 0; i < 4; ++i ) {
    BOOST_CHECK_EQUAL( in_f[i], out_f_2D[i] );
    BOOST_CHECK_EQUAL( in_i[i], out_i_2D[i] );
  }
  BOOST_CHECK_EQUAL( in_f[2], out_f_2D[4] );
  BOOST_CHECK_EQUAL( in_i[2], out_i_2D[4] );
  BOOST_CHECK_EQUAL( in_f[3], out_f_2D[5] );
  BOOST_CHECK_EQUAL( in_i[3], out_i_2D[5] );

  Test3D                    z{};
  std::array<float, 4>      in_f2{9, 10, 11, 12};
  SIMDWrapper::sse::float_v f2_v{in_f2.data()};
  {
    auto a = z.emplace_back<SIMDWrapper::InstructionSet::SSE>();
    a.field<Position2D>( 0 ).set( f_v );
    a.field<Position2D>( 1 ).set( f2_v );
    a.field<Int>().set( i_v );
  }
  {
    auto a = z.compress_back<SIMDWrapper::InstructionSet::SSE>( f2_v < 11 );
    a.field<Position2D>( 0 ).set( f_v );
    a.field<Position2D>( 1 ).set( f2_v );
    a.field<Int>().set( i_v );
  }
  std::vector<float> out_f_3D{z.data<Position2D>( 0 ), z.data<Position2D>( 0 ) + 6};
  std::vector<float> out_f2_3D{z.data<Position2D>( 1 ), z.data<Position2D>( 1 ) + 6};
  std::vector<int>   out_i_3D{z.data<Int>(), z.data<Int>() + 6};
  for ( auto i = 0; i < 4; ++i ) {
    BOOST_CHECK_EQUAL( in_f[i], out_f_3D[i] );
    BOOST_CHECK_EQUAL( in_f2[i], out_f2_3D[i] );
    BOOST_CHECK_EQUAL( in_i[i], out_i_3D[i] );
  }
  BOOST_CHECK_EQUAL( in_f[0], out_f_3D[4] );
  BOOST_CHECK_EQUAL( in_f2[0], out_f2_3D[4] );
  BOOST_CHECK_EQUAL( in_i[0], out_i_3D[4] );
  BOOST_CHECK_EQUAL( in_f[1], out_f_3D[5] );
  BOOST_CHECK_EQUAL( in_f2[1], out_f2_3D[5] );
  BOOST_CHECK_EQUAL( in_i[1], out_i_3D[5] );
}

BOOST_AUTO_TEST_CASE( CanCopyBack ) {
  // 1D
  Test1D x_from{};
  for ( int x = 1; x <= 5; x++ ) { x_from.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<Float>().set( x ); }

  // checking scalar copy back
  Test1D x_to_scalar{};
  x_to_scalar.copy_back<SIMDWrapper::scalar::types>( x_from, 1 );
  x_to_scalar.copy_back<SIMDWrapper::scalar::types>( x_from, 2 );

  BOOST_CHECK_EQUAL( x_to_scalar.float_( 0 ), x_from.float_( 1 ) );
  BOOST_CHECK_EQUAL( x_to_scalar.float_( 1 ), x_from.float_( 2 ) );

  if ( SIMDWrapper::sse::types::size == 4 ) {
    // vector copy back
    Test1D x_to_vector{};
    x_to_vector.copy_back<SIMDWrapper::sse::types>( x_from, 1 );

    for ( auto i = 0u; i < SIMDWrapper::sse::types::size; ++i ) {
      BOOST_CHECK_EQUAL( x_to_vector.float_( i ), x_from.float_( i + 1 ) );
    }

    // masked vector copy back
    Test1D x_to_masked{};
    x_to_masked.copy_back<SIMDWrapper::sse::types>( x_from, 1, SIMDWrapper::sse::float_v{x_from.data<Float>()} > 2 );
    // mask = {0,0,1,1}

    for ( int i = 0; i < 2; ++i ) { BOOST_CHECK_EQUAL( x_to_masked.float_( i ), x_from.float_( i + 3 ) ); }
  }

  // 2D
  Test2D y_from{};
  for ( int x = 1; x <= 5; x++ ) {
    auto a = y_from.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    a.field<Float>().set( x );
    a.field<Int>().set( x );
  }

  // checking scalar copy back
  Test2D y_to_scalar{};
  y_to_scalar.copy_back<SIMDWrapper::scalar::types>( y_from, 1 );
  y_to_scalar.copy_back<SIMDWrapper::scalar::types>( y_from, 2 );

  BOOST_CHECK_EQUAL( y_to_scalar.float_( 0 ), y_from.float_( 1 ) );
  BOOST_CHECK_EQUAL( y_to_scalar.float_( 1 ), y_from.float_( 2 ) );
  BOOST_CHECK_EQUAL( y_to_scalar.int_( 0 ), y_from.int_( 1 ) );
  BOOST_CHECK_EQUAL( y_to_scalar.int_( 1 ), y_from.int_( 2 ) );

  if ( SIMDWrapper::sse::types::size == 4 ) {
    // vector copy back
    Test2D y_to_vector{};
    y_to_vector.copy_back<SIMDWrapper::sse::types>( y_from, 1 );

    for ( auto i = 0u; i < SIMDWrapper::sse::types::size; ++i ) {
      BOOST_CHECK_EQUAL( y_to_vector.float_( i ), y_from.float_( i + 1 ) );
      BOOST_CHECK_EQUAL( y_to_vector.int_( i ), y_from.int_( i + 1 ) );
    }

    // masked vector copy back
    Test2D y_to_masked{};
    y_to_masked.copy_back<SIMDWrapper::sse::types>( y_from, 1, SIMDWrapper::sse::float_v{y_from.data<Float>()} > 2 );
    // mask = {0,0,1,1}

    for ( int i = 0; i < 2; ++i ) {
      BOOST_CHECK_EQUAL( y_to_masked.float_( i ), y_from.float_( i + 3 ) );
      BOOST_CHECK_EQUAL( y_to_masked.int_( i ), y_from.int_( i + 3 ) );
    }
  }

  // 3D
  Test3D z_from{};
  for ( int x = 1; x <= 5; x++ ) {
    auto a = z_from.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    a.field<Position2D>( 0 ).set( x );
    a.field<Position2D>( 1 ).set( x );
    a.field<Int>().set( x );
  }

  // checking scalar copy back
  Test3D z_to_scalar{};
  z_to_scalar.copy_back<SIMDWrapper::scalar::types>( z_from, 1 );
  z_to_scalar.copy_back<SIMDWrapper::scalar::types>( z_from, 2 );

  BOOST_CHECK_EQUAL( z_to_scalar.posx_( 0 ), z_from.posx_( 1 ) );
  BOOST_CHECK_EQUAL( z_to_scalar.posx_( 1 ), z_from.posx_( 2 ) );
  BOOST_CHECK_EQUAL( z_to_scalar.posy_( 0 ), z_from.posy_( 1 ) );
  BOOST_CHECK_EQUAL( z_to_scalar.posy_( 1 ), z_from.posy_( 2 ) );
  BOOST_CHECK_EQUAL( z_to_scalar.int_( 0 ), z_from.int_( 1 ) );
  BOOST_CHECK_EQUAL( z_to_scalar.int_( 1 ), z_from.int_( 2 ) );

  if ( SIMDWrapper::sse::types::size == 4 ) {
    // vector copy back
    Test3D z_to_vector{};
    z_to_vector.copy_back<SIMDWrapper::sse::types>( z_from, 1 );

    for ( auto i = 0u; i < SIMDWrapper::sse::types::size; ++i ) {
      BOOST_CHECK_EQUAL( z_to_vector.posx_( i ), z_from.posx_( i + 1 ) );
      BOOST_CHECK_EQUAL( z_to_vector.posy_( i ), z_from.posy_( i + 1 ) );
      BOOST_CHECK_EQUAL( z_to_vector.int_( i ), z_from.int_( i + 1 ) );
    }

    // masked vector copy back
    Test3D z_to_masked{};
    z_to_masked.copy_back<SIMDWrapper::sse::types>( z_from, 1,
                                                    SIMDWrapper::sse::float_v{z_from.data<Position2D>( 0 )} > 2 );
    // mask = {0,0,1,1}

    for ( int i = 0; i < 2; ++i ) {
      BOOST_CHECK_EQUAL( z_to_masked.posx_( i ), z_from.posx_( i + 3 ) );
      BOOST_CHECK_EQUAL( z_to_masked.posy_( i ), z_from.posy_( i + 3 ) );
      BOOST_CHECK_EQUAL( z_to_masked.int_( i ), z_from.int_( i + 3 ) );
    }
  }
}

struct Double : public V2::array_field<> {
  using type = double;
};

struct Test : public V2::SOACollection<Test, Int, Double> {
  auto& dbl( std::size_t i ) { return *std::next( data<Double>(), i ); }
  auto& integer( std::size_t i ) { return *std::next( data<Int>(), i ); }
};

BOOST_AUTO_TEST_CASE( test_soacollection ) {
  Test test{};
  test.resize( 1 );
  BOOST_CHECK_EQUAL( test.size(), 1 );
  test.dbl( 0 )     = 42.0;
  test.integer( 0 ) = 42;
  test.resize( test.capacity() + 1 );
  BOOST_CHECK_EQUAL( test.dbl( 0 ), 42.0 );
  BOOST_CHECK_EQUAL( test.integer( 0 ), 42 );
  Test new_test{};
  BOOST_CHECK_EQUAL( new_test.size(), 0 );
  using simd_t = SIMDWrapper::best::types;
  new_test.copy_back<simd_t>( test, 0, simd_t::loop_mask( 0, 1 ) );
  BOOST_CHECK_EQUAL( new_test.size(), 1 );
  BOOST_CHECK_EQUAL( new_test.dbl( 0 ), 42.0 );
  BOOST_CHECK_EQUAL( new_test.integer( 0 ), 42 );
}

BOOST_AUTO_TEST_CASE( test_unmasked_copy ) {
  using simd_t = SIMDWrapper::best::types;
  Test source{}, dest{};
  source.resize( simd_t::size );
  for ( auto i = 0ul; i < simd_t::size; ++i ) {
    source.dbl( i )     = 13.0 * i;
    source.integer( i ) = i % 3;
  }
  dest.copy_back<simd_t>( source, 0 );
  BOOST_CHECK_EQUAL( dest.size(), simd_t::size );
  for ( auto i = 0ul; i < simd_t::size; ++i ) {
    BOOST_CHECK_EQUAL( dest.dbl( i ), 13.0 * i );
    BOOST_CHECK_EQUAL( dest.integer( i ), i % 3 );
  }
}

struct Weird1 : public V2::array_field<> {
  using type = struct alignas( 16 ) { std::array<short, 9> data; };
  static_assert( alignof( type ) == 16 );
  static_assert( sizeof( type ) == 32 );
};

struct Weird2 : public V2::array_field<> {
  using type = std::array<char, 7>;
  static_assert( alignof( type ) == 1 );
  static_assert( sizeof( type ) == 7 );
};

struct WeirdTest : public V2::SOACollection<WeirdTest, Weird1, Weird2> {
  auto& nineshorts( std::size_t i ) { return std::next( data<Weird1>(), i )->data; }
  auto& sevenchars( std::size_t i ) { return *std::next( data<Weird2>(), i ); }
};

BOOST_AUTO_TEST_CASE( test_weird_container ) {
  WeirdTest test{};
  test.resize( 1 );
  constexpr std::array           char_data{'B', 'a', 'z', 'i', 'n', 'g', 'a'};
  constexpr std::array<short, 9> short_data{1, 1, 2, 3, 5, 8, 13, 21, 34};
  test.nineshorts( 0 ) = short_data;
  test.sevenchars( 0 ) = char_data;
  test.resize( test.capacity() + 1 );
  // BOOST_CHECK_EQUAL requires output operators for std::array, and it seems
  // difficult to help Boost to find those...
  BOOST_CHECK( test.sevenchars( 0 ) == char_data );
  BOOST_CHECK( test.nineshorts( 0 ) == short_data );
}

struct Weird3 : public V2::array_field<> {
  struct alignas( std::max_align_t ) type {
    unsigned char data;
  };
  static_assert( sizeof( type ) == alignof( std::max_align_t ) );
  static_assert( alignof( type ) == alignof( std::max_align_t ) );
};

struct WeirdTest2 : public V2::SOACollection<WeirdTest2, Weird3> {
  auto& overaligned_byte( std::size_t i ) { return std::next( data<Weird3>(), i )->data; }
};

BOOST_AUTO_TEST_CASE( test_weird_container_2 ) {
  WeirdTest2 test{};
  test.resize( 1 );
  test.overaligned_byte( 0 ) = 42;
  test.resize( test.capacity() + 37 );
  BOOST_CHECK_EQUAL( test.overaligned_byte( 0 ), 42 );
}