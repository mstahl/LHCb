/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/STLExtensions.h"
#include "SOAExtensions/ZipUtils.h"
#include "SOAUtils.h"

#include <boost/align/is_aligned.hpp>
#include <boost/integer/common_factor_rt.hpp>
#include <boost/mp11/set.hpp>

#include <tuple>

#include "Proxy.h"
#include "SOAZip.h"

// For example and documentation, see: Event/TrackEvent/Event/PrSeedTracks.h

namespace LHCb::v2::Event {
  template <std::size_t... Ns>
  struct array_field {
    template <typename T>
    [[nodiscard]] static constexpr std::size_t num_columns( T const& ) {
      return ( 1 * ... * Ns );
    }
    template <typename T, typename... Ts>
    [[nodiscard]] static constexpr std::size_t flat_index( T const&, Ts... Is ) {
      assert( ( ... && ( static_cast<std::size_t>( Is ) < Ns ) ) );
      std::size_t idx{0};
      ( ( idx = idx * Ns + Is ), ... );
      return idx;
    }
    template <typename Tag, SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour,
              typename ContainerType, typename... Ts>
    using proxy_type = NDNumericProxy<Tag, Simd, Behaviour, ContainerType, Ts...>;
  };
  template <std::size_t... N>
  struct ints_field : array_field<N...> {
    using type = int;
    template <typename simd_t>
    using vec_type = SIMDWrapper::vector_type_map_t<type, simd_t>;
  };
  template <std::size_t... N>
  struct floats_field : array_field<N...> {
    using type = float;
    template <typename simd_t>
    using vec_type = SIMDWrapper::vector_type_map_t<type, simd_t>;
  };

  using int_field   = ints_field<>;
  using float_field = floats_field<>;

  namespace detail {
    template <typename, typename, typename = void>
    struct has_vec_type : std::false_type {};

    template <typename T, typename simd_t>
    struct has_vec_type<T, simd_t, std::void_t<typename T::template vec_type<simd_t>>> : std::true_type {};

    template <typename T, typename simd_t>
    inline constexpr bool has_vec_type_v = has_vec_type<T, simd_t>::value;
  } // namespace detail

  /** This is a utility that functions a bit like a struct full of std::vector
   *  members. The key differences are that:
   *  - all vectors are served by the same memory allocation
   *  - there is only one size -- all columns are the same length
   *  This is designed to be a useful tool for LHCb::Pr::make_zip()-compatible
   *  event model classes. It was designed with LHCb::v2::Composites in mind.
   *  SOACollection is a CRTP base class, i.e. the intended use is that you
   *  define e.g.
   *    struct Composites : public SOACollection<Composites, Tag1, Tag2, ...> {
   *      auto const* column1data() const { return data<Tag1>(); }
   *    };
   *  It is possible for a Tag to represent a runtime-variable (but
   *  class-instance-constant) number of columns.
   */
  template <typename Derived, typename... Tags>
  struct SOACollection {
    /** Useful alias below.
     */
    using TagsTuple = std::tuple<Tags...>;

    /** Utility to check if this collection contains Tag
     */
    template <typename Tag>
    using hasTag = boost::mp11::mp_set_contains<TagsTuple, Tag>;

  private:
    /** The implementation assumes that tag types are unique. Assert that.
     */
    static_assert( boost::mp11::mp_is_set<TagsTuple>::value, "Tag types must be unique" );

    /** The implementation does not call any constructors, destructors or move
     *  operations. This means it is unsafe to use non-trivial column types.
     */
    static_assert( ( std::is_trivially_default_constructible_v<typename Tags::type> && ... ) );

    /** Widest SIMD vector backend that is supported, in bytes.
     *  SOACollection guarantees that it is valid to read/write a vector this
     *  wide starting from any valid container entry, though the contained
     *  values are undefined if they lie outside the validity of the container.
     */
    constexpr static std::size_t max_vector_width = constants::max_vector_width;

    /** Smallest column type. This is useful when figuring out how to enforce
     *  the max_vector_width guarantee.
     */
    constexpr static std::size_t min_sizeof = std::min( {sizeof( typename Tags::type )...} );

    /** Maximum alignment of any of our column types.
     */
    constexpr static std::size_t max_alignment = std::max( {alignof( typename Tags::type )...} );
    static_assert( max_alignment > 0, "max_alignment was 0" );
    static_assert( ( max_alignment & ( max_alignment - 1 ) ) == 0, "max_alignment not a power of 2" );
    static_assert( max_alignment <= alignof( std::max_align_t ),
                   "max_alignment indicated extended alignment and it is implementation defined whether "
                   "std::aligned_storage_t supports that" );

    /** Get the greatest common divisor of all the sizeofs and max_alignment.
     *  std::gcd exists, but it only has a two-parameter overload.
     *  It's more succinct to just use Boost.
     */
    constexpr static std::size_t sizeofs_gcd = boost::integer::gcd( max_alignment, sizeof( typename Tags::type )... );
    static_assert( sizeofs_gcd );
    static_assert( max_alignment % sizeofs_gcd == 0 );
    static_assert( ( ( sizeof( typename Tags::type ) % sizeofs_gcd == 0 ) && ... ) );

    /** All offsets to the starts of columns are of the form
     *    x * sizeof_gcd * capacity
     *  And as a not-too-sophisticated way of ensuring that these addresses are
     *  all aligned by max_alignment, we round up 'capacity' by some factor.
     *
     *  This also has the effect of ensuring that the size of the complete
     *  buffer is a multiple of max_alignment:
     *    total_size = sum_of_sizeofs * capacity [in bytes]
     *  is proportional to
     *    sizeofs_gcd * capacity_rounding_factor
     *  which is just max_alignment.
     */
    constexpr static std::size_t capacity_rounding_factor = max_alignment / sizeofs_gcd;
    static_assert( capacity_rounding_factor, "capacity_rounding_factor was 0" );
    // It wouldn't actually matter if it wasn't a power of 2, but I don't think
    // that can happen...
    static_assert( ( capacity_rounding_factor & ( capacity_rounding_factor - 1 ) ) == 0,
                   "capacity_rounding_factor not a power of 2" );

    /** The number of extra entries that we must guarantee are valid after the
     *  notional last ("capacity() - 1") element. A conservative choice is to
     *  make sure that `( required_padding_entries + 1 ) * min_sizeof` is >=
     *  max_vector_width.
     */
    constexpr static std::size_t required_padding_entries =
        ( max_vector_width / min_sizeof ) + bool{max_vector_width % min_sizeof} - 1;
    static_assert( ( required_padding_entries + 1 ) * min_sizeof >= max_vector_width );

    /** Type with sizeof == alignof == max_alignment that is used to interact
     *  with the allocator.
     */
    using block_t = std::aligned_storage_t<max_alignment, max_alignment>;
    static_assert( sizeof( block_t ) == max_alignment );
    static_assert( alignof( block_t ) == max_alignment );

  public:
    using allocator_type = LHCb::Allocators::EventLocal<block_t>;
    SOACollection( Zipping::ZipFamilyNumber zip_identifier = Zipping::generateZipIdentifier(),
                   allocator_type           alloc          = {} )
        : m_alloc{std::move( alloc )}, m_zip_identifier{std::move( zip_identifier )} {}
    SOACollection( Zipping::ZipFamilyNumber zn, SOACollection const& old )
        : SOACollection( std::move( zn ), old.get_allocator() ) {}

    // Could define SOACollection( SOACollection& old ) : SOACollection() { swap( *this, old ); }
    // if we had a default constructor that didn't generate new zip identifiers
    SOACollection( SOACollection&& old ) noexcept
        : m_alloc{std::move( old.m_alloc )}
        , m_data{std::exchange( old.m_data, nullptr )}
        , m_size{std::exchange( old.m_size, 0 )}
        , m_capacity{std::exchange( old.m_capacity, required_padding_entries )}
        , m_zip_identifier{std::move( old.m_zip_identifier )} {
      assert( m_capacity >= required_padding_entries );
    }
    ~SOACollection() {
      // Note that we can only ask the allocator to deallocate a pointer it
      // allocated, which excludes the nullptr default value of m_data.
      if ( m_data ) {
        // The product of any valid (rounded) capacity with any sizeof is
        // guaranteed to be a multiple of max_alignment == sizeof( block_t )
        assert( ( m_capacity * sum_of_sizeofs() ) % sizeof( block_t ) == 0 );
        std::allocator_traits<allocator_type>::deallocate( m_alloc, m_data,
                                                           ( m_capacity * sum_of_sizeofs() ) / sizeof( block_t ) );
      }
    }
    friend void swap( SOACollection& a, SOACollection& b ) noexcept {
      using std::swap;
      swap( a.m_alloc, b.m_alloc );
      swap( a.m_data, b.m_data );
      swap( a.m_size, b.m_size );
      swap( a.m_capacity, b.m_capacity );
      swap( a.m_zip_identifier, b.m_zip_identifier );
    }
    // If we want to enable copy-assignment then enable a copy constructor
    SOACollection& operator=( SOACollection old ) {
      swap( *this, old );
      return *this;
    }
    SOACollection( SOACollection const& ) = delete;

    inline __attribute__( ( always_inline ) ) void resize( std::size_t size ) {
      reserve_exponential( size );
      m_size = size;
    }

    void reserve( std::size_t required_capacity ) {
      if ( required_capacity > capacity() ) { reallocate( safe_capacity( required_capacity ) ); }
    }

    void                      clear() { resize( 0 ); }
    [[nodiscard]] bool        empty() const { return !m_size; }
    [[nodiscard]] std::size_t size() const { return m_size; }
    [[nodiscard]] std::size_t capacity() const {
      assert( m_capacity >= required_padding_entries );
      return m_capacity - required_padding_entries;
    }
    [[nodiscard]] allocator_type           get_allocator() const { return m_alloc; }
    [[nodiscard]] Zipping::ZipFamilyNumber zipIdentifier() const { return m_zip_identifier; }

    template <typename simd_t>
    inline __attribute__( ( always_inline ) ) void copy_back( SOACollection const& from, int at ) {
      // Copy simd_t::size elements from 'from' into ourself
      // First, make sure we have enough capacity
      reserve_exponential( m_size + simd_t::size );
      ( copy_back_helper<Tags, simd_t>( from, at, std::true_type{} ), ... );
      m_size += simd_t::size;
    }

    template <typename simd_t>
    inline __attribute__( ( always_inline ) ) void copy_back( SOACollection const& from, int at,
                                                              typename simd_t::mask_v const& mask ) {
      // Copy elements from 'from' into ourself according to 'mask'
      // First, make sure we have enough capacity
      auto const count = popcount( mask );
      if ( !count ) {
        // warning if you think about removing this: if m_size == count == 0
        // then this would lead to a segfault
        return;
      }
      reserve_exponential( m_size + count );
      ( copy_back_helper<Tags, simd_t>( from, at, mask ), ... );
      m_size += count;
    }

    template <typename Tag, typename F, typename M = std::true_type>
    void store( int at, F const& value, M&& mask = {} ) {
      auto store = [&]( auto* storage, auto const& val ) {
        if constexpr ( std::is_same_v<std::true_type, std::decay_t<M>> ) {
          val.store( storage );
        } else {
          val.compressstore( mask, storage );
        }
      };
      store( std::next( this->data<Tag>(), at ), value );
    }
    template <typename Tag, typename I, typename M = std::true_type>
    void store( int at, int i, I const& idx, M&& mask = {} ) {
      auto store = [&]( auto* storage, auto const& val ) {
        if constexpr ( std::is_same_v<std::true_type, std::decay_t<M>> ) {
          val.store( storage );
        } else {
          val.compressstore( mask, storage );
        }
      };
      store( std::next( this->data<Tag>( i ), at ), idx );
    }
    template <typename Tag, typename I, typename M = std::true_type>
    void store( int at, int i, int j, I const& idx, M&& mask = {} ) {
      auto store = [&]( auto* storage, auto const& val ) {
        if constexpr ( std::is_same_v<std::true_type, std::decay_t<M>> ) {
          val.store( storage );
        } else {
          val.compressstore( mask, storage );
        }
      };
      store( std::next( this->data<Tag>( i, j ), at ), idx );
    }

  private:
    /** Helper for methods that want to trigger exponential capacity growth if
     *  more capacity is needed (copy_back, resize)
     */
    inline __attribute__( ( always_inline ) ) void reserve_exponential( std::size_t required_capacity ) {
      auto const usable_capacity = capacity();
      if ( required_capacity > usable_capacity ) {
        reallocate( safe_capacity( std::max( required_capacity, 2 * usable_capacity ) ) );
      }
    }

    /** Helper for reserve methods that assumes its argument is nonzero, has
     *  been rounded up by safe_capacity(), and is large enough that we should
     *  really reallocate.
     */
    void reallocate( std::size_t capacity ) {
      // Allocate the new storage
      auto const bytes_per_row = sum_of_sizeofs();
      // The product of any valid (rounded) capacity with any sizeof is
      // guaranteed to be a multiple of max_alignment == sizeof( block_t )
      assert( ( capacity * bytes_per_row ) % sizeof( block_t ) == 0 );
      auto new_data =
          std::allocator_traits<allocator_type>::allocate( m_alloc, ( capacity * bytes_per_row ) / sizeof( block_t ) );
      assert( boost::alignment::is_aligned( new_data, max_alignment ) );
      // Copy m_size entries from the old storage to the new storage and then
      // free the old storage, if old storage was allocated.
      if ( m_data ) {
        ( copy_impl<Tags>( m_size, m_data, m_capacity, new_data, capacity ), ... );
        // Note that we can only ask the allocator to deallocate a pointer it
        // allocated, which excludes the nullptr default value of m_data.

        // The product of any valid (rounded) capacity with any sizeof is
        // guaranteed to be a multiple of max_alignment == sizeof( block_t )
        assert( ( m_capacity * bytes_per_row ) % sizeof( block_t ) == 0 );
        std::allocator_traits<allocator_type>::deallocate( m_alloc, m_data,
                                                           ( m_capacity * bytes_per_row ) / sizeof( block_t ) );
      }
      // Note: do *not* want to update m_size
      m_data     = new_data;
      m_capacity = capacity;
    }

    /** Helper to calculate what actual capacity we need to have allocated in
     *  order for all operations to be safe if the container is used as if it
     *  has the requested capacity. To expand a bit, the idea is that if you do
     *    reserve( n ); // might reallocate
     *    resize( n );  // will not reallocate
     *  then loading a SIMD vector starting from the last (n-1) element should
     *  be safe, even though for a 512bit register and 32bit floats then 15 out
     *  of 16 loaded elements are "past the end".
     *
     *  Note that this also guarantees that compressstore operations that
     *  permute and then write a full vector unit width (avx2) will not clobber
     *  anything that they shouldn't.
     */
    [[nodiscard]] std::size_t static safe_capacity( std::size_t capacity ) {
      capacity += required_padding_entries;
      // Finally, make sure all columns have at least max_alignment alignment.
      // See also: comment of the 'capacity_rounding_factor' static variable.
      // Round up to a multiple of 'capacity_rounding_factor':
      capacity += capacity_rounding_factor - 1;
      return capacity - capacity % capacity_rounding_factor;
    }

    template <typename Tag, typename simd_t, typename mask_t>
    void copy_back_helper( SOACollection const& from, int at, mask_t const& mask ) {
      auto const num_columns_this = Tag::num_columns( static_cast<Derived const&>( *this ) );
      auto const num_columns_from = Tag::num_columns( static_cast<Derived const&>( from ) );
      if ( UNLIKELY( num_columns_this != num_columns_from ) ) {
        throw GaudiException{"logic error", "SOACollection", StatusCode::FAILURE};
      }
      for ( auto n = 0ul; n < num_columns_this; ++n ) {
        auto const* from_data_ptr   = std::next( from.data_impl<Tag>( from.m_data, from.m_capacity, n ), at );
        auto*       this_target_ptr = std::next( data_impl<Tag>( m_data, m_capacity, n ), m_size );
        // Copy simd_t::size entries from `from_data_ptr` to `this_target_ptr`,
        // respecting `mask`. If a relevant SIMD type is available then we can
        // implement this using that. At the time of writing, SIMDWrapper
        // provides SIMD counterparts for `int` and `float` column only. For
        // other types we can just write a scalar loop (for a masked copy) or
        // use std::copy (for an unmasked copy).
        if constexpr ( detail::has_vec_type_v<Tag, simd_t> ) {
          // Use a SIMD type
          typename Tag::template vec_type<simd_t> from_data{from_data_ptr};
          if constexpr ( std::is_same_v<mask_t, std::true_type> ) {
            // No mask
            from_data.store( this_target_ptr );
          } else {
            // With mask
            from_data.compressstore( mask, this_target_ptr );
          }
        } else {
          if constexpr ( std::is_same_v<mask_t, std::true_type> ) {
            // No mask, just copy simd_t::size entries
            std::copy( from_data_ptr, std::next( from_data_ptr, simd_t::size ), this_target_ptr );
          } else {
            // There's a mask, respect it
            // Generate a scalar loop
            for ( auto i = 0ul; i < simd_t::size; ++i, ++from_data_ptr ) {
              if ( !testbit( mask, i ) ) { continue; }
              *this_target_ptr = *from_data_ptr;
              ++this_target_ptr;
            }
          }
        }
      }
    }

  public:
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = Proxy<simd, behaviour, ContainerType>;

    // Add simd_t::size entries to the container and return a proxy object that can be used to write to them
    template <SIMDWrapper::InstructionSet simd = SIMDWrapper::InstructionSet::Best>
    inline __attribute__( ( always_inline ) ) auto emplace_back() {
      using simd_t        = SIMDWrapper::type_map_t<simd>;
      auto const old_size = size();
      resize( old_size + simd_t::size );
      return proxy_type<resolve_instruction_set_v<simd>, LHCb::Pr::ProxyBehaviour::Contiguous, Derived>(
          static_cast<Derived*>( this ), old_size );
    }

    // Add simd_t::size entries to the container and return a proxy object that can be used to write to them
    // only if it fits under max_size, otherwise ignore the last entries.
    // Can be used as a cheaper but safe alternative to compress_back if the mask is only a loop_mask
    template <SIMDWrapper::InstructionSet simd = SIMDWrapper::InstructionSet::Best>
    inline __attribute__( ( always_inline ) ) auto emplace_back( std::size_t max_size ) {
      using simd_t        = SIMDWrapper::type_map_t<simd>;
      auto const old_size = size();
      resize( std::min( old_size + simd_t::size, max_size ) );
      return proxy_type<resolve_instruction_set_v<simd>, LHCb::Pr::ProxyBehaviour::Contiguous, Derived>(
          static_cast<Derived*>( this ), old_size );
    }

    template <SIMDWrapper::InstructionSet simd = SIMDWrapper::InstructionSet::Best>
    inline __attribute__( ( always_inline ) ) auto
    compress_back( typename SIMDWrapper::type_map_t<simd>::mask_v const& mask ) {
      auto const old_size = size();
      resize( old_size + popcount( mask ) );
      return proxy_type<resolve_instruction_set_v<simd>, LHCb::Pr::ProxyBehaviour::Compress, Derived>(
          static_cast<Derived*>( this ), {old_size, mask} );
    }

    LHCb::v2::Event::Zip<SIMDWrapper::InstructionSet::Scalar, Derived> scalar() {
      return {static_cast<Derived*>( this )};
    }

    LHCb::v2::Event::Zip<SIMDWrapper::InstructionSet::Scalar, const Derived> scalar() const {
      return {static_cast<const Derived*>( this )};
    }

    template <SIMDWrapper::InstructionSet InstSet = SIMDWrapper::InstructionSet::Best>
    LHCb::v2::Event::Zip<InstSet, Derived> simd() {
      return {static_cast<Derived*>( this )};
    }

    template <SIMDWrapper::InstructionSet InstSet = SIMDWrapper::InstructionSet::Best>
    LHCb::v2::Event::Zip<InstSet, const Derived> simd() const {
      return {static_cast<const Derived*>( this )};
    }

    /** e.g. float* to first element of column Tag
     */
    template <typename Tag, typename... Ts>
    [[nodiscard]] constexpr typename Tag::type* data( Ts... Is ) {
      return data_impl<Tag>( m_data, m_capacity, Tag::flat_index( static_cast<Derived const&>( *this ), Is... ) );
    }
    template <typename Tag, typename... Ts>
    [[nodiscard]] constexpr typename Tag::type const* data( Ts... Is ) const {
      return data_impl<Tag>( m_data, m_capacity, Tag::flat_index( static_cast<Derived const&>( *this ), Is... ) );
    }

  private:
    /** Calculate the sum of sizeofs of all of the types up to a certain point.
     *  Note that this may not be a compile-time constant if, for example, the
     *  number of copies of a field depends on a runtime value. An example of
     *  this is particle child relations, which consist of 2 * num_children
     *  fields with the num_children value stored as a member of 'Derived'.
     *  The return value of this function is used to convert capacities to
     *  numbers of bytes. It is assumed to be constant for the lifetime of a
     *  given Derived object.
     */
    template <std::size_t... Ns>
    [[nodiscard]] constexpr std::size_t sum_of_sizeofs( std::index_sequence<Ns...> ) const {
      auto const& derived = static_cast<Derived const&>( *this );
      return ( ( sizeof( typename std::tuple_element_t<Ns, TagsTuple>::type ) *
                 std::tuple_element_t<Ns, TagsTuple>::num_columns( derived ) ) +
               ... );
    }
    /** Shorthand for the sum over *all* columns.
     */
    [[nodiscard]] constexpr std::size_t sum_of_sizeofs() const {
      return sum_of_sizeofs( std::index_sequence_for<Tags...>{} );
    }

    /** Get the index of a particular tag type.
     */
    template <typename Tag>
    [[nodiscard]] static constexpr std::size_t index() {
      return LHCb::index_of_v<Tag, TagsTuple>;
    }
    /** Get the sum of all the sizeof(T) of columns **before** Tag
     */
    template <std::size_t column_index>
    [[nodiscard]] constexpr std::size_t sum_of_sizeofs_before() const {
      // Need the sum of sizeofs of tags that come before Tag,
      // i.e a sum from 0 to column_index-1 (or zero if column_index is zero)
      if constexpr ( column_index == 0 ) {
        return 0; // nothing before the first column
      } else {
        return sum_of_sizeofs( std::make_index_sequence<column_index>{} );
      }
    }
    /** Backend for [non-]const data<Tag>().
     */
    template <typename Tag, typename Data>
    [[nodiscard]] constexpr auto data_impl( Data* data, std::size_t capacity, std::size_t column_offset = 0 ) const {
      // We want the column 'column_offset' after 'Tag'
      auto const sizeofs_before = sum_of_sizeofs_before<index<Tag>()>() + column_offset * sizeof( typename Tag::type );
      // Get the return type, which is const-qualified if 'data' was
      using ret_t = std::conditional_t<std::is_const_v<Data>, typename Tag::type const*, typename Tag::type*>;
      // Don't bother doing anything complicated if the data is nullptr
      if ( !data ) { return ret_t{nullptr}; }
      // The product of any valid capacity and [sum of] sizeofs should be a
      // multiple of sizeof( block_t )
      assert( ( sizeofs_before * capacity ) % sizeof( block_t ) == 0 );
      // Calculate the address of the start of this column
      auto* column_data =
          reinterpret_cast<ret_t>( std::next( data, ( sizeofs_before * capacity ) / sizeof( block_t ) ) );
      // Make sure that this pointer respects the alignment requirements that it should
      assert( boost::alignment::is_aligned( column_data, max_alignment ) );
      return column_data;
    }
    /** Helper for reserve(). Copy 'size' bytes from an offset into 'data' into
     *  an offset into 'new_data', where the offsets are calculated based on
     *  Tag and the given capacities ('capacity' & 'new_capacity' respectively)
     */
    template <typename Tag>
    void copy_impl( std::size_t size, block_t* data, std::size_t capacity, block_t* new_data,
                    std::size_t new_capacity ) {
      // See if there are multiple copies of this column
      auto const num_columns = Tag::num_columns( static_cast<Derived const&>( *this ) );
      for ( auto n = 0ul; n < num_columns; ++n ) {
        auto tag_data     = data_impl<Tag>( data, capacity, n );          // old address of start of column
        auto new_tag_data = data_impl<Tag>( new_data, new_capacity, n );  // new address of start of column
        std::copy( tag_data, std::next( tag_data, size ), new_tag_data ); // copy valid old entries to new
      }
    }

    allocator_type           m_alloc;
    block_t*                 m_data{nullptr};
    std::size_t              m_size{0}, m_capacity{required_padding_entries};
    Zipping::ZipFamilyNumber m_zip_identifier;
  };
} // namespace LHCb::v2::Event
