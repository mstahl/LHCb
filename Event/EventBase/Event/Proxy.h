/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LHCbMath/SIMDWrapper.h"
#include "Zip.h"

#include <boost/mp11/algorithm.hpp>

/**
 * A proxy represent a slice of data of a certain width (defined by the simd).
 * It can be used to read / write or perform computation on the stored data.
 *
 * Three level of proxies are defined corresponding to the three levels of
 * containers:
 *
 * Zip           -> ZipProxy
 * SOACollection -> Proxy
 * Tag           -> NDNumericProxy
 *
 * Each container base class have an associated proxy base class that can
 * be used as default proxy or customized.
 */
namespace LHCb::v2::Event {
  /**
   * Proxy representing an N-Dimensional array of numerical data (ints or floats)
   */
  template <typename Tag, SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
            typename... Ts>
  struct NDNumericProxy {
    using simd_t     = SIMDWrapper::type_map_t<Simd>;
    using type       = typename Tag::template vec_type<simd_t>;
    using OffsetType = LHCb::Pr::detail::offset_t<Behaviour, Simd>;

    NDNumericProxy( ContainerType* container, OffsetType offset, Ts... Is )
        : m_container( container ), m_offset( offset ), m_indices( Is... ) {}

    inline __attribute__( ( always_inline ) ) constexpr auto get() const {
      auto ptr = std::apply(
          [&]( auto&&... args ) {
            return m_container->template data<Tag>( std::forward<decltype( args )>( args )... );
          },
          m_indices );
      if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
        return gather( ptr, m_offset );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
        return type{*std::next( ptr, m_offset )};
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
        return type{std::next( ptr, m_offset )};
      } else {
        static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::Compress,
                       "New ProxyBehaviour was added but not implemented everywhere!" );
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::Compress, "Compress proxies are write-only." );
      }
    }

    inline __attribute__( ( always_inline ) ) constexpr void set( type value ) {
      auto ptr = std::apply(
          [&]( auto&&... args ) {
            return m_container->template data<Tag>( std::forward<decltype( args )>( args )... );
          },
          m_indices );
      if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
        value.store( std::next( ptr, m_offset ) );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Compress ) {
        auto const [start_index, mask] = m_offset;
        value.compressstore( mask, std::next( ptr, start_index ) );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::ScalarFill, "ScalarFill proxies are read-only." );
      } else {
        static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather,
                       "New ProxyBehaviour was added but not implemented everywhere!" );
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::ScatterGather,
                       "ScatterGather writing is not implemented yet." );
      }
    }

  private:
    ContainerType*          m_container;
    OffsetType              m_offset;
    const std::tuple<Ts...> m_indices;
  };

  /**
   * Proxy representing an SOACollection
   */
  template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType>
  struct Proxy {
    static_assert(
        Simd != SIMDWrapper::Best,
        "To allow proxy objects to be passed between conventionally compiled and JIT-compiled code, where `Best` has "
        "different meanings, `Best` should be translated into a concrete backend before proxy objects are created." );
    // Perhaps `simd` should be given a better name, but this name is assumed in
    // Functors::Adapters::CombinationFromComposite
    static constexpr SIMDWrapper::InstructionSet simd = Simd;
    using simd_t                                      = SIMDWrapper::type_map_t<Simd>;
    using mask_v                                      = typename simd_t::mask_v;
    using OffsetType                                  = LHCb::Pr::detail::offset_t<Behaviour, Simd>;

    template <typename Tag>
    using hasTag = typename ContainerType::template hasTag<Tag>;

    Proxy( ContainerType* container, OffsetType offset ) : m_container( container ), m_offset( offset ) {}

    // Make a const proxy from non-const proxy
    template <typename T = ContainerType, typename std::enable_if_t<std::is_const_v<T>, int> = 0>
    Proxy( Proxy<Simd, Behaviour, std::remove_const_t<T>> other )
        : m_container{&other.container()}, m_offset{other.offset()} {}

    template <typename Tag, typename... Ts>
    constexpr auto field( Ts... Is ) const {
      using tag_proxy = typename Tag::template proxy_type<Tag, Simd, Behaviour, ContainerType, Ts...>;
      return tag_proxy{m_container, m_offset, Is...};
    }

    template <typename Tag, typename... Ts>
    constexpr auto get( Ts... Is ) const {
      using tag_proxy = typename Tag::template proxy_type<Tag, Simd, Behaviour, ContainerType, Ts...>;
      return tag_proxy{m_container, m_offset, Is...}.get();
    }

    constexpr ContainerType&       container() { return *m_container; }
    constexpr ContainerType const& container() const { return *m_container; }

    constexpr auto loop_mask() const {
      if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
        return simd_t::loop_mask( offset(), m_container->size() );
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ||
                            Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
        // TODO we might like to reconsider this for Gather proxies, there one
        //      could imagine having a proxy that knows which of its elements
        //      are valid
        return simd_t::mask_true();
      } else {
        static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::Compress );
        // TODO what makes sense here?
      }
    }

    constexpr auto indices() const {
      if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScalarFill ) {
        return typename simd_t::int_v{m_offset};
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::ScatterGather ) {
        return m_offset;
      } else if constexpr ( Behaviour == LHCb::Pr::ProxyBehaviour::Contiguous ) {
        return simd_t::indices( m_offset );
      } else {
        static_assert( Behaviour == LHCb::Pr::ProxyBehaviour::Compress,
                       "New ProxyBehaviour was added but not implemented everywhere!" );
        static_assert( Behaviour != LHCb::Pr::ProxyBehaviour::Compress,
                       "indices() not implemented for write-only Compress proxy type" );
        // TODO what makes sense here?
      }
    }

    OffsetType            offset() const { return m_offset; }
    static constexpr auto width() { return simd_t::size; }

    // Make functors return type deduction happy ...
    static constexpr mask_v mask_true() { return mask_v{true}; }

  private:
    ContainerType* m_container;
    OffsetType     m_offset;
  };

  namespace detail {
    template <typename First, typename... Others>
    struct first {
      using type = First;
    };

    // Seach the list of Proxy T... the first Proxy that contains Tag
    template <typename Tag, typename... T>
    struct find_proxy_with_tag {
      using type                  = void;
      static constexpr bool found = false;
      static constexpr bool value = found;
    };
    template <typename Tag, typename First, typename... T>
    struct find_proxy_with_tag<Tag, First, T...> {
      using type                  = typename std::conditional<First::template hasTag<Tag>::value, First,
                                             typename find_proxy_with_tag<Tag, T...>::type>::type;
      static constexpr bool found = First::template hasTag<Tag>::value || find_proxy_with_tag<Tag, T...>::found;
      static constexpr bool value = found;
    };

    // Turn Proxy<Simd, Behaviour, T const> into ProxyBehaviour<Simd, Behaviour, T>
    template <typename>
    struct non_const_proxy {};

    template <template <SIMDWrapper::InstructionSet, LHCb::Pr::ProxyBehaviour, typename> typename Proxy,
              SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType>
    struct non_const_proxy<Proxy<Simd, Behaviour, ContainerType>> {
      using type = Proxy<Simd, Behaviour, std::remove_const_t<ContainerType>>;
    };

    template <typename Proxy>
    using non_const_proxy_t = typename non_const_proxy<Proxy>::type;
  } // namespace detail

  /**
   * Proxy representing a Zip of SOACollections
   */
  template <typename... Proxies>
  struct ZipProxy : Proxies... {
    ZipProxy( Proxies... p ) : Proxies( p )... {}

    /** @brief Make a ZipProxy-of-const from a ZipProxy-of-non-const proxies
     *
     *  This overload only participates if at least one of `Proxies` refers to
     *  a const qualified container; i.e. this does *not* shadow the copy
     *  constructor, as it is only enabled if the argument type is different to
     *  `this`.
     */
    template <
        typename ProxiesList = boost::mp11::mp_list<Proxies...>,
        typename std::enable_if_t<
            !std::is_same_v<ProxiesList, boost::mp11::mp_transform<detail::non_const_proxy_t, ProxiesList>>, int> = 0>
    ZipProxy( ZipProxy<detail::non_const_proxy_t<Proxies>...> const& old )
        : Proxies{static_cast<detail::non_const_proxy_t<Proxies> const&>( old )}... {}

    template <typename Tag, typename... Ts>
    auto field( Ts... Is ) const {
      using prox = typename detail::find_proxy_with_tag<Tag, Proxies...>::type;
      static_assert( detail::find_proxy_with_tag<Tag, Proxies...>::found );
      return static_cast<prox const*>( this )->template field<Tag>( Is... );
    }

    template <typename Tag, typename... Ts>
    auto get( Ts... Is ) const {
      using prox = typename detail::find_proxy_with_tag<Tag, Proxies...>::type;
      static_assert( detail::find_proxy_with_tag<Tag, Proxies...>::found );
      return static_cast<prox const*>( this )->template get<Tag>( Is... );
    }

    template <typename Tag>
    using hasTag = detail::find_proxy_with_tag<Tag, Proxies...>;
    using detail::first<Proxies...>::type::loop_mask;
    using detail::first<Proxies...>::type::indices;
    using detail::first<Proxies...>::type::offset;
    using detail::first<Proxies...>::type::width;
    using detail::first<Proxies...>::type::mask_true;
    using typename detail::first<Proxies...>::type::OffsetType;
  };
} // namespace LHCb::v2::Event

// Enable header lookup of proxies
template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour Behaviour, typename... ContainerTypes>
struct LHCb::header_map<typename LHCb::v2::Event::Proxy<simd, Behaviour, ContainerTypes...>> {
  static constexpr auto value = ( LHCb::header_map_v<std::remove_const_t<ContainerTypes>> + ... ) + "Event/Proxy.h";
};

// Enable header lookup of zips of proxies
template <typename... Ts>
struct LHCb::header_map<LHCb::v2::Event::ZipProxy<Ts...>> {
  static constexpr auto value = ( LHCb::header_map_v<Ts> + ... ) + "Event/Proxy.h";
};
