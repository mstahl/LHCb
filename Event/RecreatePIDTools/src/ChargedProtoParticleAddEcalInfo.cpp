/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddEcalInfo.h
 *
 * Implementation file for algorithm FutureChargedProtoParticleAddEcalInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "Interfaces/IProtoParticleTool.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"

//-----------------------------------------------------------------------------

/** @class FutureChargedProtoParticleAddEcalInfo FutureChargedProtoParticleAddEcalInfo.h
 *
 *  Updates the ECAL information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

namespace LHCb::Rec::ProtoParticle::Charged {

  class AddEcalInfo final : public extends<GaudiTool, Interfaces::IProtoParticles> {

  public:
    /// Standard constructor
    using extends::extends;

    StatusCode operator()( ProtoParticles& ) const override; ///< Algorithm execution

  private:
    using TrAccTable    = Relation1D<Track, bool>;
    using HypoTrTable2D = RelationWeighted2D<CaloHypo, Track, float>;
    using ClusTrTable2D = RelationWeighted2D<CaloCellID, Track, float>;
    using TrEvalTable   = Relation1D<Track, float>;

    /// Add Calo Ecal information to the given ProtoParticle
    bool addEcal( LHCb::ProtoParticle& proto, TrAccTable const& InEcalTable, HypoTrTable2D const& ElecTrTable,
                  ClusTrTable2D const& ClusTrTable, TrEvalTable const& EcalChi2Table, TrEvalTable const& EcalETable,
                  TrEvalTable const& ClusChi2Table, TrEvalTable const& dlleEcalTable,
                  TrEvalTable const& dllmuEcalTable ) const;

  private:
    ToolHandle<Calo::Interfaces::IHypoEstimator> m_estimator{this, "CaloHypoEstimator", "CaloFutureHypoEstimator"};

    DataObjectReadHandle<TrAccTable>    m_InEcalTable{this, "InputInEcalLocation", CaloFutureIdLocation::InEcal};
    DataObjectReadHandle<HypoTrTable2D> m_elecTrTable{this, "InputElectronMatchLocation",
                                                      CaloFutureIdLocation::ElectronMatch};
    DataObjectReadHandle<ClusTrTable2D> m_clusTrTable{this, "InputClusterMatchLocation",
                                                      CaloFutureIdLocation::ClusterMatch};
    DataObjectReadHandle<TrEvalTable>   m_EcalChi2Table{this, "InputEcalChi2Location", CaloFutureIdLocation::EcalChi2};
    DataObjectReadHandle<TrEvalTable>   m_EcalETable{this, "InputEcalELocation", CaloFutureIdLocation::EcalE};
    DataObjectReadHandle<TrEvalTable> m_ClusChi2Table{this, "InputClusterChi2Location", CaloFutureIdLocation::ClusChi2};
    DataObjectReadHandle<TrEvalTable> m_dlleEcalTable{this, "InputEcalPIDeLocation", CaloFutureIdLocation::EcalPIDe};
    DataObjectReadHandle<TrEvalTable> m_dllmuEcalTable{this, "InputEcalPIDmuLocation", CaloFutureIdLocation::EcalPIDmu};

    /// CaloElectron tool, the tool has an internal state, therefore mutable.
    ToolHandle<Calo::Interfaces::IElectron> m_electron{this, "CaloFutureElectron", "CaloFutureElectron"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( AddEcalInfo, "ChargedProtoParticleAddEcalInfo" )

  //=============================================================================
  // Main execution
  //=============================================================================
  StatusCode AddEcalInfo::operator()( ProtoParticles& protos ) const {
    const auto& inEcalTable    = *m_InEcalTable.get();
    const auto& elecTrTable    = *m_elecTrTable.get();
    const auto& clusTrTable    = *m_clusTrTable.get();
    const auto& ecalChi2Table  = *m_EcalChi2Table.get();
    const auto& ecalETable     = *m_EcalETable.get();
    const auto& clusChi2Table  = *m_ClusChi2Table.get();
    const auto& dlleEcalTable  = *m_dlleEcalTable.get();
    const auto& dllmuEcalTable = *m_dllmuEcalTable.get();

    // Loop over proto particles and update ECAL info
    for ( auto* proto : protos )
      addEcal( *proto, inEcalTable, elecTrTable, clusTrTable, ecalChi2Table, ecalETable, clusChi2Table, dlleEcalTable,
               dllmuEcalTable );

    return StatusCode::SUCCESS;
  }

  //=============================================================================

  //=============================================================================
  // Add Calo Ecal info to the protoparticle
  //=============================================================================
  bool AddEcalInfo::addEcal( LHCb::ProtoParticle& proto, TrAccTable const& InEcalTable,
                             HypoTrTable2D const& ElecTrTable, ClusTrTable2D const& ClusTrTable,
                             TrEvalTable const& EcalChi2Table, TrEvalTable const& EcalETable,
                             TrEvalTable const& ClusChi2Table, TrEvalTable const& dlleEcalTable,
                             TrEvalTable const& dllmuEcalTable ) const {
    // First remove existing ECAL info
    proto.removeCaloEcalInfo();

    // Now add new ECAL info
    const auto aRange = InEcalTable.relations( proto.track() );
    if ( aRange.empty() ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " -> No entry for that track in the Ecal acceptance table " << endmsg;
      return false;
    }

    if ( !aRange.front().to() ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> The track is NOT in Ecal acceptance" << endmsg;
      return false;
    }

    if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> The track is in Ecal acceptance" << endmsg;
    proto.addInfo( LHCb::ProtoParticle::additionalInfo::InAccEcal, true );

    // Get the highest weight associated electron CaloHypo (3D matching)

    if ( const auto hRange = ElecTrTable.inverse()->relations( proto.track() ); !hRange.empty() ) {

      const auto* hypo = hRange.front().to();
      proto.addToCalo( hypo );
      // CaloElectron->caloTrajectory must be after addToCalo
      auto hypoPair = m_electron->getElectronBrem( proto );
      if ( hypoPair ) {
        proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloTrajectoryL,
                       *m_electron->caloTrajectoryL( proto, CaloPlane::ShowerMax ) );
        proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloEoverP, m_electron->eOverP( proto ) );
      }

      using namespace Calo::Enum;
      auto value_or_default = [data = m_estimator->get_data( *hypo )]( auto type ) {
        auto it = data.find( type );
        if ( it != data.end() ) return it->second;
        return Default;
      };
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloChargedEcal, value_or_default( DataType::ClusterE ) );
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloChargedID, value_or_default( DataType::CellID ) );
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, hRange.front().weight() );
    }

    // Get the highest weight associated CaloCluster (2D matching)
    if ( const auto r = ClusTrTable.inverse()->relations( proto.track() ); !r.empty() ) {
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloTrMatch, r.front().weight() );
    }

    auto addTarget = [&]( const auto& table, LHCb::ProtoParticle::additionalInfo ai ) {
      if ( const auto r = table.relations( proto.track() ); !r.empty() ) { proto.addInfo( ai, r.front().to() ); }
    };

    // Get EcalE (intermediate) estimator
    addTarget( EcalETable, LHCb::ProtoParticle::additionalInfo::CaloEcalE );

    // Get EcalChi2 (intermediate) estimator
    addTarget( EcalChi2Table, LHCb::ProtoParticle::additionalInfo::CaloEcalChi2 );

    // Get ClusChi2 (intermediate) estimator
    addTarget( ClusChi2Table, LHCb::ProtoParticle::additionalInfo::CaloClusChi2 );

    // Get Ecal DLL(e)
    addTarget( dlleEcalTable, LHCb::ProtoParticle::additionalInfo::EcalPIDe );

    // Get Ecal DLL(mu)
    addTarget( dllmuEcalTable, LHCb::ProtoParticle::additionalInfo::EcalPIDmu );

    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << " -> Ecal PID : "
                << " Chi2-3D    =" << proto.info( LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, -999. )
                << " Chi2-2D    =" << proto.info( LHCb::ProtoParticle::additionalInfo::CaloTrMatch, -999. )
                << " EcalE      =" << proto.info( LHCb::ProtoParticle::additionalInfo::CaloEcalE, -999. )
                << " ClusChi2   =" << proto.info( LHCb::ProtoParticle::additionalInfo::CaloClusChi2, -999. )
                << " EcalChi2   =" << proto.info( LHCb::ProtoParticle::additionalInfo::CaloEcalChi2, -999. )
                << " Dlle (Ecal) =" << proto.info( LHCb::ProtoParticle::additionalInfo::EcalPIDe, -999. )
                << " Dllmu (Ecal) =" << proto.info( LHCb::ProtoParticle::additionalInfo::EcalPIDmu, -999. )
                << " Ecal Cluster " << proto.info( LHCb::ProtoParticle::additionalInfo::CaloChargedEcal, 0. )
                << " TrajectoryL " << proto.info( LHCb::ProtoParticle::additionalInfo::CaloTrajectoryL, 0. ) << endmsg;

    return true;
  }
} // namespace LHCb::Rec::ProtoParticle::Charged
