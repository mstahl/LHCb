/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ProtoParticle.h"
#include "GaudiAlg/Consumer.h"

struct PrintProtoParticles final : Gaudi::Functional::Consumer<void( LHCb::ProtoParticles const& )> {
  PrintProtoParticles( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name, pSvcLocator, {"Input", ""}} {};

  void operator()( LHCb::ProtoParticles const& protos ) const override {
    always() << "Print all proto particles of an event" << endmsg;
    for ( auto const* proto : protos ) {
      always() << "{"
               << " Track " << ( proto->track() != nullptr ) << " CaloHypos " << proto->calo().size() << " RichPID "
               << ( proto->richPID() != nullptr ) << " MuonPID " << ( proto->muonPID() != nullptr ) << "\n"
               << "ExtraInfo [";
      for ( const auto& i : proto->extraInfo() ) {
        always() << " " << static_cast<LHCb::ProtoParticle::additionalInfo>( i.first ) << "=" << i.second;
      }
      always() << " ] } \n";
    }
    always() << "Printing ended" << endmsg;
  }
};

DECLARE_COMPONENT( PrintProtoParticles )
