2020-12-12 LHCb v51r3
===

This version uses
Gaudi [v34r1](../../../../Gaudi/-/tags/v34r1) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to LHCb [v51r2](../-/tags/v51r2), with the following changes:

### New features ~"new feature"

- ~Decoding ~UT | UT TELL40 encoder/decoder v1r1, !2631 (@xuyuan) :star:
- ~Tracking ~UT ~"Event model" | Add PrDownstreamTracks, !2801 (@ahennequ)
- ~Tracking ~"Event model" ~Utilities | Simplify Pr::Tracks, !2811 (@ahennequ) :star:
- ~RICH ~Conditions | Add RICH Derived condition objects to wrap detector objects, !2795 (@jonrob) :star:
- ~Persistency | Add serialisation and de-serialisation to class PackedWeightedRelations, !2862 (@axu)
- ~Utilities | Spline grid interpolation, !2511 (@graven)


### Fixes ~"bug fix" ~workaround

- ~Decoding ~UT | UTDet / UTDAQ / UTKernel: const correctness fixes, !2852 (@graven) [Rec#173]
- ~Build | Add workaround to suppress clang unused-lambda-capture warning, !2834 (@jonrob)


### Enhancements ~enhancement

- ~RICH | Rich add safe histogram fill method, !2819 (@jonrob)
- ~RICH | RICH - SIN model updates., !2806 (@bmalecki) :star:
- ~"Event model" ~Utilities | Write via zip proxies, !2783 (@olupton)
- ~Persistency | Made (Un)PackCaloHypo half way functional, !2836 (@sponce)
- ~Persistency ~Core | Support EvtStoreSvc in PackParticlesAndVertices, !2878 (@apearce)
- ~Conditions | Changed the location of the LHCb compact XML description, !2851 (@bcouturi)
- ~Build | Fix reproducibility of LoKi functor cache sources, !2858 (@rmatev)
- ~Build | Fix test_public_headers_build test target, !2536 (@pseyfert)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Fix python warnings exposed in dev4 slot, !2847 (@cattanem)
- ~UT | Remove unused UT code, !2854 (@graven)
- ~UT | Replace `GeomCache::computeGeometry` with a constructor, !2850 (@graven)
- ~Calo | Cleanup of Calorimeter interfaces, !2822 (@graven)
- ~RICH ~Functors ~Persistency ~Core ~Conditions | Drop use of IJobOptionsSvc, !2856 (@clemenci) [(gaudi/Gaudi#140]
- ~Composites ~"Event model" | Migrate Composites' zip machinery, !2849 (@olupton) [#103]
- ~Composites ~Persistency | Re-organize ProtoParticle unpacking/making/ExtraInfo filling, !2797 (@graven)
- ~Filters | Simplify the LoKi::HDRFilter logic, !2857 (@apearce)
- ~"Event model" ~"MC checking" | Remove unused LinksByKey::getAllLinks, !2829 (@chasse)
- ~Persistency | Consolidate EventPacker components, !2817 (@graven)
- ~Conditions | Cleanup of alignment specific code in DetDesc, !2882 (@sponce)
- ~Conditions | Dropped unused class DetElemFinder, !2845 (@sponce)
- ~Conditions | Updated ref file of dd4hep_conditions test after new exclusions in log files, !2833 (@sponce)
- ~Conditions | Fixes to DD4hep log to decrease differences with DD4hep, !2828 (@sponce)
- ~Conditions | Adapt to DD4hep 1.14, !2824 (@sponce)
- ~Utilities | Simplify SynchronizedValue, !2846 (@graven)
- ~Build | Attempt to fix handling of Histos in ref files, !2874 (@sponce)
- Remove no longer required support for range v3 version < 0.9, !2816 (@graven)


### Documentation ~Documentation

- Update CONTRIBUTING.md, !2825 (@cattanem)
